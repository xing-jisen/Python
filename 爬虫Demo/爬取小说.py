# -*- codeing = utf-8 -*-
# @Time : 2022/5/23 23:07
# @Author : 邢继森
# @File : 爬取小说.py
# Software : PyCharm
import time
import random  # 随机数生成
import docx
import requests
import parsel
from faker import Factory
from selenium import webdriver
from selenium.webdriver.chrome.options import Options # 导入无头浏览器对应类
from selenium.webdriver import ChromeOptions # 导入实现规避检测的类

def Link():
    # url = 'http://www.530p.com/xuanhuan/douluodalu-15077'
    url = 'http://www.530p.com/qita/dushilirendemengzuo-25258/'
    # 随机U_A
    f = Factory.create()
    UA = f.user_agent()
    # U_A伪装
    headers = {
        'User-Agent': UA,
    }
    reaponse = requests.get(url,headers=headers).text
    page_text = reaponse.encode('iso-8859-1').decode('gbk')# 设置编码格式
    rell = parsel.Selector(page_text)
    url_List = rell.xpath('/ html/body/div[3]/div[5]/div')
    list_url = []
    for div in url_List:
        url = div.xpath('./a/@href').get()
        url = 'http://www.530p.com' + str(url)
        list_url.append(url)
    return list_url


def Fiction(url,T):
    try:
        # 实现无可视化的界面的操作
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        # 实现规避检测
        option = ChromeOptions()
        option.add_experimental_option('excludeSwitches', ['enable-automation'])

        # 如何实现让selenium规避检测到的风险
        bro = webdriver.Chrome(executable_path='C:\Program Files\Google\Chrome\Application\chromedriver.exe',
                               chrome_options=chrome_options, options=option)
        bro.get(url)
        page_text = bro.page_source  # 源码数据
        # page_text = page_text.encode('iso-8859-1').decode('gbk')# 设置编码格式
        # print(page_text)
        rell = parsel.Selector(page_text)
        # 获取标题
        title = rell.xpath('//*[@id="cps_title"]/h1/text()').get()
        # 获取内容
        br_body = rell.xpath('//*[@id="cp_content"]').get()
        # 去掉用不到的东西
        br_body = br_body.replace('<div id="cp_content">', '') \
            .replace('<script language="javascript">setFontSize();</script>无弹窗小说网(www.530p.com)', '').replace('</div>','').replace('<br>','')  # 把内容替换掉，替换成空
        # .strip()去除空格 ljust(),center(),rjust()函数实现输出的字符串左对齐、居中、右对齐
        print(title, '\n\n', br_body.strip())
        T.append(title + '\t\t\t\t\t' + br_body + '\t\t\t')
    except:
        pass
    else:
        return T
# 存入word
def Write_in(T):
    from docx import Document
    # 准备写入内容
    # title = title
    # body = br_body
    # 设置文档的基础字体中文
    file = docx.Document()  # 创建内存中的word文档对象
    # file.add_paragraph(title)# 写入标题
    file.add_paragraph(T)# 写入内容
    file.save("斗罗大陆123.docx")  # 保存才能看到结果

if __name__ == '__main__':
    # 返回小说链接
    list_url = Link()
    print(list_url)
    T = []
    for i,url in enumerate(list_url):
        print("正在爬取第",(i+1),"章")
        Fiction(url,T)
        # print(T)
        Write_in(T)

