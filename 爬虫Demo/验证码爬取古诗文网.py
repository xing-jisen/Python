# -*- codeing = utf-8 -*-
# @Time : 2022/4/18 19:10
# @Author : 邢继森
# @File : 验证码爬取古诗文网.py
# Software : PyCharm
# 导包
import requests
import parsel

# 获取登录验证码
def Auth_code():
    url = 'https://so.gushiwen.cn/user/login.aspx?from=http://so.gushiwen.cn/user/collect.aspx'
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    page_text = requests.get(url=url, headers=headers).text
    # 解析验证码数据中的属性值
    tree = parsel.Selector(page_text)
    code_img_src = 'https://so.gushiwen.cn/' + tree.xpath('//*[@id="imgCode"]/@src').get()
    img_data = requests.get(url=code_img_src, headers=headers).content
    # 验证码图片保存到本地
    with open('./验证码图片.jpg', 'wb') as fp:
        fp.write(img_data)
        print('验证码输出完毕!!!')

# 创建一个session对象
session = requests.Session()

# 模拟登陆
def Log_in():
    url = 'https://so.gushiwen.cn/user/collect.aspx'
    # 对参数封装
    result = input("请手动输入验证码:")
    from_data = {
        '__VIEWSTATE':'l0Y3i0HIbSIalrAEmt6fz3+048VV+unYzEpojyG3YOMgdfg0HUjZypS/qpIIPTHmpH+XiyxaPTfPrme+xkP69UyQkJQaioK15zGnEWaysnLBNobAQL+QzekajYk=',
        '__VIEWSTATEGENERATOR':'C93BE1AE',
        'from':'http://so.gushiwen.cn/user/collect.aspx',
        'email':'15269095903',
        'pwd':'Xjs0912 +.',
        'code':result,
        'denglu':'登录'
    }
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    response = session.post(url=url,headers=headers,data=from_data)
    # 输出200证明正确 404是找不到页面 验证方式1 （通用）
    print('响应状态码:',response.status_code)
    login_page_text = response.text
    # 验证方式2 打印源代码
    with open('./wangye/yanzheng.html','w',encoding='utf-8') as fp:
        fp.write(login_page_text)



# 获取个人信息页面
def Message():
    # 爬取当前用户的个人主页对应的的页面数据
    url = 'https://so.gushiwen.cn/user/collect.aspx?sort=t'
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36',
        # 手动处理Cookie
        # 'Cookie': 'login=flase; ASP.NET_SessionId=tc1ij4gs4pkmol2tmzd23srf; codeyzgswso=80645dac2f5ebd6a; gsw2017user=2419317%7c5BD217892E7610CF610F878852EB80C8; login=flase; wxopenid=defoaltid; gswZhanghao=15269095903; gswPhone=15269095903'
    }
    # 使用携带cookie的session进行get请求发送
    detal_page_text = session.get(url=url,headers=headers).text
    with open('./wangye/geren.html','w',encoding='utf-8') as fp:
        fp.write(detal_page_text)

if __name__ == "__main__":
    Auth_code()
    Log_in()
    Message()