# -*- codeing = utf-8 -*-
# @Time : 2022/4/29 21:43
# @Author : 邢继森
# @File : 爬取网易云评论.py
# Software : PyCharm
# 找到未加密的参数
# 想办法把参数进行贾母（必须遵从网易云洛基），Params，encSecKey
# 请求到网易，拿到评论信息

url = 'https://music.163.com/weapi/comment/resource/comments/get?csrf_token=b0d405d6fc67246323c7e6c4460e2a43'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
}