# -*- codeing = utf-8 -*-
# @Time : 2022/4/18 13:53
# @Author : 邢继森
# @File : 爬取小说.py
# Software : PyCharm
# xpath解析原理

import requests
import parsel
import time
def Take_a_climb(url):
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    response = requests.get(url=url,headers=headers).text
    # print(response)
    tree = parsel.Selector(response)
    # 数据解析
    list_li = tree.xpath('/html/body/div[2]/div[4]/div[2]/div[2]/div')
    fp = open('../wenben/小说信息.txt','w',encoding='utf-8')
    for count,div in enumerate(list_li):
        if (count+1) <= 20:
            div_title = div.xpath('./div[2]/div[1]/a/text()').get()
            div_body = div.xpath('./div[2]/div[3]/text()').get()
            print(div_title,div_body)
            article = '\n\n' + div_title + '\n\t' + div_body
            fp.write(article)

# 函数入口
if __name__ == "__main__":
    for i in range(1,11):
        time.sleep(2)
        print('第',i,'页')
        time.sleep(5)
        url = f'http://www.zongheng.com/rank/details.html?rt=1&d=1&i=2&p={i}'
        Take_a_climb(url)
        print('\n')
