# -*- codeing = utf-8 -*-
# @Time : 2022/4/18 14:46
# @Author : 邢继森
# @File : 解析下载图片.py
# Software : PyCharm
import requests
import parsel
import os
import time


def Climb_take_pictures(url):


    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    response = requests.get(url=url, headers=headers).text
    sel = parsel.Selector(response)

    # 解析数据
    tree = sel.xpath('//*[@id="main"]/div[3]/ul/li')

    # 创建文件夹
    if not os.path.exists(r'D:\PyCharm\pycharm one\爬虫Demo\tupian1'):
        os.mkdir(r'D:\PyCharm\pycharm one\爬虫Demo\tupian1')

    for li in tree:
        img_src = 'https://pic.netbian.com' + li.xpath('./a/img/@src').get()
        img_title = li.xpath('./a/img/@alt').get() + '.jpg'

        # 通用解决中文乱码的方法
        img_title = img_title.encode('iso-8859-1').decode('gbk')

        # print(img_title,img_src)
        # 进行持久化存储
        img_data = requests.get(url=img_src, headers=headers).content
        with open('../tupian1/' + img_title, 'wb') as fp:
            fp.write(img_data)
            print('下载完成!')
            time.sleep(1)

if __name__ == "__main__":
    for i in range(1,144):

        j = '_' + str(i)
        # url = f'https://pic.netbian.com/4kmeinv/index{j}.html'
        try:
            print('第', i, '页')
            Climb_take_pictures('https://pic.netbian.com/4kfengjing/index.html')
            time.sleep(3)
        except:
            print('第', i, '页')
            Climb_take_pictures(f'https://pic.netbian.com/4kfengjing/index{j}.html')
            time.sleep(3)
