# -*- codeing = utf-8 -*-
# @Time : 2022/4/18 14:19
# @Author : 邢继森
# @File : 58二手房.py
# Software : PyCharm
import requests
import parsel
import os
# UA伪装
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
}
url = 'https://bj.58.com/ershoufang/?PGTID=0d100000-0000-1bd2-31fa-6f4d500b54b0&ClickID=2'

response = requests.get(url=url,headers=headers).text
sel = parsel.Selector(response)
li_list = sel.xpath('//*[@id="__layout"]/div/section/section[3]/section[1]/section[2]/div')
# print(li_list)
    # 创建文件夹
    # 如果文件夹不存在
if not os.path.exists('D:\PyCharm\pycharm one\爬虫Demo\wenben'):
    # 则创建一个文件夹
    os.mkdir('D:\PyCharm\pycharm one\爬虫Demo\wenben')

fp = open('../wenben/58二手房信息.txt','w',encoding='utf-8')
for div in li_list:
    title = div.xpath('./a/div[2]/div[1]/div[1]/h3/text()').get()
    print(title)
    fp.write(title+'\n')