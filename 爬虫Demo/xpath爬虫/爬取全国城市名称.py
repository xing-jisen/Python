# -*- codeing = utf-8 -*-
# @Time : 2022/4/18 15:46
# @Author : 邢继森
# @File : 爬取全国城市名称.py
# Software : PyCharm
import requests
import parsel
url = 'http://www.air-level.com/'
# UA伪装
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
}
response = requests.get(url=url,headers=headers).text
tree = parsel.Selector(response)
fp = open('../wenben/全国城市名称.txt','w',encoding='utf-8')
# 数据解析
host_div = tree.xpath('//*[@id="citylist"]/div | //*[@id="citylist"]/div/a')
for count,div in enumerate(host_div):
    # print(div)
        div_name = div.xpath('./text()').get()
        print(div_name)

        fp.write(div_name + '\n')