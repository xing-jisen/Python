# -*- codeing = utf-8 -*-
# @Time : 2022/4/16 16:36
# @Author : 邢继森
# @File : 破解百度翻译.py
# Software : PyCharm
import requests
import json
# 指定url
post_url = 'https://fanyi.baidu.com/sug'
# 进行UA伪装
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
}
word = input('输入需要翻译的信息:')
# post 请求参数预处理（同get请求一致）
data = {
    'kw' : word
}
# 请求发送
response = requests.post(url=post_url,data=data,headers=headers)
# 获取相应数据json:json方法返回的是对象
# 确定了是json才可以使用json
dic_obj = response.json()
print(dic_obj)
# 进行持久化存储
fileName = word + '.json'
fp = open('./wangye/' + fileName,'w',encoding='utf-8')
json.dump(dic_obj,fp = fp,ensure_ascii=False)
print('over')