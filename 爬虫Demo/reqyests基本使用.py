# -*- codeing = utf-8 -*-
# @Time : 2022/4/21 21:26
# @Author : 邢继森
# @File : reqyests基本使用.py
# Software : PyCharm
import requests

# 发起请求的url
url = 'http://www.baidu.com'

res = requests.get(url=url)

# 获取响应结果
print(res)
print(res.content)# 二进制文本流
print(res.content.decode('utf-8'))# 二进制的类型转换为普通类型
print(res.text)# 获取相应的内容
print("响应状态码:",res.status_code)# 响应状态码
print(res.request.headers)# 请求头的信息
print(res.headers)# 响应头信息