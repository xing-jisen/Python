# -*- codeing = utf-8 -*-
# @Time : 2022/4/25 16:36
# @Author : 邢继森
# @File : requests中的session方法.py
# Software : PyCharm
import requests

# 需要请求的URL
url = 'https://passport.1905.com/v2/?m=manage&a=home'
headers = {
    'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1Safari/534.50',
    'Cookie': 'PHPSESSID=8co4g4on0iegs82cunjlst0oe1; WOlTvIlgRpuid=0'
}
res = requests.get(url=url,headers=headers)

# 获取响应状态码
code = res.status_code
print(code)

if code == 200:
    with open('./wangye/电影网.html','w',encoding='utf-8') as fp:
        fp.write(res.text)

    print("爬取结束")


