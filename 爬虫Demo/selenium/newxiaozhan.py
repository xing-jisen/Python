# -*- codeing = utf-8 -*-
# @Time : 2022/5/15 21:47
# @Author : 邢继森
# @File : newxiaozhan.py
# Software : PyCharm
import time
from selenium.webdriver import ChromeOptions # 导入实现规避检测的类
from selenium import webdriver
# 实现规避检测
option = ChromeOptions()
option.add_experimental_option('excludeSwitches',['enable-automation'])

# 如何实现让selenium规避检测到的风险
bro = webdriver.Chrome(executable_path='C:\Program Files\Google\Chrome\Application\chromedriver.exe',options=option)
# 请求的url
bro.get('https://newxiaozhan.com/')
time.sleep(60)

# id定位
scbar_txt = bro.find_element_by_id('scbar_txt')
# 标签交互输入信息
scbar_txt.send_keys('蜘蛛侠')
