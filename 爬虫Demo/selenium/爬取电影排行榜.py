# -*- codeing = utf-8 -*-
# @Time : 2022/5/14 16:52
# @Author : 邢继森
# @File : 爬取电影排行榜.py
# Software : PyCharm
import time
import parsel
from selenium import webdriver
# 实例化浏览器对象
bro = webdriver.Chrome(executable_path='C:\Program Files\Google\Chrome\Application\chromedriver.exe') # 驱动程序路径 打开一个浏览器
# 让浏览器发起指定url发起的请求
bro.get('https://movie.douban.com/tag/#/')
time.sleep(2)
# page_source获取浏览器当前源码数据
page_text = bro.page_source
# 解析页面数据
tree = parsel.Selector(page_text)
a_list = tree.xpath('//div[@class="list-wp"]/a')
for a in a_list:
    title = a.xpath('./p/span[1]/text()').get()
    rate = a.xpath('./p/span[2]/text()').get()
    print(title,rate)
time.sleep(5) # 停留五秒



# 关闭浏览器
bro.quit()