# -*- codeing = utf-8 -*-
# @Time : 2022/5/15 15:35
# @Author : 邢继森
# @File : 无头浏览器+规避检测.py
# Software : PyCharm
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options # 导入无头浏览器对应类
from selenium.webdriver import ChromeOptions # 导入实现规避检测的类
# 实现无可视化的界面的操作
chrome_options = Options()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')

# 实现规避检测
option = ChromeOptions()
option.add_experimental_option('excludeSwitches',['enable-automation'])

# 如何实现让selenium规避检测到的风险
bro = webdriver.Chrome(executable_path='C:\Program Files\Google\Chrome\Application\chromedriver.exe',chrome_options=chrome_options,options=option)

# get请求
bro.get('http://www.baidu.com')
print(bro.page_source)# 打印源码数据
time.sleep(2)
# 关闭浏览器
bro.quit()

