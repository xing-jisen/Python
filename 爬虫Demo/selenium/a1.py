# -*- codeing = utf-8 -*-
# @Time : 2022/5/2 21:39
# @Author : 邢继森
# @File : Lixia_District______.py
# Software : PyCharm
import datetime
import os
import pprint
import zlib
import re
import parsel
import requests  # 代替浏览器进行网络请求
import json  # json字典 方便取值
from concurrent.futures import ThreadPoolExecutor  # 开启多线程模式
import random  # 随机数生成
import time  # 导入time模块做定时休眠
import base64  # 加密
from faker import Factory
from openpyxl import Workbook
from faker import Factory
from selenium import webdriver
from selenium.webdriver.chrome.options import Options # 导入无头浏览器对应类
from selenium.webdriver import ChromeOptions, ActionChains  # 导入实现规避检测的类


def Headers(cookie):
    # 随机U_A
    f = Factory.create()
    UA = f.user_agent()
    # U_A伪装
    headers = {
        'Accept': 'application/json',
        'Cookie': cookie,
        'Host': 'jn.meituan.com',
        'User-Agent': UA,
    }
    return headers

# 解析网页
def RquestData(headers):
    # 请求地址
    url = 'https://jn.meituan.com/'
    response = requests.session().get(url,headers=headers) # session() 保持登陆状态
    print(response.status_code == 200)

    # 实现规避检测
    option = ChromeOptions()
    option.add_experimental_option('excludeSwitches', ['enable-automation'])
    # 谷歌浏览器
    bro = webdriver.Chrome(executable_path='C:\Program Files\Google\Chrome\Application\chromedriver.exe',options=option)
    return bro

def Area(bro):
    # 对城市链接发起请求
    bro.get('https://jn.meituan.com/meishi/b139/')
    time.sleep(3)
    # page_source获取浏览器当前源码数据
    page_text = bro.page_source

    rell = parsel.Selector(page_text)
    li_list = rell.xpath('//*[@id="app"]/section/div/div[2]/div[2]/div[1]/ul/li')
    for li in li_list:
        title = li.xpath('./div[2]/a/h4/text()').get()
        print(title)

if __name__ == "__main__":
    cookie = '_lxsdk_cuid=180a8f21c32c8-0701bea909dc82-9771a39-144000-180a8f21c33c8; cityname=%E6%BD%8D%E5%9D%8A; iuuid=0BD85904319FFA99D91F20769ADABEDD046E7CD7F5B1DC705B5BB694C11FAD10; _lxsdk=0BD85904319FFA99D91F20769ADABEDD046E7CD7F5B1DC705B5BB694C11FAD10; webp=1; i_extend=H__a100002__b1; __utma=74597006.1893403857.1652101887.1652101887.1652101887.1; __utmz=74597006.1652101887.1.1.utmcsr=baidu|utmccn=(organic)|utmcmd=organic; ci=96; rvct=96; _hc.v=e051f322-e17d-eea6-96d7-c26c641a0df4.1652158408; _lx_utm=utm_source%3Dbaidu%26utm_medium%3Dorganic; mtcdn=K; client-id=823d6d22-d091-42fb-a930-3962c270c54c; uuid=e34376509a3646b5b794.1652967404.1.0.0; lt=PzzFlfRhtnNVZIJRVJ6a-5IhQ38AAAAA-REAAAXz7q38xg79fkgKVc2unPldyIgAYLKV-2wkH1Oh2djleTpIKSItuqlYk2o4XERaAg; u=3654047068; n=GdP851987340; _lxsdk_s=180dca86092-413-859-e4a%7C%7C28; token2=PzzFlfRhtnNVZIJRVJ6a-5IhQ38AAAAA-REAAAXz7q38xg79fkgKVc2unPldyIgAYLKV-2wkH1Oh2djleTpIKSItuqlYk2o4XERaAg; firstTime=1652969581744; unc=GdP851987340; __mta=176010781.1652155814414.1652967407145.1652969581821.42'
    headers = Headers(cookie)
    bro = RquestData(headers)
    Area(bro)
