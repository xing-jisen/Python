# -*- codeing = utf-8 -*-
# @Time : 2022/5/14 17:30
# @Author : 邢继森
# @File : 淘宝自动搜索.py
# Software : PyCharm
import time

from selenium import webdriver
# 请求
bro = webdriver.Chrome(executable_path='C:\Program Files\Google\Chrome\Application\chromedriver.exe') # 驱动程序路径 打开一个浏览器
bro.get('https://www.taobao.com/')

# 根据ip定位
search_input = bro.find_element_by_id('q')
# 标签交互输入信息
search_input.send_keys('红米k50')

# 执行js代码，向下滚动一屏高度
bro.execute_script('window.scrollTo(0,document.body.scrollHeight)')
time.sleep(2)
# 根据class定位
btn = bro.find_element_by_class_name('btn-search')
time.sleep(5)
# 点击操作
btn.click()

# 对百度发起请求
bro.get('https://www.baidu.com')
time.sleep(2)
# 回退
bro.back()
time.sleep(2)
# 前进
bro.forward()

bro.quit()