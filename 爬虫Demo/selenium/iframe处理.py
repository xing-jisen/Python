# -*- codeing = utf-8 -*-
# @Time : 2022/5/14 18:07
# @Author : 邢继森
# @File : iframe处理.py
# Software : PyCharm
import time

from selenium import webdriver
from selenium.webdriver import ActionChains  # 导入动作链对应的类

bro = webdriver.Chrome(executable_path='C:\Program Files\Google\Chrome\Application\chromedriver.exe')  # 加载驱动程序 打开浏览器
# 请求的url
bro.get('https://www.runoob.com/try/try.php?filename=jqueryui-api-droppable')
# ip定位 如果定位的标签是存在于iframe之中的，则必须通过如下操作在进行标签定位
bro.switch_to.frame('iframeResult')  # 切换浏览器标签定位的作用域，切换作用域，值为作用域的id
# ip标签定位
div = bro.find_element_by_id('draggable')
# print(div)
# 拖动操作  使用动作链
action = ActionChains(bro)
# 点击长按指定的标签
action.click_and_hold(div)
# 拖动五次
for i in range(15):
    # 偏移17px     # perform()立即执行
    # x水平方向 y垂直方向
    action.move_by_offset(17,0).perform()
    time.sleep(0.3)
# 释放动作链
action.release()
time.sleep(3)
# 关闭浏览器
bro.quit()