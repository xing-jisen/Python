# -*- codeing = utf-8 -*-
# @Time : 2022/5/16 21:43
# @Author : 邢继森
# @File : DouYin.py
# Software : PyCharm
import time

import parsel
from selenium.webdriver import ChromeOptions  # 导入实现规避检测的类
from selenium import webdriver
# 实现规避检测
option = ChromeOptions()
option.add_experimental_option('excludeSwitches',['enable-automation'])
from selenium.webdriver import ActionChains  # 导入动作链对应的类

# 如何实现让selenium规避检测到的风险
bro = webdriver.Chrome(executable_path='C:\Program Files\Google\Chrome\Application\chromedriver.exe',options=option)
# 请求的url
bro.get('https://www.douyin.com/?enter=guide')
time.sleep(10)
# print("下一个视频")
# time.sleep(5)
# 发起请求
bro.get('https://www.douyin.com/?enter=guide')
# page_source获取浏览器当前源码数据
page_text = bro.page_source
tree = parsel.Selector(page_text)
a_list = tree.xpath('//*[@id="root"]').get()
print(a_list)
time.sleep(5) # 停留五秒