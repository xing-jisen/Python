# -*- codeing = utf-8 -*-
# @Time : 2022/5/18 13:46
# @Author : 邢继森
# @File : Meituan.py
# Software : PyCharm
import parsel
from faker import Factory
from selenium import webdriver
import time
import requests
from selenium.webdriver.chrome.options import Options # 导入无头浏览器对应类
from selenium.webdriver import ChromeOptions, ActionChains  # 导入实现规避检测的类

# 登录美团
def Log_in():
    # 实现规避检测
    option = ChromeOptions()
    option.add_experimental_option('excludeSwitches', ['enable-automation'])
    # 谷歌浏览器
    bro = webdriver.Chrome(executable_path='C:\Program Files\Google\Chrome\Application\chromedriver.exe',options=option)
    # 发起get请求
    bro.get('https://jn.meituan.com/')
    time.sleep(2)
    login = bro.find_element_by_class_name('growth-entry')
    # 点击操作
    login.click()
    time.sleep(2)
    # 输入账号
    email = bro.find_element_by_id('login-email')
    email.click()    # 点击操作
    email.send_keys('15269095903')    # 标签交互输入账号
    time.sleep(1)
    # 输入密码
    passwd = bro.find_element_by_id('login-password')
    passwd.click()  # 点击操作
    passwd.send_keys('Xjs0912+.')    # 标签交互输入密码
    time.sleep(2)
    # 勾选协议
    agreement = bro.find_element_by_id('user-agreement-wrap-text-circle')
    agreement.click()  # 点击操作
    # 单击确定按钮
    commit = bro.find_element_by_name('commit')
    commit.click()  # 点击操作
    time.sleep(30)
    # 发起get请求
    # page_source获取浏览器当前源码数据
    page_text = bro.page_source
    print(page_text)

    return bro

def Area(bro):
    # 对城市链接发起请求
    bro.get('https://jn.meituan.com/meishi/b139/')
    time.sleep(3)
    bro.get('https://jn.meituan.com/meishi/b139/')
    # page_source获取浏览器当前源码数据
    page_text = bro.page_source

    rell = parsel.Selector(page_text)
    li_list = rell.xpath('//*[@id="app"]/section/div/div[2]/div[2]/div[1]/ul/li')
    for li in li_list:
        title = li.xpath('./div[2]/a/h4/text()').get()
        print(title)

if __name__ == '__main__':
    bro = Log_in()
    Area(bro)
    time.sleep(10)