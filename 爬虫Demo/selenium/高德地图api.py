# -*- codeing = utf-8 -*-
# @Time : 2022/5/20 22:25
# @Author : 邢继森
# @File : 高德地图api.py
# Software : PyCharm
import os # 创建文件夹
import pprint
import re
import time
import random
from faker import Factory
import requests # 网络请求模块
import parsel
import json # jion模块
import sys # 退出模块
from openpyxl import Workbook
import Shandong_province_city as SPC # 城市数据
from selenium import webdriver
from selenium.webdriver.chrome.options import Options  # 导入无头浏览器对应类
from selenium.webdriver import ChromeOptions  # 导入实现规避检测的类


# 获取数据
def get_location(i,key,x,y,sh2,wb1):
    url = 'https://restapi.amap.com/v3/place/text?parameters '
    parameters = {
        'key': 'c09a167d0ec4e7dc9a735ca39156a0e9',
        'location': f'{x},{y}',  # 中心点坐标
        'keywords': key,  # 查询关键字多个用 | 分割
        'types': '餐饮服务',  # 类型 餐饮050000
        # 'city':'济南',# 查询城市
        'radius':'10000',# 查询半径，单位米，默认3000
        'sortrule':'distance',# 排序规则 按距离排序：distance；综合排序：weight
        # 'children':'1',# 按照层级展示子POI数据
        'offset':'25',# 每页记录数据  最大25
        'page':i,# 当前页数
        'extensions':'all',# 返回结果控制all and base
        'output': 'JSON',  # 返回的数据类型
    }
    page_resource = requests.get(url,params=parameters)
    time.sleep(random.randint(1,2))
    text = page_resource.text  # 获得数据是json格式
    # pprint.pprint(text)
    data = json.loads(text)  # 把数据变成字典格式
    # pprint.pprint(data)
    x1 = True
    if (data['count'] == '0'):
        print('没有数据')
        return  x1
        # sys.exit() # 结束
    for i in data['pois']:
        print("店铺名称", i['name'])
        try:
            print("地址", i['address'])
        except:
            i['address'] = '无'
            print("地址", "无")
        print("联系电话", i['tel'])
        print("类型", i['type'])

        print("区域名称", i['pname'], end=" ")
        print("区域名称", i['cityname'], end=" ")
        print("区域名称", i['adname'])

        print("评分", i['biz_ext']['rating'])
        print("人均消费", i['biz_ext']['cost'])
        print("所属商圈", i['business_area'])
        print("\n")
        # 写入数据
        dxt = [
            str(i['name']),# 店铺名称
            str(i['address']),# 地址
            str(i['tel']),# 联系电话
            str(i['type']),# 类型
            str(i['biz_ext']['rating']),# 评分
            str(i['biz_ext']['cost']),# 人均消费
            str(i['business_area']),# 所属商圈
            str(i['pname'] + i['cityname'] + i['adname']),# 区域名称
        ]
        sh2.append(dxt)
    wb1.save(f'.\山东省\{city_}\{city}.xlsx')  # 保存文件


# 获取坐标
def x_y(dis):
    city = dis # input("输入需要查询的城市:")
    url = f'https://apis.map.qq.com/jsapi?qt=geoc&addr={city}&key=UGMBZ-CINWR-DDRW5-W52AK-D3ENK-ZEBRC&output=jsonp&pf=jsapi&ref=jsapi&cb=qq.maps._svcb3.geocoder0'
    page_text = requests.get(url).text
    # print(page_text)
    # 使用正则表达式获取x数据
    phone = re.findall('"pointx":"(.*?)"', page_text)
    x = ','.join(phone)
    # print("\nx", x)
    # 使用正则表达式获取y数据
    phone = re.findall('"pointy":"(.*?)"', page_text)
    y = ','.join(phone)
    # print("\ny", y)
    return x,y,city


if __name__ == '__main__':
    # keyword = input("查询关键字:")
    city_one = [
        '济南', '青岛', '淄博', '枣庄', '东营', '烟台', '潍坊', '济宁', '泰安', '威海', '日照', '莱芜', '临沂', '德州', '聊城', '滨州', '菏泽',
    ]
    county_1 = ['市中区', ]#'历下区', '天桥区', '槐荫区', '历城区', '长清区', '章丘区', '济阳区', '平阴县', '商河县', '莱芜区', '钢城区', '高新区']
    county_2 = ['市南区', '市北区', ]#'四方区', '黄岛区', '崂山区', '李沧区', '城阳区', '胶州市', '即墨市', '平度市', '胶南市', '莱西市', ]
    county_3 = ['淄川区', ]#'张店区', '博山区', '临淄区', '周村区', '桓台县', '高青县', '沂源县', ]
    county_4 = ['市中区', '薛城区', '峄城区', '台儿庄区', '山亭区', '滕州市', ]
    county_5 = ['东营区', '河口区', '垦利县', '利津县', '广饶县', ]
    county_6 = ['芝罘区', '福山区', '牟平区', '莱山区', '龙口市', '莱阳市', '莱州市', '蓬莱市', '招远市', '栖霞市', '海阳市', ]
    county_7 = ['潍城区', '寒亭区', '坊子区', '奎文区', '临朐县', '昌乐县', '青州市', '诸城市', '寿光市', '安丘市', '高密市', '昌邑市', ]
    county_8 = ['市中区', '任城区', '微山县', '鱼台县', '金乡县', '嘉祥县', '汶上县', '泗水县', '梁山县', '曲阜市', '兖州市', '邹城市', ]
    county_9 = ['泰山区', '郊区', '宁阳县', '东平县', '新泰市', '肥城市', ]
    county_10 = ['环翠区', '文登市', '荣成市', '乳山市', ]
    county_11 = ['东港区', '岚山区', '五莲县', '莒县', ]
    county_12 = ['莱城区', '钢城区', ]
    county_13 = ['兰山区', '罗庄区', '河东区', '沂南县', '郯城县', '沂水县', '苍山县', '费县', '平邑县', '莒南县', '蒙阴县', '临沭县', ]
    county_14 = ['德城区', '陵县', '宁津县', '庆云县', '临邑县', '齐河县', '平原县', '夏津县', '武城县', '乐陵市', '禹城市', ]
    county_15 = ['东昌府区', '阳谷县', '莘县', '茌平县', '东阿县', '冠县', '高唐县', '临清市', ]
    county_16 = ['滨州市', '惠民县', '阳信县', '无棣县', '沾化县', '博兴县', '邹平县', ]
    county_17 = ['菏泽市', '曹县', '定陶县', '成武县', '单县', '巨野县', '郓城县', '鄄城县', '东明县', ]
    county = [county_1,county_2,county_3,county_4,county_5,county_6,county_7,county_8,county_9,county_10,county_11,county_12,county_13,county_14,county_15,county_16,county_17]

    # 创建文件夹
    # 如果文件夹不存在
    if not os.path.exists('.\山东省'):
        # 则创建一个文件夹
        os.mkdir('.\山东省')
    for ci in city_one:
        # print(ci)
        # 如果文件夹不存在
        if not os.path.exists(f'.\山东省\{ci}市'):
            # 则创建一个文件夹
            os.mkdir(f'.\山东省\{ci}市')

    # 菜系
    # keyword = [
    #     '麻辣烫', '早餐', '西餐', '烧烤','烤肉', '东北菜', '新疆菜', '西北菜', '香锅烤鱼', '东南亚菜',
    #     '甜点饮品', '自助餐', '粤港菜', '小吃快餐', '家常菜', '素食', '海鲜',
    #     '川菜', '粤菜', '苏菜', '鲁菜', '楚菜', '浙菜', '湘菜', '徽菜', '闽菜', '京菜'
    # ]
    keyword = [
        '烧烤烤肉'
    ]
    for asd in range(0,17):
        print(county[asd])
        # 循环区县
        city_ = city_one[asd] + "市"
        ci = city_one[asd]
        print(city_)
        print(ci)
        time.sleep(5)
        for dis in county[asd]:
            dis = ci + dis
            print(dis)
            # 获取城市坐标
            x, y, city = x_y(dis)
            # 创建表头
            wb1 = Workbook()
            # 删除工作薄
            del wb1["Sheet"]
            # 循环菜系
            for key in keyword:
                # 创建工作簿
                sh2 = wb1.create_sheet(f'{key}')  # 工作薄信息
                one = ['店铺名称', '地址', '联系电话', '类型', '评分', '人均消费', '所属商圈', '区域名称']
                # 写入表头
                sh2.append(one)
                # 保存文件
                wb1.save(f'.\山东省\{city_}\{city}.xlsx')  # 保存文件
                # 内循环循环页码
                for i in range(1,101):
                    print("第",i,"页" + key)
                    x1 = get_location(i,key,x,y,sh2,wb1)
                    # 没有数据可获取就跳出循环
                    if(x1 == 1):
                        break

            # workbook.save(f'{city}.xlsx')