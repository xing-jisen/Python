# -*- codeing = utf-8 -*-
# @Time : 2022/5/12 20:57
# @Author : 邢继森
# @File : Microblog.py
# Software : PyCharm
import time
import re

import requests
import parsel
from faker import Factory

# 随机U_A
f = Factory.create()
UA = f.user_agent()
# U_A伪装
headers = {
    'User-Agent': UA,
    'Cookie':'uuid=38C46230D47B7C848A626E3E8BC85F990A16BFB69C5D49DAA1ADB28BA59C1019; iuuid=38C46230D47B7C848A626E3E8BC85F990A16BFB69C5D49DAA1ADB28BA59C1019; _lxsdk_cuid=18083c1a7175f-0e5a2385e47293-3e604809-144000-18083c1a718c8; _lxsdk=38C46230D47B7C848A626E3E8BC85F990A16BFB69C5D49DAA1ADB28BA59C1019; _hc.v=72fd1102-c6e6-3ea5-b165-79d470814b4a.1651477949; fspop=test; cy=22; cye=jinan; s_ViewType=10; WEBDFPID=x045x4977x6x5588y007w91uvzzu8360819y1z4809897958u337191u-1652451669165-1652365267749KIEEOACfd79fef3d01d5e9aadc18ccd4d0c95072939; dper=ca2ab48b27ccae7676da39f155610b099138165a904d1c50759111565f954d82b2fbcb56c3d289bd58d3043ba03e905cd29f1d5886c758874a170625192cd37a; ll=7fd06e815b796be3df069dec7836c3df'
}
for i in range(1,51):

    url = f'http://www.dianping.com/jinan/ch10/r12042p{i}'
    page_text = requests.get(url=url,headers=headers)
    # time.sleep(5)
    # print(page_text.text)

    # 使用xpath获取数据
    rell = parsel.Selector(page_text.text)
    # li_list = rell.xpath('//div[@class="content-wrap"]/div[1]/div[2]/ul/li')
    li_list = rell.xpath('//*[@id="shop-all-list"]/ul/li')
    print(li_list)
    for li in li_list:
        name = li.xpath('./div[2]/div[1]/a/h4/text()').get()
        print(name)