# -*- codeing = utf-8 -*-
# @Time : 2022/4/30 17:00
# @Author : 邢继森
# @File : 微博热搜.py
# Software : PyCharm
import time

import requests
import parsel
import pymysql
# 生成随机数的函数randint
from random import randint
# 设置睡眠时间，即多久运行一次
from time import sleep

# U_A伪装
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
}


# 数据库连接
def Link():
    host = '127.0.0.1'  # 主机名
    user = 'root'  # 数据库用户名
    password = '123456'  # 数据库密码
    database = 'microblog'  # 数据库名称
    db = pymysql.connect(host=host, user=user, password=password, db=database)  # 建立连接
    # 打开数据库连接
    # 这4个参数依次是：主机名，用户名，密码和数据库名
    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()
    # 使用 execute()  方法执行 SQL 查询
    cursor.execute("SELECT VERSION()")

    # 使用 fetchone() 方法获取单条数据.
    data = cursor.fetchone()

    print("Database version : %s " % data)
    return cursor, db


# 创建数据表
def Set_up(cursor):
    s = input("输入数据表名称:")
    # 使用 execute() 方法执行 SQL，如果表存在则删除
    cursor.execute(f"DROP TABLE IF EXISTS {s}")
    # 使用预处理语句创建表
    sql = f"""CREATE TABLE `{s}` (
      `id` int(8) NOT NULL AUTO_INCREMENT,  # 序号
      `title` varchar(255) DEFAULT NULL,    # 内容
      `heat` varchar(255) DEFAULT NULL,     # 热度
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"""

    cursor.execute(sql)
    print(s,"数据表创建成功")


# 写入数据_热搜榜
def Insert_Top_search(cursor, db, b, c):
    # index = a
    title = b
    heat = c

    sql = "REPLACE INTO Top_search(title,heat) VALUES ('%s', '%s')" % ( title, heat)

    try:
        # 执行sql语句
        cursor.execute(sql)
        # 执行sql语句
        db.commit()
    except:
        # 发生错误时回滚
        db.rollback()
        # print("错误")

# 写入数据_话题榜
def Insert_Topic(cursor, db,  title, heat):
    # index = index
    title = title
    heat = heat

    sql = f"INSERT INTO Topic(title,heat) VALUES ( '%s', '%s')" % ( title, heat)

    try:
        # 执行sql语句
        cursor.execute(sql)
        # 执行sql语句
        db.commit()
    except:
        # 发生错误时回滚
        db.rollback()


# 写入数据_新时代
def Insert_New_era(cursor, db,title, heat):
    # index = index
    title = title
    heat = heat

    sql = f"INSERT INTO New_era(title,heat) VALUES ('%s', '%s')" % (title, heat)

    try:
        # 执行sql语句
        cursor.execute(sql)
        # 执行sql语句
        db.commit()
    except:
        # 发生错误时回滚
        db.rollback()


# 写入数据_电影榜
def Insert_List_of_movie(cursor, db, title, heat):
    # index = index
    title = title
    heat = heat

    sql = f"INSERT INTO List_of_movie(title,heat) VALUES ('%s', '%s')" % ( title, heat)

    try:
        # 执行sql语句
        cursor.execute(sql)
        # 执行sql语句
        db.commit()
    except:
        # 发生错误时回滚
        db.rollback()


# 热搜榜
def Top_search():
    # 访问地址
    url = 'https://tophub.today/n/KqndgxeLl9'
    # 请求地址
    page_text = requests.get(url=url, headers=headers).text
    # print(page_text)
    # 数据解析
    tree = parsel.Selector(page_text)
    title_list = tree.xpath('//table[@class="table"]/tbody/tr')
    for i, tr in enumerate(title_list):
        # 序号
        index = tr.xpath('./td[1]/text()').get()
        # 内容
        title = tr.xpath('./td[2]/a/text()').get()
        # title_url = tr.xpath('./td[2]/a/@href').get()
        # 热度
        heat = tr.xpath('./td[3]/text()').get()
        print(index, title, heat)
        # 写入数据
        Insert_Top_search(cursor,db, title, heat)
        if i + 1 == 50:
            break


# 话题榜
def Topic():
    # 访问地址
    url = 'https://tophub.today/n/VaobJ98oAj'
    # 请求地址
    page_text = requests.get(url=url, headers=headers).text
    # print(page_text)
    # 数据解析
    tree = parsel.Selector(page_text)
    title_list = tree.xpath('//table[@class="table"]/tbody/tr')
    for i, tr in enumerate(title_list):
        # 序号
        index = tr.xpath('./td[1]/text()').get()
        # 内容
        title = tr.xpath('./td[2]/a/text()').get()
        # title_url = tr.xpath('./td[2]/a/@href').get()
        # 热度
        heat = tr.xpath('./td[3]/text()').get()
        print(index, title, heat)
        # 写入数据
        Insert_Topic(cursor, db, title, heat)
        if i + 1 == 50:
            break


# 新时代
def New_era():
    # 访问地址
    url = 'https://tophub.today/n/Om4ejl3vxE'
    # 请求地址
    page_text = requests.get(url=url, headers=headers).text
    # print(page_text)
    # 数据解析
    tree = parsel.Selector(page_text)
    title_list = tree.xpath('//table[@class="table"]/tbody/tr')
    for i, tr in enumerate(title_list):
        # 序号
        index = tr.xpath('./td[1]/text()').get()
        # 内容
        title = tr.xpath('./td[2]/a/text()').get()
        # title_url = tr.xpath('./td[2]/a/@href').get()
        # 热度
        heat = tr.xpath('./td[3]/text()').get()
        print(index, title, heat)
        # 写入数据
        Insert_New_era(cursor, db, title, heat)
        if i + 1 == 50:
            break


# 电影榜
def List_of_movie():
    # 访问地址
    url = 'https://tophub.today/n/DOvnNXqvEB'
    # 请求地址
    page_text = requests.get(url=url, headers=headers).text
    # print(page_text)
    # 数据解析
    tree = parsel.Selector(page_text)
    title_list = tree.xpath('//table[@class="table"]/tbody/tr')
    for i, tr in enumerate(title_list):
        # 序号
        index = tr.xpath('./td[1]/text()').get()
        # 内容
        title = tr.xpath('./td[2]/a/text()').get()
        # title_url = tr.xpath('./td[2]/a/@href').get()
        # 热度
        heat = tr.xpath('./td[3]/text()').get()
        print(index, title, heat)
        # 写入数据
        Insert_List_of_movie(cursor, db, title, heat)
        if i + 1 == 20:
            break


if __name__ == "__main__":
    for i in range(0,10):
        # 数据库连接
        cursor, db = Link()
        # 热搜榜
        Top_search()
        # 话题榜
        Topic()
        # 新时代
        New_era()
        # 电影榜
        List_of_movie()

        time.sleep(1)
        print("十分钟之后继续运行!")
        # 放到函数里即可，程序执行到这里会进行停顿
        sleep(randint(599, 600))  # 十分钟运行一次
        time.sleep(1)
        print("继续运行!")

    # 创建数据表
    # Set_up(cursor)
