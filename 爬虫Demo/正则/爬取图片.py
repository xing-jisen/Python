# -*- codeing = utf-8 -*-
# @Time : 2022/4/17 14:34
# @Author : 邢继森
# @File : 爬取图片.py
# Software : PyCharm

import requests
import re
# 获取url
url = 'https://www.nipic.com/topic/show_27202_1.html'
# url = 'http://img.daimg.com/uploads/allimg/220409/3-220409155108.jpg'
# conteng返回的是二进制形式数据
# text（字符串） content（二进制）json()（对象）
# UA伪装
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
}
# 获取相应数据
page_text = requests.get(url=url,headers=headers).text

# 使用聚焦爬虫将页面中所有图片进行解析
# 编写正则表达式
# ex = '<ul class"ibox2_list"><li><a><img src="(.*?)" alt.*?></a></li></ul>'
# ex = '<li> <a target="_blank"> <img title="(.*?)" src="(.*?)" alt.*?> </a> </li>'
ex = '<li class="new-search-works-item"> .*? <img src="(.*?)" alt.*?> .*?</li>'
# 正则表达式匹配到page_text     re.S单行匹配re.M多行匹配
img_src_list = re.findall(ex,page_text,re.S)
print(img_src_list)

