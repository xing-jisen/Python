# -*- codeing = utf-8 -*-
# @Time : 2022/4/24 22:40
# @Author : 邢继森
# @File : requests基本使用.py
# Software : PyCharm
import requests

# UA伪装
headers = {
    'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1Safari/534.50'
}
url = 'http://www.baidu.com'

res = requests.get(url,headers=headers)

print(res.encoding)
print(res.headers) # 里面没有Content-Type，encoding=utf-8   否则设置charset的话，就已设置的为准  否则就是ISO-8859-1
print(res.url)

# 设置编码
res.encoding = "utf-8"
print(res.encoding)
print(res.text)