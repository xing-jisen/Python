# -*- codeing = utf-8 -*-
# @Time : 2022/5/2 21:39
# @Author : 邢继森
# @File : Lixia_District______.py
# Software : PyCharm
import datetime
import pprint
import zlib
import re
import parsel
import requests  # 代替浏览器进行网络请求
import json  # json字典 方便取值
from concurrent.futures import ThreadPoolExecutor  # 开启多线程模式
import random  # 随机数生成
import time  # 导入time模块做定时休眠
import base64  # 加密
from faker import Factory
from openpyxl import Workbook
from selenium import webdriver
from selenium.webdriver import ChromeOptions

def Headers(cookie):
    # 随机U_A
    f = Factory.create()
    UA = f.user_agent()
    # U_A伪装
    headers = {
        'User-Agent': UA,
        'Accept': 'application/json',
        'Host': 'jn.meituan.com',
        'Referer': 'https://jn.meituan.com/meishi/b139/pn1/',
        'Cookie': cookie,
    }
    return headers


# 解析网页
def RquestData(token, i,headers,wb1,sh2,sh3,sh4):
    # 代理IP
    proxie = ''
    # 请求地址
    url = 'https://jn.meituan.com/meishi/api/poi/getPoiList?'
    # 请求参数
    params = {
        'cityName': '济南',
        'cateId': '0',
        'areaId': '139',
        'sort': '',
        'dinnerCountAttrId': '',
        'page': f'{i}',
        'userId': '3606710339',
        'uuid': '99a987f9ca6b495d826f.1652408097.1.0.0',
        'platform': '1',
        'partner': '126',
        'originUrl': f'https://jn.meituan.com/meishi/b139/pn{i}/',
        'riskLevel': '1',
        'optimusCode': '10',
        '_token': token
    }
    response = requests.get(url, headers=headers,params=params).text
    # 每请求一次随机休眠2-3s
    time.sleep(random.randint(3, 4))

    # json字符串转字典格式       json.dumps  字典格式转json格式
    # 去掉用不到的东西
    # result = response.replace("{'data': {'poiInfos': [",'').replace('''],
    #           'totalCounts': 1000},
    #  'status': 0}''','') # 把内容替换掉，替换成空
    # 转格式
    result = json.loads(response)
    # print(response)

    # 格式化输出
    # pprint.pprint(result)

    for i in result['data']['poiInfos']:
        # print(i)

        # 详情页url
        poiId = i['poiId']
        poiId_url = f'https://jn.meituan.com/meishi/{poiId}/'

        print('\t店铺名称:', i['title'], end='')
        print('\t评分:', i['avgScore'], end='')
        print('\t评论:', i['allCommentNum'], end='')
        print('\t地址:', i['address'], end='')
        print('\t人均价格:', i['avgPrice'], end='')
        # 套餐
        for x,j in enumerate(i['dealList']):
            print(f'\t套餐{x}:',j['title'],end='')
            print(f'\t套餐价格{x}:',j['price'])
            xhr = [i['title'],j['title'],j['price']]
            sh4.append(xhr)
            wb1.save('Lixia_District.xlsx')  # 保存文件
        # 写入数据
        dxt = [
            i['title'],
            i['avgScore'],
            i['allCommentNum'],
            i['address'],
            i['avgPrice']
        ]
        sh2.append(dxt)
        wb1.save('Lixia_District.xlsx')  # 保存文件
        title = i['title']
        # 返回详情页url获得详情页数据
        Detail_page(poiId_url,headers,wb1,sh3,title,proxie)
        # 返回url实现自动获取评论
        # Appraise(poiId,poiId_url,headers)
    # 每请求一次随机休眠2-3s
    time.sleep(random.randint(2, 3))

def sele_html(url):
    global fox
    print(url)
    fox.get(url)
    time.sleep(3)
    html=fox.page_source
    return html
# 详情页店铺信息
def Detail_page(poiId_url,headers,wb1,sh3,title,proxie):
    # 代理IP
    # proxies = {
    #     "http": "https://" + proxie
    # }
    print(proxie)
    # 请求参数
    # print(poiId_url)
    #page_text = requests.get(url='', headers=headers, proxy=proxie).text
    page_text = sele_html(poiId_url)
    # 每请求一次随机休眠5-30s
    time.sleep(random.randint(3, 13))
    # print(page_text.text)
    # 使用正则表达式获取数据
    phone = re.findall('"phone":"(.*?)"', page_text)
    phone = ','.join(phone)
    print("\n电话", phone)

    address = re.findall('"address":"(.*?)"', page_text)
    address = ','.join(address)
    print("地址", address)

    openTime = re.findall('"openTime":"(.*?)"', page_text)
    openTime = ','.join(openTime)
    print("营业时间", openTime)

    text = re.findall('"text":"(.*?)"', page_text)
    text = ','.join(text)
    print("特点", text)

    # 写入数据
    dxt = [
        title,
        phone,
        address,
        openTime,
        text
    ]
    sh3.append(dxt)
    wb1.save('Lixia_District.xlsx')  # 保存文件



# 详情页评价部分
def Appraise(poiId,poiId_url,headers):
    url = 'https://jn.meituan.com/meishi/api/poi/getMerchantComment'
    # 请求参数
    params = {
        'uuid': 'd10defae57cc40a383a9.1651721567.1.0.0',
        'platform': 1,
        'partner': '126',
        'originUrl': poiId_url,
        'riskLevel': '1',
        'optimusCode': '10',
        'id': poiId,
        'userId': '2588879467',
        'offset': '0',
        'pageSize': '10',
        'sortType': '1'
    }
    response = requests.get(url=url, headers=headers, params=params).text#,proxies=proxy
    # 转成json格式
    result = json.loads(response)
    # 格式化输出
    # pprint.pprint(result)
    # 评价以及数量
    # for i in result['data']['tags']:
    #     print(i['tag'], end="")
    #     print(i['count'])

    # 总评价数量
    # print(result['data']['total'])
    # # 详细评价
    # for j in result['data']['comments']:
    #     # 昵称
    #     print(j['userName'], end=":")
    #     # 评价语
    #     print(j['comment'])
    #     # 头像
    #     print("头像:",j['userUrl'])
    #     # 配图
    #     for x in j['picUrls']:
    #         print("配图:",x['url'])
    # 每请求一次随机休眠2-3s
    time.sleep(random.randint(2, 3))

# token值获取
def Token():
    # time1 = js2py.eval_js('new Date().getTime();')
    # data = 'eJxVjtluozAUht/Ft6DYBuxApF5AIIQkE1qWbKNeELYs4IQlJfVo3n1cqSPNSEf6l/Nd/L9A62VgghEyEJLBR96CCcAjNKJABn0nPpRgommYUoJUGaT/dQbBoju2GxtMfmKCZaor719FILIoVCrrVHuX/7GKJu6L8QQCTn1/7yYQXtiozs/9I2Gj9FZD4bvTGR6xakCxAwi+jgQv9Pqtybf2f/MPMVyw3blkwuWLYXOJe3/gZhwURVjuC2ntsd6b16Z/XUzZ0TGdsYRaPVAOtrHbn5zhEMbdNEssN1MuDeyIZK6kZVRi7vKnZaFqS+ycczqVoCrBV6bty1lUstVylTmXOL8eKlZMFf92t1Rr2VV8KB91dfTJsl+nc29LFwGymtcPpwoMl+wqVzdO0MoYvLchnUkNiQJdVXjmJ6H0VIaiVex5sff7LP1czbZaAZE/q1oePZsBt7ZmJuOoS9y31AufbBfPb7wKr0azXviH9SZLU8Y0c4jHoZa+gN9/AEzDkKE='
    data = 'eJx1T8tyokAU/ZfeQtnNq9t2h1HCQ3oIClKmsgAlgDxEQIxMzb9PpypTlSxmdR733FP3/gaddQILCSGKkAjGtAMLIM3QDAMRDD2fYE1WKSZIoQoVwfGHRxRZFUHShSuweNUkVSRYfvs0fK5fJU3B4hyrb+I3yhdk9TNj8QjIh6HtFxCem1mdFsMtbmbHSw057/MC8hP+EwC8od7xBo7lF8ZfOPzTLn+FV/RF1nCW2vfqHEi3+0N/yVNhuwx6N691sr701ytqw9MZ6e6BlUHm7le2gQszTkrBt8iucqpTRKjnC6zAqxSG+56Vvqeb7VzQn2QGTTqHURPV7vrdEqQgG/1NVe2Ni/3yUIjmGs493Dy780QZWbZBYStHUco8fbk9HUzPibbscQ/I0LWTM+1s6SkeiC4lBkqbav1rcrXYY+S8RF1R7uwk1EcKH1k2bR0z+HhujQTK2QddH3OlxHS8ljQtAo3afTqhA9tjTbOczeRT41bW7+DPX9EAlHA='
    token_decode = base64.b64decode(data)  # b64解码
    token_string = zlib.decompress(token_decode)  # 二进制进行压缩
    str1 = str(token_string, 'utf-8')
    # sign 请求参数
    sign = eval(str1)
    sign['ts'] = int(time.time() * 1000)
    sign['cts'] = int(time.time() * 1000) + 100  # 跟ts相差一百左右
    info = str(sign).encode()
    token = base64.b64encode(zlib.compress(info)).decode()
    time.sleep(random.randint(1,2))
    return token




# 提取IP
def IP():
    url = 'http://tiqu.pyhttp.taolop.com/getip?count=1&neek=30373&type=2&yys=0&port=2&sb=&mr=1&sep=0&ts=1&time=4'

    browser = requests.get(url=url).text
    # 转成json格式
    result = json.loads(browser)
    # pprint.pprint(result)

    for i in result['data']:
        # print(i)
        ip = i['ip']  # ip
        port = i['port']  # 端口
        expire_time = i['expire_time']  # 到期时间
        ip_port = str(ip) + ":" + str(port)
        print("当前IP",ip_port)
        # print(ip_port)
        # expire_time = expire_time[10:16]
        #
        print("过期时间", expire_time)
        # expire_time1 = int(expire_time[0:3]) * 60 + int(expire_time[4:6])
        # print("过期分钟", expire_time1)
        now_time = datetime.datetime.now()
        print("当前时间",now_time)
        # now_time = datetime.datetime.now().strftime('%H:%M')
        # print("当前时间", now_time)
        # now_time = (datetime.datetime.now() + datetime.timedelta(minutes=30)).strftime("%H:%M")
        # print("当前时间+25分钟", now_time)
        #
        # # 当前分钟
        # now_time1 = int((datetime.datetime.now() + datetime.timedelta()).strftime("%H")) * 60 + int(
        #     (datetime.datetime.now() + datetime.timedelta()).strftime("%M"))
        # now_time = int((datetime.datetime.now() + datetime.timedelta()).strftime("%H")) * 60 + int(
        #     (datetime.datetime.now() + datetime.timedelta(minutes=25)).strftime("%M"))
        # print("当前分钟", now_time1,now_time)

        return ip_port


def Collect(cookie):
    global fox
    # 创建表头
    wb1 = Workbook()
    # 创建工作簿
    sh2 = wb1.create_sheet('首页数据')  # 工作薄信息
    sh3 = wb1.create_sheet('详情页数据')  # 工作薄信息
    sh4 = wb1.create_sheet('首页店铺推荐食品')  # 工作薄信息
    one = ['店铺名称','评分','评论','地址','人均价格']
    two = ['店铺名称','电话','地址','营业时间','特点']
    three = ['店铺名称','套餐','价格']
    # 写入表头
    sh2.append(one)
    sh3.append(two)
    sh4.append(three)

    # 删除工作薄
    del wb1["Sheet"]
    wb1.save('Lixia_District.xlsx')  # 保存文件
    # 激活工作表
    # sh2 = wb1['首页数据']

    for i in range(1, 68):
        print(f"第{i}页")
        option = ChromeOptions()
        proxie=IP()
        option.add_experimental_option('excludeSwitches', ['enable-automation'])  # 修改驱动属性
        option.add_argument("--proxy-server=https://" + proxie)  # ip代理
        # option.add_argument('--headless')  # 无头浏览模式
        fox = webdriver.Chrome(options=option)  # 创建浏览器
        fox.get('https://jn.meituan.com/meishi/5357595/')
        for i in cookie.split(';'):
            key = i.split('=')[0].strip()
            value = i.split('=')[1].strip()
            fox.add_cookie({'name': key, 'value': value})
        fox.get('https://jn.meituan.com/meishi/5357595/')

        time.sleep(2)
        headers = Headers(cookie)
        fox.get('https://jn.meituan.com/meishi/5357595/')
        # token
        token = Token()
        # 数据获取
        RquestData(token, i, headers,wb1,sh2,sh3,sh4)
        print('\n\n\n')
        # 每请求一次随机休眠10-60s
        time.sleep(random.randint(10, 60))
        fox.close()

if __name__ == "__main__":
    cookie = '_lxsdk_cuid=180a8f21c32c8-0701bea909dc82-9771a39-144000-180a8f21c33c8; cityname=%E6%BD%8D%E5%9D%8A; iuuid=0BD85904319FFA99D91F20769ADABEDD046E7CD7F5B1DC705B5BB694C11FAD10; _lxsdk=0BD85904319FFA99D91F20769ADABEDD046E7CD7F5B1DC705B5BB694C11FAD10; webp=1; i_extend=H__a100002__b1; __utma=74597006.1893403857.1652101887.1652101887.1652101887.1; __utmz=74597006.1652101887.1.1.utmcsr=baidu|utmccn=(organic)|utmcmd=organic; ci=96; rvct=96; _hc.v=e051f322-e17d-eea6-96d7-c26c641a0df4.1652158408; _lx_utm=utm_source%3Dbaidu%26utm_medium%3Dorganic; mtcdn=K; client-id=823d6d22-d091-42fb-a930-3962c270c54c; uuid=e34376509a3646b5b794.1652967404.1.0.0; lt=PzzFlfRhtnNVZIJRVJ6a-5IhQ38AAAAA-REAAAXz7q38xg79fkgKVc2unPldyIgAYLKV-2wkH1Oh2djleTpIKSItuqlYk2o4XERaAg; u=3654047068; n=GdP851987340; _lxsdk_s=180dca86092-413-859-e4a%7C%7C28; token2=PzzFlfRhtnNVZIJRVJ6a-5IhQ38AAAAA-REAAAXz7q38xg79fkgKVc2unPldyIgAYLKV-2wkH1Oh2djleTpIKSItuqlYk2o4XERaAg; firstTime=1652969581744; unc=GdP851987340; __mta=176010781.1652155814414.1652967407145.1652969581821.42'
    Collect(cookie)
