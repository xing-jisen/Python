# -*- codeing = utf-8 -*-
# @Time : 2022/5/2 21:39
# @Author : 邢继森
# @File : Lixia_District______.py
# Software : PyCharm
import datetime
import os
import pprint
import zlib
import re
import parsel
import requests  # 代替浏览器进行网络请求
import json  # json字典 方便取值
from concurrent.futures import ThreadPoolExecutor  # 开启多线程模式
import random  # 随机数生成
import time  # 导入time模块做定时休眠
import base64  # 加密
from faker import Factory
from openpyxl import Workbook


def Headers(cookie):
    # 随机U_A
    f = Factory.create()
    UA = f.user_agent()
    # U_A伪装
    headers = {

        'Accept': 'application/json',
        'Cookie': cookie,
        'Host': 'jn.meituan.com',
        'User-Agent': UA,
    }
    return headers

# 解析网页
def RquestData(token, i,headers,wb1,sh2,sh4,page,Name,userId):
    # 请求地址
    url = 'https://jn.meituan.com/meishi/api/poi/getPoiList?'
    # 请求参数
    params = {
        'cityName': '济南',
        'cateId': '0',
        'areaId': f'{page}',
        'sort': '',
        'dinnerCountAttrId': '',
        'page': f'{i}',
        'userId': f'{userId}',
        'uuid': '1cae1bc54c864ba9aca6.1652504048.1.0.0',
        'platform': '1',
        'partner': '126',
        'originUrl': f'https://jn.meituan.com/meishi/b{page}/pn{i}/',
        'riskLevel': '1',
        'optimusCode': '10',
        '_token': token
    }
    while True:
        response = requests.get(url,headers=headers,params=params).text # session() 保持登陆状态
        print(response)
        # 每请求一次随机休眠2-3s
        time.sleep(random.randint(3, 5))
        try:
            result = json.loads(response)
            o=result['data']
            break
        except:
            print('重新请求中...')
            print('请点击链接进行验证')
            pass
        time.sleep(20)
        # 等待时间需要手动点击下方链接验证
    # print(response)

    # print(response)
    # json字符串转字典格式       json.dumps  字典格式转json格式
    # 去掉用不到的东西
    # result = response.replace("{'data': {'poiInfos': [",'').replace('''],
    #           'totalCounts': 1000},
    #  'status': 0}''','') # 把内容替换掉，替换成空
    # 转格式
    # result = json.loads(response)
    # print(response)

    # 格式化输出
    # pprint.pprint(result)

    for i in result['data']['poiInfos']:
        # print(i)

        print('\t店铺名称:', i['title'], end='')
        print('\t评分:', i['avgScore'], end='')
        print('\t评论:', i['allCommentNum'], end='')
        print('\t地址:', i['address'], end='')
        print('\t人均价格:', i['avgPrice'])
        # 写入数据
        dxt = [
            i['title'],
            i['avgScore'],
            i['allCommentNum'],
            i['address'],
            i['avgPrice']
        ]
        sh2.append(dxt)
        # 套餐
        for x, j in enumerate(i['dealList']):
            print(f'\t套餐{x + 1}:', j['title'], end='')
            print(f'\t套餐价格{x + 1}:', j['price'])

            xhr = [i['title'], j['title'], j['price']]
            sh4.append(xhr)

        wb1.save(f'{Name}_District.xlsx')  # 保存文件
        # 每请求一次随机休眠2-3s
        #time.sleep(random.randint(3, 4))



# token值获取
def Token(data):
    # time1 = js2py.eval_js('new Date().getTime();')
    # data = 'eJxVjtluozAUht/Ft6DYBuxApF5AIIQkE1qWbKNeELYs4IQlJfVo3n1cqSPNSEf6l/Nd/L9A62VgghEyEJLBR96CCcAjNKJABn0nPpRgommYUoJUGaT/dQbBoju2GxtMfmKCZaor719FILIoVCrrVHuX/7GKJu6L8QQCTn1/7yYQXtiozs/9I2Gj9FZD4bvTGR6xakCxAwi+jgQv9Pqtybf2f/MPMVyw3blkwuWLYXOJe3/gZhwURVjuC2ntsd6b16Z/XUzZ0TGdsYRaPVAOtrHbn5zhEMbdNEssN1MuDeyIZK6kZVRi7vKnZaFqS+ycczqVoCrBV6bty1lUstVylTmXOL8eKlZMFf92t1Rr2VV8KB91dfTJsl+nc29LFwGymtcPpwoMl+wqVzdO0MoYvLchnUkNiQJdVXjmJ6H0VIaiVex5sff7LP1czbZaAZE/q1oePZsBt7ZmJuOoS9y31AufbBfPb7wKr0azXviH9SZLU8Y0c4jHoZa+gN9/AEzDkKE='# 15269095903手机号加密
    data = data
    token_decode = base64.b64decode(data)  # b64解码
    token_string = zlib.decompress(token_decode)  # 二进制进行压缩
    str1 = str(token_string, 'utf-8')
    # sign 请求参数
    sign = eval(str1)
    sign['ts'] = int(time.time() * 1000)
    sign['cts'] = int(time.time() * 1000) + 1000  # 跟ts相差一百左右
    info = str(sign).encode()
    token = base64.b64encode(zlib.compress(info)).decode()
    time.sleep(random.randint(1,2))
    return token



def Collect(cookie):
    print('''
        历下 139  68 市中 140  68 槐荫 141 68 天桥 142 68  历城 143 68 长清 144 31
        章丘 3120 44 平阴 3121 8 济阳 3122 17 商河 3123 10
        莱芜 3231 33 钢城 3232 7
        高新 5900 34
    ''')
    page = input("请输入Page:")
    name = input("请输入Name:")
    number = int(input("请输入页码:"))

    # 创建表头
    wb1 = Workbook()
    # 创建工作簿
    sh2 = wb1.create_sheet('首页数据')  # 工作薄信息
    # sh3 = wb1.create_sheet('详情页数据')  # 工作薄信息
    sh4 = wb1.create_sheet('首页店铺推荐食品')  # 工作薄信息
    one = ['店铺名称','评分','评论','地址','人均价格']
    three = ['店铺名称','套餐','价格']
    # 写入表头
    sh2.append(one)
    sh4.append(three)

    # 删除工作薄
    del wb1["Sheet"]
    wb1.save(f'{name}_District.xlsx')  # 保存文件
    # 激活工作表
    # sh2 = wb1['首页数据']
    # 每个城市的页码
    for i in range(1,number):
        print(f"第{i}页")
        headers = Headers(cookie)
        # 美团token加密原始数据，手机号不同需要重新获取
        # data = 'eJxVjtluozAUht/Ft6DYBuxApF5AIIQkE1qWbKNeELYs4IQlJfVo3n1cqSPNSEf6l/Nd/L9A62VgghEyEJLBR96CCcAjNKJABn0nPpRgommYUoJUGaT/dQbBoju2GxtMfmKCZaor719FILIoVCrrVHuX/7GKJu6L8QQCTn1/7yYQXtiozs/9I2Gj9FZD4bvTGR6xakCxAwi+jgQv9Pqtybf2f/MPMVyw3blkwuWLYXOJe3/gZhwURVjuC2ntsd6b16Z/XUzZ0TGdsYRaPVAOtrHbn5zhEMbdNEssN1MuDeyIZK6kZVRi7vKnZaFqS+ycczqVoCrBV6bty1lUstVylTmXOL8eKlZMFf92t1Rr2VV8KB91dfTJsl+nc29LFwGymtcPpwoMl+wqVzdO0MoYvLchnUkNiQJdVXjmJ6H0VIaiVex5sff7LP1czbZaAZE/q1oePZsBt7ZmJuOoS9y31AufbBfPb7wKr0azXviH9SZLU8Y0c4jHoZa+gN9/AEzDkKE='
        data = 'eJxVjl1vokAUhv/L3JbIgMCISS+oVRktVSgi2PQCEHSAQTqMfHSz/32nSTfZTU7yfpzn4v0FGD6DuQKhCaEEuoyBOVAmcGIACfBWfAxd1aGmTY2ZYUog/a8zIVIkkLDgGczfdUWTkKF+fBeeyO+KPjWkmaF9SP9YVRP3zWCBgCvnTTuX5aKe0Izwe1xP0huVhW+vRBYTgECpL1Ch5Y/GP8r/ZkdsFmxLLrVw2aavCl+5W+PSvWYyuTTb1SriwwYzHMQ+Dl2tP5FUHd0ti5LFwR3fuP+ACzVL8SFmUyOrzQRbbkjjzrOe17lWzM7RYinXNrqHZj54w5fHP8OqoycaNutqExRxHeSnzovJ4WVY7Z3kJUdO9xrXUV5gh9rlU0XX5S3ZmV5vt+m2WaGba0LaeWN3fMqJedjd1CFnOrme0x0/L9DyyB726dRvDR2lKGLHC/+qNNu1oVP29Y6SfYvxIlBLVDS4H3362TSvbGZVEL1V1iP4/QdCEpFB'
        # token
        token = Token(data)
        # print(token)
        # 市中 140  68 槐荫 141 68 天桥 142 68  历城 143 68 长清 144 31
        # 章丘 3120 44 平阴 3121 8 济阳 3122 17 商河 3123 10
        # 莱芜 3231 33 钢城 3232 7
        # 高新 5900 34
        page = page
        Name = name

        # 每个账号的userId不同
        userId = '2588879467'
        # 数据获取 返回详情页url
        RquestData(token,i,headers,wb1,sh2,sh4,page,Name,userId)
        print('\n\n\n')
        # 每请求一次随机休眠10-60s
        # time.sleep(random.randint(10, 60))


if __name__ == "__main__":
    cookie = '_lxsdk_cuid=180262350d5c8-01808edd4911fa-3e604809-149c48-180262350d5c8; iuuid=38C46230D47B7C848A626E3E8BC85F990A16BFB69C5D49DAA1ADB28BA59C1019; i_extend=H__a100002__b1; _hc.v=0269efac-b3fd-159e-63af-1b3a27ec8b10.1651474918; _lxsdk=38C46230D47B7C848A626E3E8BC85F990A16BFB69C5D49DAA1ADB28BA59C1019; cityname=%E6%B5%8E%E5%8D%97; webp=1; __utma=74597006.2038297276.1651413476.1651413476.1651673951.2; __utmz=74597006.1651673951.2.2.utmcsr=baidu|utmccn=(organic)|utmcmd=organic|utmctr=%E7%BE%8E%E5%9B%A2; mtcdn=K; client-id=7a7d5465-7c8b-41f7-b349-1f8d548a07df; _lx_utm=utm_source%3DBaidu%26utm_medium%3Dorganic; ci=96; rvct=96%2C727%2C176%2C1; uuid=1cae1bc54c864ba9aca6.1652504048.1.0.0; __mta=147594760.1649907319730.1652496872993.1652504049525.99; userTicket=RvHoFoCNSOwMAVmlPftZnpIVuAbFrZOaEQtpBGyV; _yoda_verify_resp=YNWlgNQM45U6KxY0a3IDKnLxoJG5afaQWLGlLu3z%2FCLEu58F3oqhq05smIoooDen6qDOfrvUHxGqgwxBBkwH9UcwbiCFueU1nK5zoAakfkaPPgdN3j%2FL%2BkTNaYR2bDX%2B%2Bn%2BQkmLQKnOMZLToT39tHAizmdH8tfHmlB0nyLIGqj04Rh8OK%2FjijkX8wZ0oB7jgO4hA1LZUHGH0mQFGsAU8J%2BzAU6rPrSX1CjT2vf%2FvZZ2oDwP3%2Bj48fSuNmALhU0JIuEnB6eP0GnSMzdSCF51Fnnxoj%2BIc5yhbnI7a%2B9Gpoz9G%2FWuP9JRyM6G2kNAB5rYfPr7DDX5I1Ao2KbOCOfZY7mZDJg02s%2FmRoUjc4U4O7UkNHdZslI7PY2Kz%2FJhVyGQd; _yoda_verify_rid=152b178adfc0c01d; u=2588879467; n=HKO654533298; lt=FaP0oYXH4yejdElsrY0sn2PXZ8YAAAAAuxEAAGY-FLi_5PEv0Je0VzSOBR-6_AyR5yX3O_xYaGwtc-qVSfQ7wS5gYyGtGOLFleztQg; mt_c_token=FaP0oYXH4yejdElsrY0sn2PXZ8YAAAAAuxEAAGY-FLi_5PEv0Je0VzSOBR-6_AyR5yX3O_xYaGwtc-qVSfQ7wS5gYyGtGOLFleztQg; token=FaP0oYXH4yejdElsrY0sn2PXZ8YAAAAAuxEAAGY-FLi_5PEv0Je0VzSOBR-6_AyR5yX3O_xYaGwtc-qVSfQ7wS5gYyGtGOLFleztQg; token2=FaP0oYXH4yejdElsrY0sn2PXZ8YAAAAAuxEAAGY-FLi_5PEv0Je0VzSOBR-6_AyR5yX3O_xYaGwtc-qVSfQ7wS5gYyGtGOLFleztQg; unc=HKO654533298; _lxsdk_s=180c0eace38-599-2aa-c43%7C%7C7; firstTime=1652504090631'
    Collect(cookie)
