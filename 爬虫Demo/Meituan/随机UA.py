# -*- codeing = utf-8 -*-
# @Time : 2022/5/9 20:38
# @Author : 邢继森
# @File : 随机UA.py
# Software : PyCharm
from faker import Factory


# U_A
f = Factory.create()
UA = f.user_agent()

print('User-Agent:',UA)