# -*- codeing = utf-8 -*-
# @Time : 2022/5/2 21:39
# @Author : 邢继森
# @File : Lixia_District______.py
# Software : PyCharm
import datetime
import pprint
import zlib
import re
import parsel
import requests  # 代替浏览器进行网络请求
import json  # json字典 方便取值
from concurrent.futures import ThreadPoolExecutor  # 开启多线程模式
import random  # 随机数生成
import time  # 导入time模块做定时休眠
import base64  # 加密
from faker import Factory
from openpyxl import Workbook


def Headers(cookie):
    # 随机U_A
    f = Factory.create()
    UA = f.user_agent()
    # U_A伪装
    headers = {
        'User-Agent': UA,
        'Accept': 'application/json',
        'Host': 'jn.meituan.com',
        'Cookie': cookie,
    }
    return headers


# 解析网页
def RquestData(token, i,headers,wb1,sh2,sh3,sh4):
    # 代理IP
    proxie = IP()
    # 请求地址
    url = 'https://jn.meituan.com/meishi/api/poi/getPoiList?'
    # 请求参数
    params = {
        'cityName': '济南',
        'cateId': '0',
        'areaId': '139',
        'sort': '',
        'dinnerCountAttrId': '',
        'page': f'{i}',
        'userId': '2588879467',
        'uuid': '2fadaed2594f4c8e9f20.1652101894.1.0.0',
        'platform': '1',
        'partner': '126',
        'originUrl': f'https://jn.meituan.com/meishi/b139/pn{i}/',
        'riskLevel': '1',
        'optimusCode': '10',
        '_token': token
    }
    response = requests.get(url, headers=headers,params=params).text
    # 每请求一次随机休眠2-3s
    time.sleep(random.randint(3, 4))

    # json字符串转字典格式       json.dumps  字典格式转json格式
    # 去掉用不到的东西
    # result = response.replace("{'data': {'poiInfos': [",'').replace('''],
    #           'totalCounts': 1000},
    #  'status': 0}''','') # 把内容替换掉，替换成空
    # 转格式
    result = json.loads(response)
    # print(response)

    # 格式化输出
    # pprint.pprint(result)

    for i in result['data']['poiInfos']:
        # print(i)

        # 详情页url
        poiId = i['poiId']
        poiId_url = f'https://jn.meituan.com/meishi/{poiId}/'

        print('\t店铺名称:', i['title'], end='')
        print('\t评分:', i['avgScore'], end='')
        print('\t评论:', i['allCommentNum'], end='')
        print('\t地址:', i['address'], end='')
        print('\t人均价格:', i['avgPrice'], end='')
        # 套餐
        for x,j in enumerate(i['dealList']):
            print(f'\t套餐{x}:',j['title'],end='')
            print(f'\t套餐价格{x}:',j['price'])
            xhr = [i['title'],j['title'],j['price']]
            sh4.append(xhr)
            wb1.save('Lixia_District.xlsx')  # 保存文件
        # 写入数据
        dxt = [
            i['title'],
            i['avgScore'],
            i['allCommentNum'],
            i['address'],
            i['avgPrice']
        ]
        sh2.append(dxt)
        wb1.save('Lixia_District.xlsx')  # 保存文件
        title = i['title']
        # 返回详情页url获得详情页数据
        Detail_page(poiId_url,headers,wb1,sh3,title,proxie)
        # 返回url实现自动获取评论
        # Appraise(poiId,poiId_url,headers)
    # 每请求一次随机休眠2-3s
    time.sleep(random.randint(2, 3))


# 详情页店铺信息
def Detail_page(poiId_url,headers,wb1,sh3,title,proxie):
    # 代理IP
    proxie = {
        "http": "http://" + proxie,
    }
    print(proxie)
    # 请求参数
    # print(poiId_url)
    page_text = requests.get(url=poiId_url, headers=headers,proxies=proxie).text
    # 每请求一次随机休眠5-30s
    time.sleep(random.randint(3, 13))
    # print(page_text.text)
    # 使用正则表达式获取数据
    phone = re.findall('"phone":"(.*?)"', page_text)
    phone = ','.join(phone)
    print("\n电话", phone)

    address = re.findall('"address":"(.*?)"', page_text)
    address = ','.join(address)
    print("地址", address)

    openTime = re.findall('"openTime":"(.*?)"', page_text)
    openTime = ','.join(openTime)
    print("营业时间", openTime)

    text = re.findall('"text":"(.*?)"', page_text)
    text = ','.join(text)
    print("特点", text)

    # 写入数据
    dxt = [
        title,
        phone,
        address,
        openTime,
        text
    ]
    sh3.append(dxt)
    wb1.save('Lixia_District.xlsx')  # 保存文件



# 详情页评价部分
def Appraise(poiId,poiId_url,headers):
    url = 'https://jn.meituan.com/meishi/api/poi/getMerchantComment'
    # 请求参数
    params = {
        'uuid': 'd10defae57cc40a383a9.1651721567.1.0.0',
        'platform': 1,
        'partner': '126',
        'originUrl': poiId_url,
        'riskLevel': '1',
        'optimusCode': '10',
        'id': poiId,
        'userId': '2588879467',
        'offset': '0',
        'pageSize': '10',
        'sortType': '1'
    }
    response = requests.get(url=url, headers=headers, params=params).text#,proxies=proxy
    # 转成json格式
    result = json.loads(response)
    # 格式化输出
    # pprint.pprint(result)
    # 评价以及数量
    # for i in result['data']['tags']:
    #     print(i['tag'], end="")
    #     print(i['count'])

    # 总评价数量
    # print(result['data']['total'])
    # # 详细评价
    # for j in result['data']['comments']:
    #     # 昵称
    #     print(j['userName'], end=":")
    #     # 评价语
    #     print(j['comment'])
    #     # 头像
    #     print("头像:",j['userUrl'])
    #     # 配图
    #     for x in j['picUrls']:
    #         print("配图:",x['url'])
    # 每请求一次随机休眠2-3s
    time.sleep(random.randint(2, 3))

# token值获取
def Token():
    # time1 = js2py.eval_js('new Date().getTime();')
    data = 'eJxVjtluozAUht/Ft6DYBuxApF5AIIQkE1qWbKNeELYs4IQlJfVo3n1cqSPNSEf6l/Nd/L9A62VgghEyEJLBR96CCcAjNKJABn0nPpRgommYUoJUGaT/dQbBoju2GxtMfmKCZaor719FILIoVCrrVHuX/7GKJu6L8QQCTn1/7yYQXtiozs/9I2Gj9FZD4bvTGR6xakCxAwi+jgQv9Pqtybf2f/MPMVyw3blkwuWLYXOJe3/gZhwURVjuC2ntsd6b16Z/XUzZ0TGdsYRaPVAOtrHbn5zhEMbdNEssN1MuDeyIZK6kZVRi7vKnZaFqS+ycczqVoCrBV6bty1lUstVylTmXOL8eKlZMFf92t1Rr2VV8KB91dfTJsl+nc29LFwGymtcPpwoMl+wqVzdO0MoYvLchnUkNiQJdVXjmJ6H0VIaiVex5sff7LP1czbZaAZE/q1oePZsBt7ZmJuOoS9y31AufbBfPb7wKr0azXviH9SZLU8Y0c4jHoZa+gN9/AEzDkKE='
    token_decode = base64.b64decode(data)  # b64解码
    token_string = zlib.decompress(token_decode)  # 二进制进行压缩
    str1 = str(token_string, 'utf-8')
    # sign 请求参数
    sign = eval(str1)
    sign['ts'] = int(time.time() * 1000)
    sign['cts'] = int(time.time() * 1000) + 100  # 跟ts相差一百左右
    info = str(sign).encode()
    token = base64.b64encode(zlib.compress(info)).decode()
    time.sleep(random.randint(1,2))
    return token



# 提取IP
def IP():
    url = 'http://tiqu.pyhttp.taolop.com/getip?count=1&neek=30373&type=2&yys=0&port=1&sb=&mr=2&sep=0&ts=1&time=4'
    # # 随机U_A
    # f = Factory.create()
    # UA = f.user_agent()
    # headers1 = {
    #     'User-Agent': UA,
    #     'Cookie': '_uab_collina=165202315607252102309894; UM_distinctid=180a440de93852-01ecd2134dd069-9771a39-144000-180a440de94fdd; __root_domain_v=.py.cn; _qddaz=QD.309752023159555; CNZZDATA1279742900=1180151137-1652018526-%7C1652139113; _qdda=3-1.1; _qddab=3-ojfc2c.l2zgrp9t; userid=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHByX3RpbWUiOiIxNjUyMTQ1NzgzIiwidWlkIjoiMzAzNzMifQ.Y1OZn8vVF99uPhxaeJ3ouBWOAnTdvlvGLgIUjMRJZEM',
    # }
    browser = requests.get(url=url).text
    # 转成json格式
    result = json.loads(browser)
    # pprint.pprint(result)

    for i in result['data']:
        # print(i)
        ip = i['ip']  # ip
        port = i['port']  # 端口
        expire_time = i['expire_time']  # 到期时间
        ip_port = str(ip) + ":" + str(port)
        print("当前IP",ip_port)
        # print(ip_port)
        # expire_time = expire_time[10:16]
        #
        print("过期时间", expire_time)
        # expire_time1 = int(expire_time[0:3]) * 60 + int(expire_time[4:6])
        # print("过期分钟", expire_time1)
        now_time = datetime.datetime.now()
        print("当前时间",now_time)
        # now_time = datetime.datetime.now().strftime('%H:%M')
        # print("当前时间", now_time)
        # now_time = (datetime.datetime.now() + datetime.timedelta(minutes=30)).strftime("%H:%M")
        # print("当前时间+25分钟", now_time)
        #
        # # 当前分钟
        # now_time1 = int((datetime.datetime.now() + datetime.timedelta()).strftime("%H")) * 60 + int(
        #     (datetime.datetime.now() + datetime.timedelta()).strftime("%M"))
        # now_time = int((datetime.datetime.now() + datetime.timedelta()).strftime("%H")) * 60 + int(
        #     (datetime.datetime.now() + datetime.timedelta(minutes=25)).strftime("%M"))
        # print("当前分钟", now_time1,now_time)

        return ip_port


def Collect(cookie):
    # 创建表头
    wb1 = Workbook()
    # 创建工作簿
    sh2 = wb1.create_sheet('首页数据')  # 工作薄信息
    sh3 = wb1.create_sheet('详情页数据')  # 工作薄信息
    sh4 = wb1.create_sheet('首页店铺推荐食品')  # 工作薄信息
    one = ['店铺名称','评分','评论','地址','人均价格']
    two = ['店铺名称','电话','地址','营业时间','特点']
    three = ['店铺名称','套餐','价格']
    # 写入表头
    sh2.append(one)
    sh3.append(two)
    sh4.append(three)

    # 删除工作薄
    del wb1["Sheet"]
    wb1.save('Lixia_District.xlsx')  # 保存文件
    # 激活工作表
    # sh2 = wb1['首页数据']

    for i in range(1, 68):
        print(f"第{i}页")
        headers = Headers(cookie)
        # token
        token = Token()
        # 数据获取
        RquestData(token, i, headers,wb1,sh2,sh3,sh4)
        print('\n\n\n')
        # 每请求一次随机休眠10-60s
        time.sleep(random.randint(10, 60))

# if __name__ == "__main__":
# cookie = '_lxsdk_cuid=180a8f21c32c8-0701bea909dc82-9771a39-144000-180a8f21c33c8; cityname=%E6%BD%8D%E5%9D%8A; iuuid=0BD85904319FFA99D91F20769ADABEDD046E7CD7F5B1DC705B5BB694C11FAD10; _lxsdk=0BD85904319FFA99D91F20769ADABEDD046E7CD7F5B1DC705B5BB694C11FAD10; webp=1; i_extend=H__a100002__b1; latlng=36.708077,119.141246,1652101886361; __utma=74597006.1893403857.1652101887.1652101887.1652101887.1; __utmz=74597006.1652101887.1.1.utmcsr=baidu|utmccn=(organic)|utmcmd=organic; _lx_utm=utm_source%3Dgoogle%26utm_medium%3Dorganic; mtcdn=K; ci=96; rvct=96; client-id=39197cdf-f138-4d25-9b84-84e48862d027; _hc.v=e051f322-e17d-eea6-96d7-c26c641a0df4.1652158408; uuid=32b3bdf6e7df40f69480.1652255681.1.0.0; userTicket=OrHZgZjMsnVBsGpowMoCMqaXnaHRiRSegDAnODBN; _yoda_verify_resp=Kjtkjdy80UZi3W%2BNIUau2O0gYJqB%2BJrX8Q%2Bq7frGQkA9V6XLoYIZ5TphoVY8gXHq%2BdlatOv4ChWhnTwQOGgZ%2B26SDndxou7pVuJQVsB%2BJtQiS6n7qDdDNkZuL6Uw0x7XrYHPS7TLYCekW9uaAQkWP%2FHZ9zHMTbI6WPMRnZsZnl%2FSmy7rxxleaaJHh6ZP1VY%2BlnZ2wbeSlgFfpIopiRI5%2B21nonG%2FmUrgyfARilN%2F0FanTcy0M027%2B9GKXFcM8P4VVey2T2JcphGW0cFuKtfqpNWE821E%2FkO7RPq%2FuE61dYW0fhlyBrezrQOlKWtVXUMeMOkfxwss%2FhbIq2qIceDHAQvHI%2B3tfgvGS0eZMAQZMEvywBTl%2BlvcNDyfWngWu%2B3E; _yoda_verify_rid=15279668d9c1500f; u=2588879467; n=HKO654533298; lt=shXDTSTbA7XoRycHvaNUtEx1qkAAAAAAuxEAAJGDwLa0AMgqML233o2HDYKgJRtCloFhQtjeSOgw_68K0nmgBbw0vw_Sva1fX2Hy6A; mt_c_token=shXDTSTbA7XoRycHvaNUtEx1qkAAAAAAuxEAAJGDwLa0AMgqML233o2HDYKgJRtCloFhQtjeSOgw_68K0nmgBbw0vw_Sva1fX2Hy6A; token=shXDTSTbA7XoRycHvaNUtEx1qkAAAAAAuxEAAJGDwLa0AMgqML233o2HDYKgJRtCloFhQtjeSOgw_68K0nmgBbw0vw_Sva1fX2Hy6A; token2=shXDTSTbA7XoRycHvaNUtEx1qkAAAAAAuxEAAJGDwLa0AMgqML233o2HDYKgJRtCloFhQtjeSOgw_68K0nmgBbw0vw_Sva1fX2Hy6A; __mta=176010781.1652155814414.1652268793889.1652268905966.13; unc=HKO654533298; firstTime=1652268911267; _lxsdk_s=180b2c8d597-15a-1ba-ae3%7C%7C25'
#
# Collect(cookie)
