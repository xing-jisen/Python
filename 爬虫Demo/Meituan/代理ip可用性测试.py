# -*- codeing = utf-8 -*-
# @Time : 2022/5/8 16:22
# @Author : 邢继森
# @File : 代理ip可用性测试.py
# Software : PyCharm

import requests
import random
import time


http_ip = [
        # '36.62.217.2:64257',
        '114.99.11.194:64257'
]
# http_ip = [
#     '118.163.13.200:8080',
#     '222.223.182.66:8000',
#     '51.158.186.242:8811',
#     '171.37.79.129:9797',
#     '139.255.123.194:4550'
# ]

for i in range(len(http_ip)*2):
    try:
        ip_proxy = random.choice(http_ip)
        proxy_ip = {
            'http': 'http://121.225.65.19:64257',
            # 'https':  'https://' + ip_proxy,
        }
        print('使用代理的IP:', proxy_ip)
        response = requests.get("http://httpbin.org/ip", proxies=proxy_ip).text
        print(response,end='')
        print('当前IP有效\n')
        time.sleep(2)
    except Exception as e:
        print(e.args[0])
        print('当前IP无效')
        continue