# -*- codeing = utf-8 -*-
# @Time : 2022/5/9 20:52
# @Author : 邢继森
# @File : 自动获取代理ip.py
# Software : PyCharm
import parsel
import requests
import re
# 爬取ip
from faker import Factory

f = Factory.create()
UA = f.user_agent()
headers = {
        'User-Agent': UA,
    }
def a(i):
    url = f'http://www.ip3366.net/?stype=1&page={i}'
    page_text = requests.get(url=url, headers=headers).text
    rell = parsel.Selector(page_text)
    # ip = rell.xpath('//div[@class="center"]/table/tr')
    ip = rell.xpath('//*[@id="list"]/table/tbody/tr')
    # print(ip)
    ip_list = []
    for tr in ip:
        x1 = tr.xpath('./td[1]/text()').get()
        x2 = tr.xpath('./td[2]/text()').get()
        x3 = tr.xpath('./td[4]/text()').get()
        # print(x1,x2,x3)
        ip_list.append(x1 + ":" + x2)
        # print(ip_list)
        # 验证是否可用
    list = []
    for ip in ip_list:
        try:
            response = requests.get('https://www.baidu.com', proxies={'http':'http://' + ip + '\''}, timeout=2)
            if response.status_code == 200:
                list.append(ip)
        except:
            pass
        else:
            print(ip, '检测通过')

for i in range(0,11):
    # 运行
    a(i)

response = requests.get('https://www.baidu.com', proxies={'http': 'http://103.178.43.14:8181'}, timeout=2)
if response.status_code == 200:
    print(1)
else:
    print(0)



