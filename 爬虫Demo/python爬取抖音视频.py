# -*- codeing = utf-8 -*-
# @Time : 2022/4/28 22:01
# @Author : 邢继森
# @File : python爬取抖音视频.py
# Software : PyCharm
import requests
import parsel
import os

url = 'https://www.douyin.com/?enter=guide'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
}
page_c = requests.get(url=url,headers=headers).text
# 解析字符串
tree = parsel.Selector(page_c)
# print(tree)
# 数据解析 视频
video_content = tree.xpath('//[@class="ckqOrial"]/div/div[2]/div/div/div[4]/div[1]/div[2]/ul/li[1]/a/div/p/text()').get()
# video_content = tree.xpath('//*[@id="root"]/div/div[2]/div/div/div[4]/div[1]/div[2]/ul/li')
print(video_content)
# for li in video_content:
#     print(li,"123")


# 数据解析 名称
# video_name = tree.xpath('//*[@id="video-info-wrap"]/div[1]/div[2]/div/div/span/span/a/span/text()').get()
# print(video_name)

# # 创建文件夹
# if not os.path.exists(r'D:\PyCharm\pycharm one\爬虫Demo\抖音视频'):
#     os.mkdir(r'D:\PyCharm\pycharm one\爬虫Demo\抖音视频')
#
# x = requests.get(url=video_content,headers=headers).content
#
# with open('./抖音视频/a.mp4','wb') as fp:
#     fp.write(x)