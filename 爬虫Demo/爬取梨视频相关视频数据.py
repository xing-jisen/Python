# -*- codeing = utf-8 -*-
# @Time : 2022/4/19 21:21
# @Author : 邢继森
# @File : 爬取梨视频相关视频数据.py
# Software : PyCharm
# 原则:只是处理耗时间和阻塞的数据
import json
import random

import requests
import parsel
import time
from selenium import webdriver
# UA伪装
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36',
    'Cookie':'__secdyid=779bbb2ac47fe6e62f5cb581d9657f147026d92da11ce464021650374450; PEAR_UUID=26763632-14f8-4a18-a0b2-e96f2bc4e345; _uab_collina=165037445169434323095364; p_h5_u=958F3C57-FCC1-4C2A-B6EB-0397B2AC57F8; acw_tc=2f624a1b16518974280095509e0e7fb657f8ac015b220d47cd04e5e212dd29; JSESSIONID=34DCB762FF502DA94EF33B8926970A82; PEAR_UID="6faHXw5OgH1baIOYgh3SXQ=="; PEAR_TOKEN=e277513c-70d0-4325-b505-aa8695d6f810; SERVERID=a6169b2e0636a71b774d6641c064eb8c|1651898053|1651897428'
}
url= 'https://www.pearvideo.com/category_5'
page_text = requests.get(url=url,headers=headers).text
# 解析字符串
tree = parsel.Selector(page_text)
# 数据解析
li_list = tree.xpath('//*[@id="listvideoListUl"]/li')
for li in li_list:
    li_video = 'https://www.pearvideo.com/' + li.xpath('./div/a/@href').get()
    li_name = li.xpath('./div/a/div[2]/text()').get() #  + '.mp4'
    print(li_name,li_video)

    url_v = 'https://www.pearvideo.com/videoStatus.jsp'
# 对详情页的数据发起请求
    datas={
        'contId': '1732555',
        'mrd': random.random()
    }
    print(datas)
    li_video_response = requests.get(url=url_v,headers=headers,data=datas).text

    print(li_video_response)
    # 转格式
    # result = json.loads(li_video_response)
    # print(result)

