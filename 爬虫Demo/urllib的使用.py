# -*- codeing = utf-8 -*-
# @Time : 2022/4/24 22:28
# @Author : 邢继森
# @File : urllib的使用.py
# Software : PyCharm

from urllib import request
# UA伪装
headers = {
    'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1Safari/534.50'
}
url = 'http://www.dianping.com'

req = request.Request(url,headers=headers)
res = request.urlopen(req) # 获取相应

print(res.info()) # 响应头
print(res.getcode()) # 状态码
print(res.geturl()) # 返回相应地址

html = res.read()
html = html.decode("utf-8") # 解码
print(html)
