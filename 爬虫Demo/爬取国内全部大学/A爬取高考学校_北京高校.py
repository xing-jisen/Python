# -*- codeing = utf-8 -*-
# @Time : 2022/4/20 18:48
# @Author : 邢继森
# @File : B爬取高考学校_天津高校.py
# Software : PyCharm
import requests
import parsel
# 生成随机数的函数randint
from random import randint
# 设置睡眠时间，即多久运行一次
from time import sleep
import pymysql

# 数据库连接
def Link():
    host = '127.0.0.1'  # 主机名
    user = 'root'  # 数据库用户名
    password = '123456'  # 数据库密码
    database = 'notebook'  # 数据库名称
    db = pymysql.connect(host=host, user=user, password=password, db=database)  # 建立连接
    # 打开数据库连接
    # 这4个参数依次是：主机名，用户名，密码和数据库名
    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()


    # 使用 execute()  方法执行 SQL 查询
    cursor.execute("SELECT VERSION()")

    # 使用 fetchone() 方法获取单条数据.
    data = cursor.fetchone()

    print("Database version : %s " % data)
    return cursor,db

# 创建数据表
def Set_up(cursor):
    # 使用 execute() 方法执行 SQL，如果表存在则删除
    cursor.execute("DROP TABLE IF EXISTS college")

    # 使用预处理语句创建表
    sql = """CREATE TABLE `college` (
      `id` int(8) NOT NULL AUTO_INCREMENT,
      `Cname` varchar(255) DEFAULT NULL,    # 大学名称
      `Csection` varchar(255) DEFAULT NULL, # 所属部门
      `Ceducation_background` varchar(255) DEFAULT NULL, # 学历层次
      `school_E_B` varchar(255) DEFAULT NULL, # 学历层次
      `school_T_U` varchar(255) DEFAULT NULL, # 学历层次
      `school_T_D` varchar(255) DEFAULT NULL, # 学历层次
      `school_G_S` varchar(255) DEFAULT NULL, # 学历层次
      `school_satisfaction` double(200,2),    # 学历层次
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"""

    cursor.execute(sql)
# 写入数据
def Insert(cursor,db,a,b,c,d,e,f,g,h):
    Cname = a
    Csection = b
    Ceducation_background = c
    school_E_B = d

    #
    print(id(e))
    # 是否一流院校之类
    if id(e) == 140720668530792:
        school_T_U = "YES"
        print(school_T_U)
    else:
        school_T_U = "NO"
        print(school_T_U)

    if id(f) == 140720668530792:
        school_T_D = 'YES'
        print(school_T_D)
    else:
        school_T_D = 'NO'
        print(school_T_D)

    if id(g) == 140720668530792:
        school_G_S = 'YES'
        print(school_G_S)
    else:
        school_G_S = 'NO'
        print(school_G_S)

    # 评分
    school_satisfaction = h


    sql = "INSERT INTO college(Cname,Csection,Ceducation_background,school_E_B,school_T_U,school_T_D,school_G_S,school_satisfaction) VALUES ('%s', '%s', '%s','%s', '%s', '%s','%s','%s')" % (Cname, Csection, Ceducation_background,school_E_B,school_T_U,school_T_D,school_G_S,school_satisfaction)
    sql_ = "INSERT INTO college(Cname,Csection,Ceducation_background,school_E_B,school_T_U,school_T_D,school_G_S) VALUES ('%s', '%s', '%s','%s', '%s', '%s','%s')" % (Cname, Csection, Ceducation_background,school_E_B,school_T_U,school_T_D,school_G_S)

    try:
        # 执行sql语句
        cursor.execute(sql)
        # 执行sql语句
        db.commit()
    except:
        # # 发生错误时回滚
        # db.rollback()

        # 执行sql语句
        cursor.execute(sql_)
        # 执行sql语句
        db.commit()





headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
}

def Crawl_a_page(url):
    # print('院校名称	院校所在地	教育行政主管部门	学历层次	 一流大学建设高校	一流学科建设高校	研究生院	满意度')
    page_text= requests.get(url=url,headers=headers).text
    tree = parsel.Selector(page_text)
    tr_tab = tree.xpath("//div[@class='yxk-table']/table/tr")
    # print(tr_tab)

    # /tbody 有也不能加**************************************************************

    for count,tr in enumerate(tr_tab):
        if (count + 1) != 1:
            school_name = tr.xpath('./td[1]/a/text()').get()            # 院校名称
            school_area = tr.xpath('./td[2]/text()').get()              # 院校地址
            school_branch = tr.xpath('./td[3]/text()').get()            # 教育行政主管部门
            school_E_B = tr.xpath('./td[4]/text()').get()               # 学历

            # 是否一流大学建设高校
            if (tr.xpath('./td[5]/i')):
                school_T_U = True
            else:  # 一流大学建设高校
                school_T_U = False

            # 是否一流学科建设高校
            if (tr.xpath('./td[6]/i')):
                school_T_D = True
            else:  # 一流大学建设高校
                school_T_D = False

            # 是否研究生院
            if (tr.xpath('./td[7]/i')):
                school_G_S = True
            else:  # 一流大学建设高校
                school_G_S = False

            school_satisfaction = tr.xpath('./td[8]/a/text()').get()  # 满意度

            print(school_name.strip(),school_area.strip(),school_branch.strip(),school_E_B.strip(),school_T_U,school_T_D,school_G_S,school_satisfaction)
            # sleep(1)
            # print(school_name.strip().ljust(30),school_area.strip().ljust(10),school_branch.strip().center(20),school_E_B.strip().center(10),school_T_U,school_T_D,school_G_S,school_satisfaction.rjust(30))
            # .strip()去除空格 ljust(),center(),rjust()函数实现输出的字符串左对齐、居中、右对齐
            # 插入数据
            Insert(cursor,db,school_name.strip(),school_area.strip(),school_branch.strip(),school_E_B.strip(),school_T_U,school_T_D,school_G_S,school_satisfaction)

if __name__ == "__main__":

    # 连接
    cursor,db = Link()
    # 创建
    Set_up(cursor)


    x = 0
    for i in range(1,6):
        y = i
        print("正在爬取第" , i , "页")
        url = f'https://gaokao.chsi.com.cn/sch/search--searchType-1,ssdm-11,start-{x}.dhtml'
        x += 20
        Crawl_a_page(url)
        # 放到函数里即可，程序执行到这里会进行停顿
        sleep(randint(2, 5))


    print("爬取结束,一共爬取了",y,"页")

