# -*- codeing = utf-8 -*-
# @Time : 2022/4/20 18:48
# @Author : 邢继森
# @File : B爬取高考学校_天津高校.py
# Software : PyCharm
import requests
import parsel
# 生成随机数的函数randint
from random import randint
# 设置睡眠时间，即多久运行一次
from time import sleep


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
}

def Crawl_a_page(url):
    # print('院校名称	院校所在地	教育行政主管部门	学历层次	 一流大学建设高校	一流学科建设高校	研究生院	满意度')
    page_text= requests.get(url=url,headers=headers).text
    tree = parsel.Selector(page_text)
    tr_tab = tree.xpath("//div[@class='yxk-table']/table/tr")
    # print(tr_tab)

    # /tbody 有也不能加**************************************************************

    for count,tr in enumerate(tr_tab):
        if (count + 1) != 1:
            school_name = tr.xpath('./td[1]/a/text()').get()            # 院校名称
            school_area = tr.xpath('./td[2]/text()').get()              # 院校地址
            school_branch = tr.xpath('./td[3]/text()').get()            # 教育行政主管部门
            school_E_B = tr.xpath('./td[4]/text()').get()               # 学历

            # 是否一流大学建设高校
            if (tr.xpath('./td[5]/i')):
                school_T_U = True
            else:  # 一流大学建设高校
                school_T_U = False

            # 是否一流学科建设高校
            if (tr.xpath('./td[6]/i')):
                school_T_D = True
            else:  # 一流大学建设高校
                school_T_D = False

            # 是否研究生院
            if (tr.xpath('./td[7]/i')):
                school_G_S = True
            else:  # 一流大学建设高校
                school_G_S = False

            school_satisfaction = tr.xpath('./td[8]/a/text()').get()  # 满意度

            print(school_name.strip(),school_area.strip(),school_branch.strip(),school_E_B.strip(),school_T_U,school_T_D,school_G_S,school_satisfaction)
            sleep(1)
            # print(school_name.strip().ljust(30),school_area.strip().ljust(10),school_branch.strip().center(20),school_E_B.strip().center(10),school_T_U,school_T_D,school_G_S,school_satisfaction.rjust(30))
            # .strip()去除空格 ljust(),center(),rjust()函数实现输出的字符串左对齐、居中、右对齐

if __name__ == "__main__":
    x = 0
    for i in range(1,5):
        y = i
        print("正在爬取第" , i , "页")
        url = f'https://gaokao.chsi.com.cn/sch/search--searchType-1,ssdm-31,start-{x}.dhtml'
        x += 20
        Crawl_a_page(url)
        # 放到函数里即可，程序执行到这里会进行停顿
        sleep(randint(2, 5))
    print("爬取结束,一共爬取了",y,"页")

