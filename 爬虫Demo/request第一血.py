# -*- codeing = utf-8 -*-
# @Time : 2022/4/16 15:59
# @Author : 邢继森
# @File : request第一血.py
# Software : PyCharm
import requests
url = 'https://www.sogou.com'
# get方法会返回一个响应对象
response = requests.get(url=url)
# page_text返回的是txt类型数据
page_text = response.text
print(page_text)
# 持久化存储
with open('./wangye/搜狗首页.html','w',encoding='utf-8') as fp:
    fp.write(page_text)
    print('爬取数据结束')