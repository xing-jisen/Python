# -*- codeing = utf-8 -*-
# @Time : 2022/4/25 16:07
# @Author : 邢继森
# @File : cookie获取登陆后的数据.py
# Software : PyCharm
import requests

url = 'https://course.dongao.com/datum/1014'
headers = {
    'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1Safari/534.50',
    'Cookie': 'sajssdk_2015_cross_new_user=1; da_vu_id=%7B%22distinct_id%22%3A%22473acd2c-67f9-da80-95af-4def5ddc3111%22%2C%22%24device_id%22%3A%22473acd2c-67f9-da80-95af-4def5ddc3111%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E8%87%AA%E7%84%B6%E6%90%9C%E7%B4%A2%E6%B5%81%E9%87%8F%22%2C%22%24latest_referrer%22%3A%22https%3A%2F%2Fwww.baidu.com%2Flink%22%2C%22%24latest_referrer_host%22%3A%22www.baidu.com%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC%22%7D%7D; NTKF_T2D_CLIENTID=guestAD4ECA66-F392-7873-886E-5FB922BD8F9E; nTalk_CACHE_DATA={uid:kf_9517_ISME9754_guestAD4ECA66-F392-78,tid:1650873410236952}; memberAccessVersion=1; cookieIpSign=b120947e1acb4d7589be1d6af112a48a; 653885df-0b23-daec-bd0b-216667e5e4dc=43270050; 2b2b7eb2-4e70-dacc-b992-f1e919794a8c=43270050; send_session_id=c45bb13e-6b29-dacc-ad33-7cf115f84620; dongaoLogin=eyJtZW1iZXJJZCI6IjQzMjcwMDUwIiwicmFuZG9tU3RyaW5nIjoiZmE5OTBkMDY5YjMwNDAwNjkxMzE5NjQwMzlkNjFkOWYiLCJicm93ZXJNYXJrZXIiOiJNb3ppbGxhLzUuMCAoV2luZG93cyBOVCAxMC4wOyBXT1c2NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzg2LjAuNDI0MC4xOTggU2FmYXJpLzUzNy4zNiIsImF1dG9Mb2dpbiI6ZmFsc2UsImNhY2hlS2V5IjoiYml6OnBhc3Nwb3J0OmxvZ2luQ29va2llOm1lbWJlcklkXzQzMjcwMDUwOmZhOTkwZDA2OWIzMDQwMDY5MTMxOTY0MDM5ZDYxZDlmIn0%3D; memberinfo="{\"uid\":43270050,\"uname\":\"152****5903\"}"; c45bb13e-6b29-dacc-ad33-7cf115f84620=43270050; riskTokenFlag=1; redDotStatus="{\"messageCenterRedDot\":0,\"courseUpdateRedDot\":0}"; memberNewLeftSite=datum; JSESSIONID=E9B63A89D354E9AC545E68FA5B37657E'
}
res = requests.get(url=url,headers=headers)

# 获取响应状态码
code = res.status_code
print(code)

if code == 200:
    with open('./wangye/dengluhou.html','w',encoding='utf-8') as fp:
        fp.write(res.text)

    print("爬取结束")