# -*- codeing = utf-8 -*-
# @Time : 2022/4/16 16:15
# @Author : 邢继森
# @File : 建议网页采集器.py
# Software : PyCharm
# 导包
import requests
# UA伪装
headers = {
'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36'
}
# 指定url

url = 'https://www.baidu.com/s'
wd = input('请输入搜索的数据:')
# 封装到字典中
param = {
    'wd':wd
}
response = requests.get(url=url,params=param,headers=headers)
# 获取相应数据
page_text = response.text
filrName = wd + '.html'
with open('./wangye/' + filrName,'w',encoding='utf-8') as fp:
    fp.write(page_text)
print(filrName,'爬取成功')