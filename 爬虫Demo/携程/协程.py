# -*- codeing = utf-8 -*-
# @Time : 2022/4/20 17:28
# @Author : 邢继森
# @File : 协程.py
# Software : PyCharm
import time
import asyncio
import requests

async def image(i,url):
    print("下载开始",url)
    time.sleep(2)
    # 发送请求下载图片
    response = requests.get(url)
    print("下载完成")
    # 图片保存
    file_name = str(url)
    # print(file_name)
    with open('./tupian/' + str(i+1) + '.jpg','wb') as fp:
        fp.write(response.content)

if __name__ == '__main__':
    url_list = [
        'https://previews.123rf.com.cn/images/kenangle/kenangle2010/kenangle201000026/156473797.jpg',
        'https://images-new.123rf.com.cn/450wm/zoomteam/zoomteam1408/zoomteam140800457/31061581-autumn-woman-portrait-with-creative-makeup.jpg',
        'https://previews.123rf.com.cn/images/kitthanesratanasiraanan/kitthanesratanasiraanan2003/kitthanesratanasiraanan200300209/142938467.jpg',
        'https://previews.123rf.com.cn/images/domenicogelermo/domenicogelermo2004/domenicogelermo200400618/144293214.jpg',
    ]
    for i,item in enumerate(url_list):
        image(i,item)