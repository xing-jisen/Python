# -*- codeing = utf-8 -*-
# @Time : 2022/5/7 16:15
# @Author : 邢继森
# @File : await(等待)关键字.py
# Software : PyCharm
import asyncio

# await + 可等待对象（携程对象，Future、Task对象） 类似IO等待
# 等待对象的值得到结果之后才可以往下执行

async def func():
    print("来呀，来玩呀")
    response = await asyncio.sleep(2)
    print("结束",response)

asyncio.run(func())