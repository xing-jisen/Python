# -*- codeing = utf-8 -*-
# @Time : 2022/5/7 17:51
# @Author : 邢继森
# @File : future对象.py
# Software : PyCharm
import asyncio
# 不常用 Task一部分功能基于它
# task集成Future，Task对象内部uawait结果的处理基于Future对象

async def set_after(fut):
    await asyncio.sleep(2)
    fut.set_result("666")

async def main():
    # 获取的当前事件
    loop = asyncio.get_event_loop()

    # 创建一个任务（Future）对象，这个任务什么也不干
    fut = loop.create_future()

    # 等待人物最终结果（Future对象），没有结果会一直等待
    data = await fut
    print(data)
asyncio.run(main())



