# -*- codeing = utf-8 -*-
# @Time : 2022/5/7 16:34
# @Author : 邢继森
# @File : task对象（添加多个任务）.py
# Software : PyCharm
import asyncio

# task 对象，往事件循环添加多个任务
# 如果遇到IO等待，自动执行下一个任务
# asyncio.create_task(携程对象) 创建方法

async def func():
    print(1)
    await asyncio.sleep(2)
    print(2)
    return "返回值"

async def main():
    print("main开始")

    # 用的比较少
    # # 创建Task对象，将当前执行func函数任务添加到事件循环
    # task1 = asyncio.create_task(func())
    # # 创建Task对象，将当前执行func函数任务添加到事件循环
    # task2 = asyncio.create_task(func())

    task_list = [
        asyncio.create_task(func(),name='N1'),
        asyncio.create_task(func(),name='N2')
    ]
    print("main结束")
    #
    # # 当执行某协程遇到IO操作时，会自动切换到其他任务
    # #此处的await是等待相应的携程全部执行完毕并获取结果
    # ret1 = await task1
    # ret2 = await task2

    # wait等待这个任务
    # timeout=数字或None，等待秒数
    done,pending = await asyncio.wait(task_list,timeout=None)
    print(done)
    # print(ret1,ret2)


asyncio.run(main())