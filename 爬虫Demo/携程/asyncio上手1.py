# -*- codeing = utf-8 -*-
# @Time : 2022/5/7 15:54
# @Author : 邢继森
# @File : asyncio上手1.py
# Software : PyCharm
import asyncio

# 携程函数
async def func():
    print("执行携程函数创建携程对象，携程内部代码不会执行")
    print("如果想执行代码，必须将携程对象交给事件循环执行")

# 携程对象
resule = func()

# # 复杂写法
# # 生成一个事件循环
# loop = asyncio.get_event_loop()
# # 将任务放到任务列表
# loop.run_until_complete(resule)

# 简单写法 py3.7之后可用
asyncio.run(resule) # 相当于上面两句