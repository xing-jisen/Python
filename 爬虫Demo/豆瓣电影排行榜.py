# -*- codeing = utf-8 -*-
# @Time : 2022/4/16 20:35
# @Author : 邢继森
# @File : 豆瓣电影排行榜.py
# Software : PyCharm
import json
import requests
# 获取url
url = 'https://movie.douban.com/j/chart/top_list'
param = {
    'type': '24',
    'interval_id': '100:90',
    'action': '',
    'start': '0',# 从库中的第几部电影开始取
    'limit': '20'# 一次取多少个数
}
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'

}
response = requests.get(url=url,params=param,headers=headers)
list_data = response.json()

# 持久化存储
fp = open('./wangye/豆瓣.json','w',encoding='utf-8')
json.dump(list_data,fp = fp,ensure_ascii=False)
print('over!!!')
