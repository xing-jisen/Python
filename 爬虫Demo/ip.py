# -*- codeing = utf-8 -*-
# @Time : 2022/5/8 16:22
# @Author : 邢继森
# @File : 代理ip可用性测试.py
# Software : PyCharm

import requests
import random
import time

http_ip = [
        # '36.62.217.2:64257',
        '114.99.11.194:64257'
]

for i in range(len(http_ip)*2):
    try:
        # ip_proxy = random.choice(http_ip)
        proxy_ip = {
            'http': 'http://124.94.252.89:64257',
            # 'https':  'https://' + ip_proxy,
        }
        print('使用代理的IP:', proxy_ip)
        response = requests.get("https://www.baidu.com/s?wd=ip", proxies=proxy_ip).text
        with open('./代理ip地址.html','w',encoding='utf-8') as fp:
            fp.write(response)
        print(response)
        print('当前IP有效\n')
        time.sleep(2)
    except Exception as e:
        print(e.args[0])
        print('当前IP无效')
        continue