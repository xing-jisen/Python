# -*- codeing = utf-8 -*-
# @Time : 2022/4/20 17:55
# @Author : 邢继森
# @File : 多任务协程.py
# Software : PyCharm
import asyncio
import time

def request(url):
    print("正在下载",url)
    # 在异步协程中如果出现了同步模块相关的代码，就无法实现异步
    # time.sleep(2)
    # 在asyncio中遇到阻塞必须手动挂起
    await asyncio.sleep(2)
    print("下载完毕",url)

start = time.time()
urls = [
    'www.baidu.com',
    'www.sogou.com',
    'www.google.com'
]

# 任务列表，需要存储多个对象
stasks = []
for url in urls:
    c = request(url)
    task = asyncio.ensure_future(c)
    stasks.append(task)

loop = asyncio.get_event_loop()
# 需要将任务列表封装到wait'中
loop.run_until_complete(asyncio.wait(stasks))

print(time.time()-start)