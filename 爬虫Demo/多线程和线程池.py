# -*- codeing = utf-8 -*-
# @Time : 2022/4/19 21:07
# @Author : 邢继森
# @File : 多线程和线程池.py
# Software : PyCharm

# import time
# # 多线程方式执行
# def get_page(str):
#     print("正在下载",str)
#     time.sleep(2)
#     print("下载成功",str)
# name_list = ['asfd','fafasf','gawe','gag']
#
# start_time = time.time()
#
# for i in range(len(name_list)):
#     get_page(name_list[i])
#
# 获取运行时间
# end_time = time.time()
# print("%d second"% (end_time-start_time))

import time
# 导入线程池对应的类        用于阻塞
from multiprocessing.dummy import Pool
# 线程池方式执行
start_time = time.time()
def get_page(str):
    print("正在下载",str)
    time.sleep(2)
    print("下载成功",str)
name_list = ['asfd','fafasf','gawe','gag']

# 实例化一个线程池对象
pool = Pool(4)      # 4个请求阻塞
# 将列表中每一个列表元素传递给get page进行处理
pool.map(get_page,name_list)

# 获取运行时间
end_time = time.time()
print(end_time-start_time)