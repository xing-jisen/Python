# -*- codeing = utf-8 -*-
# @Time : 2022/4/22 21:21
# @Author : 邢继森
# @File : requests库的post请求.py
# Software : PyCharm
import requests

# 定义请求的URL
url = 'https://fanyi.baidu.com/v2transapi?'

# UA伪装
headers = {   
    'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1Safari/534.50'
}
data = {
    'from': 'zh',
    'to': 'en',
    'query': '世界',
    'transtype': 'realtime',
    'simple_means_flag': '3',
    'sign': '232427.485594',
    'token': '130d896cf481fc541e23ea11b543dc8d',
    'domain': 'common'
}
response = requests.post(url=url,headers=headers,data=data)

# 接收返回数据
print(response.status_code) # 响应状态码
print(response.json())