# -*- codeing = utf-8 -*-
# @Time : 2022/4/30 14:38
# @Author : 邢继森
# @File : python爬取微博.py
# Software : PyCharm
import requests
import parsel
import json

# 热门微博_热门
def Hot_microblog(i):

    # 访问地址
    url2 = 'https://weibo.com/ajax/feed/hottimeline'
    # 请求参数
    params = {
        'since_id': '0',
        'refresh': '0',
        'group_id': '102803',
        'containerid': '102803',
        'extparam': 'discover|new_feed',
        'max_id': i,
        'count': '10',
    }

    # U_A伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }

    # 请求地址
    response = requests.get(url2,headers=headers,params=params)

    # 返回的结果（元组类型）
    result = response.text

    # json字符串转字典格式       json.dumps  字典格式转json格式

    # 去掉用不到的东西
    # result = result.replace('{"ok":','') # 把内容替换掉，替换成空

    # 转格式
    result = json.loads(result)
    # print(result)
    # json格式读取
    for x,i in enumerate(result['statuses']):
        print(x+1)
        # 获取微博博主名称
        print(i['user']['screen_name'])

        # 内容
        print(i['text_raw'].strip())







if __name__ == "__main__":
    for i in range(0,100):
        Hot_microblog(i)


