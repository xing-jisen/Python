# -*- codeing = utf-8 -*-
# @Time : 2022/4/19 20:45
# @Author : 邢继森
# @File : 同步爬虫.py
# Software : PyCharm
import requests
# UA伪装
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
}
urls = [
    'https://ppt.1ppt.com/uploads/soft/2204/1-220415153154.zip',
    'https://ppt.1ppt.com/uploads/soft/2204/1-220414154I0.zip'
]
# 获取相应数据
def get_content(url):
    print("正在爬取:",url)
    # get方法是一个阻塞的方法
    response = requests.get(url=url,headers=headers)
    if response.status_code == 200:
        return response.content

# 数据解析
def parsel_content(content):
    print("相应数据的长度为:",len(content))

for url in urls:
    content = get_content(url)
    parsel_content(content)