# -*- codeing = utf-8 -*-
# @Time : 2021/11/27 21:40
# @Author : 邢继森
# @File : 字符串的常用操作（判断）.py
# Software : PyCharm
if __name__ == "__main__":
    a = 'hello,python'
    print('1',a.isidentifier())
    print('2','hello'.isidentifier())
    print('3','张三_'.isidentifier())
    print('4','张三_123'.isidentifier())

    print('5','\t'.isspace())

    print('6','abc'.isalpha())
    print('7','张三'.isalpha())
    print('8','张三1'.isalpha())

    print('9','123'.isdecimal())
    print('10','123四'.isdecimal())
    print('11','ⅠⅡⅢⅣⅤ'.isdecimal())

    print('12','123'.isnumeric())
    print('13', '123四'.isnumeric())
    print('14', 'ⅠⅡⅢⅣⅤ'.isnumeric())

    print('15', 'abc1'.isalnum())
    print('16', '张三123'.isalnum())
    print('17', 'abc!'.isalnum())

