# -*- codeing = utf-8 -*-
# @Time : 2021/11/27 20:43
# @Author : 邢继森
# @File : 字符串的常用操作.py
# Software : PyCharm
if __name__ == "__main__":
    s = 'hello,hello'
    print(s.index('lo'))
    print(s.find('lo'))
    print(s.rindex('lo'))
    print(s.rfind('lo'))

     # print(s.index('k'))   抛异常  IndentationError: unexpected indent
    print(s.find('k'))    # -1
    # print(s.rindex('k'))   抛异常  IndentationError: unexpected indent
    print(s.rfind('k'))

    # 建议使用 find 或者 rfind