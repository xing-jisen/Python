# -*- codeing = utf-8 -*-
# @Time : 2021/11/27 21:27
# @Author : 邢继森
# @File : 字符串的常用操作（劈分）.py
# Software : PyCharm
if __name__ == "__main__":
    s = 'hello world python'
    lst = s.split()      # 从左侧开始劈分
    print(lst)

    s1 = 'hello|world|python'
    print(s1.split(sep = '|'))
    print(s1.split(sep='|',maxsplit=1))   # 左侧开始最大分一段
    print('-'*50)

    s = 'hello world python'
    print(s.rsplit())       # 从右侧开始劈分
    print(s1.rsplit('|'))
    print(s1.rsplit(sep='|', maxsplit=1))  # 右侧开始最大分一段