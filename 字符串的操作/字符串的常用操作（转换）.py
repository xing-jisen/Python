# -*- codeing = utf-8 -*-
# @Time : 2021/11/27 20:54
# @Author : 邢继森
# @File : 字符串的常用操作（转换）.py
# Software : PyCharm
if __name__ == "__main__":
    s = 'hello,python'

    a = s.upper()    # 转成大写之后会生成新的字符串对象
    print(a,id(a))
    print(s,id(s))

    b = s.lower()   # 本身小写转换之后依然会生成新的字符串对象
    print(b, id(b))
    print(s, id(s))
    print(b == s)
    print(b is s)

    s1 = 'Hello,Python'
    c = s1.swapcase()
    print(c, id(c))
    print(s1, id(s1))

    s2 = 'HeLlo,PyThon'
    d = s2.title()
    print(d, id(d))
    print(s2, id(s2))

    s3 = 'HeLlo,PyThon'
    e = s3.capitalize()
    print(e, id(e))
    print(s3, id(s3))