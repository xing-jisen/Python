# -*- codeing = utf-8 -*-
# @Time : 2021/11/28 14:36
# @Author : 邢继森
# @File : 字符串的比较操作.py
# Software : PyCharm
if __name__ == "__main__":
    print('apple'>'app')
    print('apple'>'banana')

    print(ord('a'),ord('b'))  # ord 获取字符的原始值

    print(chr(97),chr(98))    # chr 原始值转换成字符

    print(ord('邢'))
    print(chr(37026))


    ''' == 与 is 区别
    is  比较的事id是否相等
    '''
    a = b = 'python'
    c = 'python'
    print(a is b)
    print(a is c)
    print(id(a))
    print(id(b))
    print(id(c))
