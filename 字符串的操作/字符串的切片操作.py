# -*- codeing = utf-8 -*-
# @Time : 2021/11/28 14:44
# @Author : 邢继森
# @File : 字符串的切片操作.py
# Software : PyCharm
if __name__ == "__main__":
    a = 'hello,python'
    b = 'java'
    print(1,a[0])

    print(2,a[:5])
    print(3,a[6:])
    print(4,a[0:3])
    print(5,a[0:6:2])
    print(6,a[::2])
    print(7,a[::-1])   # 倒置
    print(8,a + '!' + b)