# -*- codeing = utf-8 -*-
# @Time : 2021/11/28 16:16
# @Author : 邢继森
# @File : 字符串的编码转换.py
# Software : PyCharm
if __name__ == "__main__":
    s = '天涯共此时'
    # 编码
    # b    二进制格式
    print(s.encode(encoding='GBK'))     # GBK编码中，一个中文跟占两个字节
    print(s.encode(encoding='UTF-8'))   # UTF-8编码中，一个中文跟占三个字节
    # 解码
    # byte代表的就是二进制数据（字节类型的数据）
    byte = s.encode(encoding='GBK')
    print(byte.decode(encoding='GBK'))

    bytes = s.encode(encoding='UTF-8')
    print(bytes.decode(encoding='UTF-8'))