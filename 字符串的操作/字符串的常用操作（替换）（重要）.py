# -*- codeing = utf-8 -*-
# @Time : 2021/11/28 13:51
# @Author : 邢继森
# @File : 字符串的常用操作（替换）（重要）.py
# Software : PyCharm
if __name__ == "__main__":
    '''字符串的替换'''
    a = 'hello,python'
    print(a.replace('python','java'))       # 第一个参数原来的字符，第二个参数是第一个参数需要被替换成什么样个数据
    b = 'hello,python,python,python'
    print(b.replace('python','java',2))     # 第三个参数指定最大替换次数
    print('-'*50)
    '''字符串的合并'''
    lst = ['hello','python','python','python']
    print('|'.join(lst))
    print(''.join(lst))
    print('-'*50)

    t = ('hello','python','python','python')
    print(''.join(t))
    print('-'*50)

    print('*'.join('python'))


