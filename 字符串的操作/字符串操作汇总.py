# -*- codeing = utf-8 -*-
# @Time : 2021/11/30 10:48
# @Author : 邢继森
# @File : 字符串操作汇总.py
# Software : PyCharm
if __name__ == "__main__":
    # 查询操作
    a = 'Python,Python'
    print(a.index('n'))
    print(a.rindex('P'))
    print(a.find('P'))
    print(a.rfind('n'))
    # 大小写转换操作
    a = 'PythoN,PytHon'
    print(a.upper())
    print(a.lower())
    print(a.swapcase())
    print(a.capitalize())
    print(a.title())
    # 内容对齐操作
    a = 'python'
    print(a.center(20,'-'))
    print(a.ljust(20,'-'))
    print(a.rjust(20,'-'))
    print(a.zfill(20))
    # 劈分操作
    a = 'hello world python'
    print(a.split())
    b = 'hello*world*python'
    print(b.split(sep='*',maxsplit=1))
    print(b.rsplit(sep='*',maxsplit=1))
    # 字符串判断
    print('_fw95'.isidentifier())
    print('3_fw95'.isidentifier())
    print('  '.isspace())
    print('  \b'.isspace())
    print('asdas'.isalpha())
    print('asdas6'.isalpha())
    print('156'.isdecimal())
    print('01454F0'.isdecimal())
    print('6171'.isnumeric())