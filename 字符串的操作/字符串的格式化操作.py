# -*- codeing = utf-8 -*-
# @Time : 2021/11/28 14:57
# @Author : 邢继森
# @File : 字符串的格式化操作.py
# Software : PyCharm
if __name__ == "__main__":
    # % 占位符
    name = '邢继森'
    age = 19
    print('我的名字叫%s，今年%d岁了'%(name,age))
    # {} 占位符
    print('我的名字叫{0},今年{1}岁了'.format(name,age))
    # f-string 占位符
    print(f'我的名字叫{name},今年{age}岁了')



    '''精度与宽度'''
    print('%d' % 99)
    print('%10d' % 99)     # 宽度
    print('%f' % 3.1415926)
    print('%.3f' % 3.1415926)
    print('%10.3f' % 3.1415926)   # 总宽度为10 ，小数点后3位


    print('{}'.format(3.1415926))
    print('{0:.3}'.format(3.1415926))  # 一共3位数
    print('{0:.3f}'.format(3.1415926))
    print('{0:10.3f}'.format(3.1415926))