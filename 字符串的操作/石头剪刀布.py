#石头剪刀布
import random
import time

win_list = [("石头","剪刀"),("布","石头"),("剪刀","布")]
kind_list=['石头','剪刀','布']
print("================欢迎来到人机石头剪刀布大战，by杨叔叔===================")
while True:
    time.sleep(1)  # 等待时间
    print("请等待机器人出牌........")
    time.sleep(1)    # 等待时间

    print('.',end='')
    time.sleep(1)    # 等待时间
    print('.', end='')
    time.sleep(1)  # 等待时间
    print('.', end='')
    time.sleep(1)  # 等待时间

    time.sleep(1)    # 等待时间
    robot = random.choice(kind_list)    # choice() 方法返回一个列表，元组或字符串的随机项。相当于系统随机出牌
    print("机器人出牌完毕！")
    people = str(input("请你输入石头/剪刀/布：").strip())    # 将输入的值提取出来赋值给people
    time.sleep(1)    # 等待时间
    print("请等待系统判断~~")
    time.sleep(1)    # 等待时间
    if people in kind_list:
        print("您输入的是：",people)
        time.sleep(1)    # 等待时间
        print("机器人输入的是：",robot)
        time.sleep(1)  # 等待时间
        if people == robot:
            print("平局")
        elif (people,robot) in win_list:
            print("你赢了！~！！~游戏结束")
            break
        else:
            print("你输了~战胜机器人才能退出 ^.^,游戏继续")
    else:
        print("你的输入无效！重新开局！")