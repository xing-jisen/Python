# -*- codeing = utf-8 -*-
# @Time : 2021/11/27 21:10
# @Author : 邢继森
# @File : 字符串的常用操作（对齐）.py
# Software : PyCharm
if __name__ == "__main__":
    a = 'hello,Python'
    print(a.center(20,'*'))   # 居中对齐
    print('-'*50)
    print(a.ljust(20,'*'))    # 左对齐
    print(a.ljust(10,'*'))      # 如果宽度小，将返回原字符
    print(a.ljust(20))          # 如果只写一个参数并写大于字符串，则后面会有空格
    print('-'*50)
    print(a.rjust(20,'*'))    # 右对齐
    print(a.rjust(10, '*'))      # 如果宽度小，将返回原字符
    print(a.rjust(20))           # 如果只写一个参数并写大于字符串，则后面会有空格
    print(a.zfill(20))        # 右对齐，左边 0 填充
    print('-1234'.zfill(8))