# -*- codeing = utf-8 -*-
# @Time : 2022/1/13 14:05
# @Author : 邢继森
# @File : Ppython海龟绘图：Z形曲线.py
# Software : PyCharm
if __name__ == "__main__":
    import turtle
    turtle.colormode('1.0')
    turtle.left(45)
    turtle.fd(150)
    turtle.right(135)
    turtle.fd(300)
    turtle.left(135)
    turtle.fd(150)
