# -*- codeing = utf-8 -*-
# @Time : 2022/1/13 12:46
# @Author : 邢继森
# @File : Python海龟绘图：绘制蟒蛇.py
# Software : PyCharm
if __name__ == "__main__":
    import turtle
    turtle.setup(650,350,200,200)
    turtle.penup()
    turtle.fd(-250)
    turtle.pendown()
    turtle.pensize(25)  # 线条粗细
    turtle.pencolor("red")
    turtle.seth(-40)
    for i in range(4):
        turtle.circle(40,80)
        turtle.circle(-40, 80)
    turtle.circle(40,80/2)
    turtle.fd(40)
    turtle.circle(16,180)
    turtle.pencolor("green")
    turtle.fd(40 * 2 / 3)
    turtle.done()

