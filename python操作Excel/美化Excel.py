# -*- codeing = utf-8 -*-
# @Time : 2022/4/4 21:52
# @Author : 邢继森
# @File : 美化Excel.py
# Software : PyCharm
from openpyxl import Workbook
from openpyxl.styles import Font , colors , Alignment , PatternFill       # Excel美化
wb = Workbook()
sh = wb.active
# font =  Font(name = '微软雅黑' , size = '30' , bold= 'True' , italic=True ,color = colors.BLUE)
font =  Font(name = '微软雅黑' , size = '30' , bold= 'True' , italic=True ,color = 'FF0000')
                                                # 加粗           斜体          颜色
sh['B2'] = 'Hello'
sh['B2'].font =font

# 修改单元格
sh.row_dimensions[6].height = 30        # 高度
sh.column_dimensions['C'].width = 15    # 宽度
sh['C6'] = 'Python'

# 文字对齐
sh['C6'].alignment = Alignment(horizontal='right',vertical='top')   # 上下左右对齐

# 单元格填充
sh['H5'].fill = PatternFill('solid' , '00FF00')

wb.save('corstel/美化Excel.xlsx')