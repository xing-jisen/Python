# -*- codeing = utf-8 -*-
# @Time : 2022/3/25 23:11
# @Author : 邢继森
# @File : python操作Excel.py
# Software : PyCharm

# pip install openpyxl

def open():
    from openpyxl import load_workbook
    wb = load_workbook(r'corstel\test5.xlsx')

    # 三种方式激活
    sh1 = wb.active   # 默认激活第一个工作表
    sh2 = wb['Sheet'] # 推荐这种方法激活指定工作表
    sh3 = wb.get_sheet_by_name('Sheet') # 弃用，不推荐用

    print(sh1 == sh2 == sh3)


# 展示出工作薄
def show_sheets():
    from openpyxl import load_workbook
    wb = load_workbook(r'corstel\test5.xlsx')

    print(wb.sheetnames) # 一次性打印出来工作薄
    for i in wb:        # 遍历的方式
        print(i.title)


def get_one_value():
    from openpyxl import  load_workbook
    wb = load_workbook(r'corstel\test5.xlsx')

    sh1 = wb['Sheet']
    # 拿一个数据
    v1 = sh1.cell(1,1) # 第一个表格
    print(v1)
    print(v1.value)

    # 索引的方式
    v2 = sh1['A2'].value
    print(v2)

'''
    # 切片的方式
    v3 = sh1['A1:B2']
    for i in v3:
        print(i)
'''

def get_all_value():
    from openpyxl import load_workbook
    wb = load_workbook(r'corstel\test5.xlsx')

    # 获取所有数据
    sh = wb['Sheet']
    for row in sh.rows:     # 获取每一行
        print('-' * 9)
        for cell in row:    # 获取每一列
            print(cell.value)

    # sh.colums


# open()
# show_sheets()
# get_one_value()
# get_all_value()
x1 = 1
x1 += 1
        # 写入数据
        # dxt = [
        #     i['title'],
        #     i['avgScore'],
        #     i['allCommentNum'],
        #     i['address'],
        #     i['avgPrice']
        # ]
ax = 'A' + str(x1)
print(ax)