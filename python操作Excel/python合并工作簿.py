# -*- codeing = utf-8 -*-
# @Time : 2022/3/20 12:04
# @Author : 邢继森
# @File : python合并工作簿.py
# Software : PyCharm
# 1.操作Excel
from openpyxl import load_workbook,Workbook
# 2.打开Excel
wb = load_workbook('./corstel/a1.xlsx')
# 3.激活多个Shell
sh1 = wb['Sheet1']
sh2 = wb['Sheet2']

all = []                # 存储所有的数据

# 4.读取数据
'''合并几个就读取几个，获取sh1表的数据'''
for row in sh1.rows:    # 获取所有行
    tmp_list = []       # 专门用来存储一行的值
    for cell in row :   # 获取所有列
        tmp_list.append(cell.value)      # 获取单元格的值
    all.append(tmp_list)    # 每次存储进去一行# 整合所有的数据
    # 合并几个就读取几个

'''合并几个就读取几个，获取sh2表的数据'''
for row in sh2.rows:  # 获取所有行
    tmp_list = []     # 专门用来存储一行的值
    for cell in row:  # 获取所有列
        tmp_list.append(cell.value)  # 获取单元格的值
    all.append(tmp_list)  # 每次存储进去一行# 整合所有的数据
    '''
    sheet.append([value,value2value3])
    '''
# 保存所有的数据
wb = Workbook()
sh = wb.active

for row in all: # 遍历所有数据
    sh.append(row)

wb.save('../corstel/test5.xlsx')