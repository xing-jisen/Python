# -*- codeing = utf-8 -*-
# @Time : 2022/4/5 14:40
# @Author : 邢继森
# @File : 快速查找重复数据.py
# Software : PyCharm
from openpyxl import load_workbook
from openpyxl.styles import PatternFill


wb = load_workbook('corstel/加班时间.xlsx')
sh = wb.active      # 激活工作表

index = []      # 记录哪一行数据是重复的
tmp = []        # 记录没有重复的数据
for i,cell in enumerate(sh['B']):
    # print(cell.value)
    flag = cell.value not in tmp
    if flag:
        tmp.append(cell.value)
    else:
        index.append(i)

# print(tmp)
# print(index)
fill  = PatternFill('solid','AEEEEE')   #solid 实心 AEEEEE 颜色

for i,row in enumerate(sh.rows):    # 填充
    if i in index:
        for cell in row:
            cell.fill = fill
        print(f"第{i}行是重复数据")

wb.save('corstel/查找重复数据.xlsx')