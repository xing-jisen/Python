# -*- codeing = utf-8 -*-
# @Time : 2022/4/7 21:58
# @Author : 邢继森
# @File : python发送邮件功能.py
# Software : PyCharm
import smtplib
from email.mime.text import MIMEText
from email.header import Header

def send_simple():
    # 登陆邮箱
    smtp_obj = smtplib.SMTP('smtp.qq.com')  # 定义的语言规则
    smtp_obj.login('1933694610@qq.com' , 'lkmcalitcrqxehdc')
    # 编写内容
    msg = '你要的邮件来了  The mail you asked for has arrived'
    msg_body = MIMEText(msg,'plain','utf-8')        # 包装主题内容  plain 类型(普通)
    msg_body['Subject'] = Header('测试邮件','utf-8')    # 文件标题
    msg_body['From'] = Header('测试部门','utf-8')       #
    # 发送
    smtp_obj.sendmail('1933694610@qq.com',['17685893896@qq.com'],msg_body.as_string())
    #                       发送的邮箱           发送给谁               .as_string() 转成字符串文件

def send_html():
    # 登陆邮箱
    smtp_obj = smtplib.SMTP('smtp.qq.com')  # 定义的语言规则
    smtp_obj.login('1933694610@qq.com', 'lkmcalitcrqxehdc')
    # 编写内容
    msg = '''
        <h1>你要的邮件来了  The mail you asked for has arrived</h1>
        <p>测试一个邮件内容，内容是一个html编写的</p>
        <p><a href="http://www.baidu.com">官网</p>
    '''
    msg_body = MIMEText(msg, 'html', 'utf-8')  # 包装主题内容  html 类型(html邮件)
    msg_body['Subject'] = Header('测试邮件', 'utf-8')  # 文件标题
    msg_body['From'] = Header('测试部门', 'utf-8')  #
    # 发送
    smtp_obj.sendmail('1933694610@qq.com', ['17685893896@qq.com'], msg_body.as_string())
    #                       发送的邮箱           发送给谁               .as_string() 转成字符串文件


# send_simple()
send_html()