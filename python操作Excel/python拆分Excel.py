# -*- codeing = utf-8 -*-
# @Time : 2022/3/26 15:56
# @Author : 邢继森
# @File : python拆分Excel.py
# Software : PyCharm
from openpyxl import load_workbook

wb = load_workbook(r'corstel\a1.xlsx')
# 激活工作簿
sh = wb.active

data1 = [] # 少于300的数据
data2 = [] # 大于300的数据

# 读数据
for row in sh.rows:         # 获取列
    num = row[2].value
    # print(num)
    if num >= 300:
        data2.append(row)
    else:
        data1.append(row)


data1_sh = wb.create_sheet('少于300')
data2_sh = wb.create_sheet('大于300')


for d in data1:
    t_list = []
    for t in d:
        t_list.append(t.value)
    data1_sh.append(t_list)

for d in data2:
    t_list = []
    for t in d:
        t_list.append(t.value)
    data2_sh.append(t_list)
# 保存
wb.save(r'corstel\test4.xlsx')
print('结束')
