# -*- codeing = utf-8 -*-
# @Time : 2022/3/29 19:15
# @Author : 邢继森
# @File : 合并多个文档.py
# Software : PyCharm

# 引入包
from openpyxl import load_workbook,Workbook
# 打开Excel(合并几个打开几个)
wb1 = load_workbook(r'corstel\test4.xlsx')
wb2 = load_workbook(r'corstel\test5.xlsx')
wb3 = load_workbook(r'corstel\a1.xlsx')
wb4 = load_workbook(r'corstel\a2.xlsx')     # alt + shift + 下

# 激活工作表
sh1 = wb1.active
sh2 = wb2.active
sh3 = wb3.active
sh4 = wb4.active
# 定义变量存储数据
all = []
# 读取数据(读数据记得要保存)
for row in sh1.rows:
    tmp_list = []
    for cell in row:
        tmp_list.append(cell.value)
    all.append(tmp_list)

for row in sh2.rows:
    tmp_list = []
    for cell in row:
        tmp_list.append(cell.value)
    all.append(tmp_list)

for row in sh3.rows:
    tmp_list = []
    for cell in row:
        tmp_list.append(cell.value)
    all.append(tmp_list)

for row in sh4.rows:
    tmp_list = []
    for cell in row:
        tmp_list.append(cell.value)
    all.append(tmp_list)
# 保存数据
new_wb = Workbook() # 激活

new_sh = new_wb.active  # new_wb.aceater_sheet('video')

for row in all:         # 获取所有数据的每一条
    new_sh.append(row)

new_wb.save(r'corstel\t1.xlsx')