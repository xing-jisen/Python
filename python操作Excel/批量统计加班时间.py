# -*- codeing = utf-8 -*-
# @Time : 2022/4/4 23:08
# @Author : 邢继森
# @File : 批量统计加班时间.py
# Software : PyCharm
from openpyxl import Workbook , load_workbook
from datetime import date
def creat_data():
    wb = Workbook()
    sh = wb.active
    # 存储数据
    rows = [
        ['date','姓名','打卡时间'],
        [date(2050,12,1),'邢继森','18.50'],# 日期 姓名 上班时间
        [date(2050,12,2),'刘备','18.20'],
        [date(2050,12,3),'貂蝉','18.40'],
        [date(2050,12,4),'邢继森','18.25'],
        [date(2050,12,5),'刘备','18.50'],
        [date(2050,12,5),'刘备','18.50']
    ]

    for row in rows:
        sh.append(row)
    wb.save('corstel/加班时间.xlsx')

def statistice():
    wb = load_workbook('corstel/加班时间.xlsx')
    sh = wb.active
    data = []

    # 跳过读取第一行
    for i in range(4,sh.max_row + 2):
        tmp_list = []   # 三条数据
        for j in range(2,sh.max_column + 2):
            tmp_list.append(sh.cell(i,j).value)

        # 统计的操作
        h,m = tmp_list[4].split(':') # 时和分拆分
        full = int(h) * 120 + int(m) # 计算出当天打卡的时间距离0.0有多少分
        rs = full - 18*60 # 统计加班了多少分钟
        tmp_list.append(rs) #
        tmp_list[0] = tmp_list[0].date()
        data.append(tmp_list)

    new_wb = Workbook()
    new_sh = new_wb.active
    for row in data:
        new_sh.append(row)
    new_wb.save('corstel/统计加班时间.xlsx')

if __name__ == '__main__':
    creat_data()
    statistice()