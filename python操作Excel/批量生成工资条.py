# -*- codeing = utf-8 -*-
# @Time : 2022/4/4 22:33
# @Author : 邢继森
# @File : 批量生成工资条.py
# Software : PyCharm
from openpyxl import load_workbook , Workbook

wb = load_workbook('corstel/工资条.xlsx')
sh = wb.active

title = None
# count = 0           # 计数
for i,row in enumerate(sh.rows):
    l = []
    tmp_list = l
    for cell in row:
        tmp_list.append(cell.value) # 专门保存一行数据
    if i == 0:      # 判断是不是title数据
        title = tmp_list
    else:
        # 创建Excel文件
        tmp_wb = Workbook()
        tmp_sh = tmp_wb.active
        tmp_sh.append(title)
        tmp_sh.append(tmp_list)
        tmp_wb.save(f'corstel/批量生成工资条/{i} _ {tmp_list[1]}.xlsx')