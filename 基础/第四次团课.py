
'''num = 10
if num%2==0:
    print('偶数')
else:
    print('奇数')'''

#判断一个数是不是偶数
'''
num = int(input('请输入一个数'))
if num%2==0:
    print('偶数')
else:
    print('奇数')'''

#判断一个数是否是三的倍数
'''num = int(input('请输入一个数:'))
if num%3==0:
    print('是三的倍数')
else:
    print('不是三的倍数')'''

#1+2+3+......+10
'''font = 0
for i in range(1,11):
    font = font + i
print('结果是:',font)'''

# 单分支语句
# 多分支语句
'''data = input('是不是数字:')
if type(data) is str:
    print(data,'是字符串')
else:
    print(data,'不是字符串')'''

#
'''x = {'one':1,'two':2,'three':3}
for i in x:
    print(i)
    print(x[f'{i}'])'''

#判断数字在哪组数字之间
'''x = int(input('请输入数字:'))
if 0 < x < 20:
    print('在0和20之内')
elif x < 30:
    print('在20和30之间')
else:
    print('nice!!!')'''


'''
列表可以存放任意元素      定义形式：x = [1,2,3]
li.append   插入列表    使用方法： li.append([4,5,6])

字典                   定义形式：x = {'one':1,'two':2,'three':3}             'three':3      一个键值对
                       print(x['one'])
                       
                        定义形式：x = {'one':1,'two':2,'three':3} 
                        for i in dic:
                      print(x[f'{i}'])
'''

#字符串输出另一种方式
'''str_a = '济南'
str_b = f'山东商业职业技术学院在{str_a}'
print(str_b)'''str = '我来自山东菏泽'
print(str[4:6])

#fot循环输出1+6的总和
'''a = 0
for i in range(1,7):
    a = a + i
print(a)'''

#切片输出
'''str = '我来自山东菏泽'
print(str[4:6])
str = '我来自山东菏泽'
print(str[4:7])
str = '我来自山东菏泽'
print(str[1:6])'''

#翻转遍历
for i in range(6,0,-1):
    print(i)