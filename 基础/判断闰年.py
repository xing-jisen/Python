# -*- codeing = utf-8 -*-
# @Time : 2022/1/14 13:05
# @Author : 邢继森
# @File : 判断闰年.py
# Software : PyCharm
if __name__ == "__main__":
    pd = eval(input('输一个四位数的年份:'))
    if (pd % 4 == 0 and pd % 100 != 0) or (pd % 400 == 0):
        print('%d年是闰年' %(pd))
    else:
        print('%d年是平年' %(pd))