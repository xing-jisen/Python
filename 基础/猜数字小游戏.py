# -*- codeing = utf-8 -*-
# @Time : 2022/1/11 23:15
# @Author : 邢继森
# @File : 猜数字小游戏.py
# Software : PyCharm
import random
a = [random.randint(0,100) for i in range(1)]
x = int(input("猜数字"))
for i in range(100):
    if x in a:
        print("ok")
        break
    elif x not in a:
        print("no")
        x = int(input("不正确，请再次输入"))
    else:
        print("输入的值不正确")
        continue
