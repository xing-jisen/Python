# -*- codeing = utf-8 -*-
# @Time : 2021/11/25 10:51
# @Author : 邢继森
# @File : 格式化输出.py
# Software : PyCharm

age = 18
print('我今年:%d岁'%age)
print('我的国籍是%s，我的名字是%s' %("中国","邢继森"))

# 小知识点
print('aaa'  'bbb','ccc')

print('www','baidu','com',sep='.')

print('hello',end='')
print('world',end='\t')
print('python',end='\n')
print('world')
print('你'
      '好')