# -*- codeing = utf-8 -*-
# @Time : 2021/12/10 9:26
# @Author : 邢继森
# @File : 列表切片.py
# Software : PyCharm
a = [1,2,3,4,5,6,7,8,9]
print(a[0:5:1])
print(a[4:])
print(a[:5])
print(a[1::2])
print(a[::-1])
print(a[6::-1])
print(a[6:0:-2])