# -*- codeing = utf-8 -*-
# @Time : 2022/1/11 19:21
# @Author : 邢继森
# @File : 作业1.py
# Software : PyCharm
# if __name__ == "__main__":
#     X = input()
#     if X[-1] in ['c','C']:
#         F = eval(X[0:-1]) * 1.8 + 32
#         print("{:.2f}F".format(F))
#     elif X[-1] in ['f','F']:
#         C = (eval(X[0:-1]) - 32) / 1.8
#         print("{:.2f}C".format(C))
#     else:
#         print("输入格式错误")
a = [100,200,300,400,500,600,700,800,900,1000]
b = [i * 0.5 for i in a]
print(b)
c = [i for i in b if i > 400]
print(c)
x = a.pop(8)
print(x)
print(a)

z = tuple(a)
print(z)
for i,j in enumerate(z):
    print(i+1,j)

# # 首先获得Iterator对象:
# it = iter([1, 2, 3, 4, 5])
# # 循环:
# while True:
#     try:
#         # 获得下一个值:
#         x = next(it)
#         print(x)
#     except StopIteration:
#         # 遇到StopIteration就退出循环
#         break


x1 = (i for i in range(3))
print(x1.__next__())
print(x1.__next__())

x2 = tuple(x1)
print("转换后",x2)


# 字典
a1 = ['一','二','三','四','五']
a2 = [1,2,3,4,5]
a3 = dict(zip(a1,a2))
print(a3)
# print({a1:a2})

print(a3.popitem())
print(a3)
print(a3.pop('一'))
print(a3)
print('666' if '二' in a3 else '没有')


x1 = ['星座1','星座2','星座3']
x2 = ['星座4','星座5','星座6']
x3 = ['1','2','3']
x0 = dict(zip(x1,x2))
print(x0)
x00 = dict(zip(x3,x0))
print(x00)

for i in x0.values():
    print(i)

a0 = {'一':'1','二':'2','三':'3','四':'4','五':'5'}
a0['四'] = '5'
print(a0)
del a0['一']
print(a0)
import random
# a = random.randint(1,100)
# print(a)
a = {i:random.randint(10,100) for i in range(1,10)}
print(a)
for i in (1,2,3):
    print(i)


print('-'*100)
for i in range(1234,9999):
    x = i
    q = i % 10
    i = i // 10

    w = i % 10
    i = i // 10

    e = i % 10
    r = i // 10

    if x == q*q*q*q + w*w*w*w + e*e*e*e + r*r*r*r:
        print(x)

print(21/10)
print(21//10)
print('-'*100)
i = 1
s = 0
while i <= 100:
    s += i
    i += 1
else:
    print('1-100累加和',s)

print('-'*100)





