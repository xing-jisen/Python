x = 0
print('第一题结果是:')
for i in range(1,100):
    if i%7==0 or i % 10==7:    # 7在里面也可以这样  if '7' in str(i)
        print(i)
        x+=1
print('运行了',x,'次')
#结果应该是30次

# a = 2
# b = 3
# x = 0
# for i in range(0,30):
#     x = x + (a/b)
#     y = b
#     z = a
#     a = y + 1
#     b = z + b
# print('第二题结果是:',x)

# in
# str = '山东济南'
# if '济南' in str:
#     print('存在')
# else:
#     print('不存在')

# eval 万能

# not in
# a = 1
# li = [1,2,3,4,5,6,7,8,9,20,30,50]
# if a not in li:
#     print('不在')
# else:
#     print('在')


# x = open('text.txt',mode='w',encoding='utf-8')
# for i in range(20):
#     x.write('菏泽\n')

#打印到电脑上
# f=open('text.txt',mode='a+',encoding='utf-8')   #不写地址，默认在源目录打印      w写入模式，会覆盖原有内容
#                                                 #r只读   w写入模式，覆盖原有内容  a+新建、追加  a追加
# for i in range(1000):      #迭代器:迭代五次
#     f.write('山东菏泽\n')


# asdf = {'one':'1','two':'2'}
# for i in asdf.values():
#     print(i)