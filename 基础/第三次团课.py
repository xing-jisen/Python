#字典
'''a = [11,22,33,44,55,'hello','你好']
#a = [0, 1, 2, 3, 4,    5,     6]    下标
b = [66]
a.append('asdfghjk')   #爬取数据   就是向字典中插入一个值
a.append(b)
print(a)
print('--------------------------------------------------------')
b.append(a)
print(b)'''

'''a = [11,22,33,44,55,'hello','你好']
#a = [0, 1, 2, 3, 4,    5,     6]    下标
for i in a:        #一次性抽取一个值输出
    print(i)'''

#函数
'''def Ts():                    #如果想定义一个函数，先输入def    def Ts()   【Ts()就是变量名】
    print('hello world')

Ts()                         #如果后面不加Ts()函数，则不会输出'''

'''def Ts(a,b):      #(a,b)形式参数 (形参)
    c = a + b
    return c
Res = Ts(5,5)
print(Res)
#相当于C语言里面的子函数'''

'''def Ts(a,b):
    c = a + b
    print(c)
Ts(5,5)'''

def index():
    print('hello,world')
for i in range(4):     #迭代器:迭代四次

    index()

for i in range(4):
    print(i)

'''#打印到电脑上
f=open('text.txt',mode='a+',encoding='utf-8')   #不写地址，默认在源目录打印      w写入模式，会覆盖原有内容
                                                #r只读   w写入模式，覆盖原有内容  a+新建、追加  a追加
for i in range(5):      #迭代器:迭代五次
    f.write('山东菏泽\n')'''