#1--100偶数和
s = 0
for i in range(1,101):
    if i % 2 == 0:
        s += i
print(s)


#1--100奇数和
s = 0
for i in range(1,101):
    if i % 2 == 1:
        s += i
print(s)


for _ in range(5):      # 可以用_来代替变量，这样就可以输出输出函数里面的内容
    print('人生苦短')

i = 9
li = []
while i > 0:
    print(' '.join(li[i-1::-1]))
    del li[0:i]
    i -= 1