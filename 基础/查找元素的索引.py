li = [132,456,789,12,1,1,46,5,1,3,6,5]
print(li.index(789))    # 查找 789 的索引值
print(li.index(1,1,8))  # 查找 1 - 8 之间元素为 1 的索引

lt = ['hello','world','hello',98,56]
print(lt[3])
print(lt[-2])

print(li[1:8:2])    # 切片
print(li[::-1])     # 反向输出
print(li[12::-1])   # 反向输出
print(li[12::-2])   # 反向输出

print(132 in li)
for i in li:
    print(i)   # 赋给i 依次输出
#添加

print('--------------------------------------------')
li.append(999)
print(li)
li.extend('999')
print(li)
li.insert(-1,999)
print(li)
li[1:]
print(li)
li[3:] = lt
print(li)
# 删除

print('--------------------------------------------')
lt = ['hello','world','hello',98,56,23,4,5,677]

lt.remove(98)      # 删除指定
print(lt)

lt.pop(4)          # 删除指定
print(lt)
lt.pop()          # 不指定参数，删除最后一个
print(lt)

# 切片，产生新的列表
li = lt[1:3]
print(li)

# 直接删除
lt[1:3]=[]
print(lt)

lt.clear()    # 清空所有元素
print(lt)

# del li        # 删除列表
# print(li)
# # 修改里面的值

print('------------------------------')
lt = [10,20,30,40,50,60,70,80,90,100]
lt[0] = 666        # 修改列表里面指定的值
print(lt)

lt[1:3]=[5,5]
print(lt)
# 修改里面的值

print('-----------sort对原列表操作--sorted产生一个新的---------')
lt = [26,5,3,8,5,6]
print('排序前',lt)
lt.sort()              # 默认从小到大    在原列表进行
print('排序后',lt)
lt.sort(reverse=True)  # 从大到小
print(lt)
lt.sort(reverse=False)  # 从小到大
print(lt)

lt = [26,5,3,8,5,6]
print('排序前',lt)
li = sorted(lt)          # 会产生新的值
print(li)

li = sorted(lt,reverse=True)   # 从大到小
print(li)
li = sorted(lt,reverse=False)   # 从小到大
print(li)

print('---------------------------------')
lt = [i for i in range(1,10)]
print(lt)
lt = [i*i for i in range(1,10)]      # i = 2 时,i * i = 4
print(lt)
lt = [i*2 for i in range(1,6)]      # i = 2 时,i * 2 = 4
print(lt)