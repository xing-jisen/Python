# -*- codeing = utf-8 -*-
# @Time : 2022/1/15 22:50
# @Author : 邢继森
# @File : 模拟手机通讯录.py
# Software : PyCharm
if __name__ == "__main__":
    address_book = set()
    for i in range(1,6):
        entering = input('请输入第' + str(i) + '个朋友的姓名与手机号码:')
        address_book.add(entering)
    print("\n通讯录如下")
    for i in address_book:
        print(i)