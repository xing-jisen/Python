# -*- codeing = utf-8 -*-
# @Time : 2022/1/14 14:15
# @Author : 邢继森
# @File : 石头剪刀布.py
# Software : PyCharm
import random
if __name__ == "__main__":
    print('''
    *******欢迎来到6666游戏平台*******
             石头   剪刀   布
    *******************************
    ''')
    player = input("输入玩家姓名:")
    print('1.貂蝉 2.吕布 3.曹操')
    role1 = eval(input("请选择要对战的的角色:"))
    if role1 == 1:
        compter_name = '貂蝉'
    elif role1 == 2:
        compter_name = '吕布'
    elif role1 == 3:
        compter_name = '曹操'
    else:
        compter_name = '匿名'

    print(player," VS ",compter_name)
    # 次数
    player_score = 0
    compter_score = 0
    while True:
        # 玩家出拳
        player_fist = eval(input('------------1. 石头  2. 剪刀  3. 布------------\n请出拳:'))
        if player_fist == 1:
            player_fist_name = '石头'
        elif player_fist == 2:
            player_fist_name = '剪刀'
        elif player_fist == 3:
            player_fist_name = '布'
        else:
            player_fist_name = '石头'
            player_fist = 1
        # 电脑出拳
        compter_fist = random.randint(1,3)
        if compter_fist == 1:
            compter_fist_name = '石头'
        elif compter_fist == 2:
            compter_fist_name = '剪刀'
        elif compter_fist == 3:
            compter_fist_name = '布'

        print(player,"出拳:",player_fist_name)
        print(compter_name, "出拳:", compter_fist_name)
        if player_fist == compter_fist:
            print('平局')
        elif (player_fist == 1 and compter_fist == 2) or (player_fist == 2 and compter_fist == 3) or (player_fist == 3 and compter_fist == 1):
            print('玩家:',player,'胜')
            player_score += 1
        else:
            print('电脑:',compter_name,'胜')
            compter_score += 1
        answer = input("再来一局不?y/n")
        if answer != 'y':
            break

    # 比赛结果
    print('-'*30)
    print(player,'得分:',player_score)
    print(compter_name,'得分:',compter_score)
    print('-'*30)
    if player_score > compter_score:
        print('玩家:',player,'获胜')
    elif compter_score > player_score:
        print('电脑:',compter_name,'获胜')
    else:
        print(player,compter_name,'你俩平局')

    input()