lt = [1,2,3,'gfh',45,'dg']
lt1 = ['链接','连接']
print(lt)
#尾端添加一个元素       常用
lt.append(123)
print(lt)
#一个列表连接到另一个列表后面
lt.extend(lt1)
print(lt)
lt.extend('ad')
print(lt)
#插入到指定位置
lt.insert(3,4)
print(lt)
#两个列表相连；注意：需要设置一个新的存储空间来接收
lt2 = lt + lt1
print(lt2)

print(lt + lt1)

print('*'*99)
# 删除元素
print(lt)
lt.remove('a')
print(lt)

lt.pop(10)  # 根据索移除元素    索引不存在抛出异常
print(lt)
lt.pop()    # 不写参数删除列表最后一个元素
print(lt)

# 切片，一次提取出来一个元素，切片会生成新的列表对象
lt1 = lt[1:5]
print(lt1)

lt.clear()    # 清楚所有元素,删除列表
print(lt)
lt = [1,2,3,4,5,6,7,8,9]


# 修改列表元素
print(lt)
lt[2] = '你好'
print(lt)

lt[3:5] = [333,666,999]
print(lt)


# 列表的排序
li = [1,2,5,1,2,5,6,9]
print('排序前',li)
li.sort()                 # 默认升序排序
print('排序后',li)

li.sort(reverse=True)      # True 降序排序    False 降序排序
print(li)

print('*'*99)
# 调用内置函数sorted（）排序，会产生新的列表对象
li = [1,2,5,1,2,5,6,9]
print('排序前',li)
lt  = sorted(li,reverse=True)   # True 降序排序    False 降序排序
print('排序前',lt)


print('*'*99)
# 列表生成式
lst = [i for i in range(1,10)]
print(lst)

lst = [i*i for i in range(1,10)]
print(lst)