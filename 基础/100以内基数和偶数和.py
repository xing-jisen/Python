#偶数和  while 循环

s = 0                # s 初始值为0，如果不设置，后面s的值不确定，就没法相加
a = 1                # 进行1---100的累加
while a < 101:       # 也可以 a <= 100
    if a % 2 == 0:   # 偶数 % 2之后余0，就执行 s + a
        s = s + a
    a = a + 1        # a进行累加
print(s)

# 偶数和 for 循环
s = 0
for i in range(1,101):   # 1 对应 i ，从 1 开始累加到100
    if i % 2 == 0:
        s = s + i      # 这里没有加 1 了，因为 range(1,100)就相当于从 1 开始到 100 结数
print(s)


#奇数和  while 循环
s = 0
a = 1
while a < 101:
    if a % 2 != 0:    #就修改了一个 == 变为 ！=
        s = s + a
    a = a + 1
print(s)

# 偶数和 for 循环
s = 0
for i in range(1,101):
    if i % 2 != 0:    #也是就修改了一个 == 变为 ！=
        s = s + i
print(s)