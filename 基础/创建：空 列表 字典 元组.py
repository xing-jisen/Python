# 可变序列:值变了,地址不一定变 不可变序列:值变了,地址一定变
# 空列表[]
a = []
a1 = list()
print(a,a1,type(a),type(a1))

# 空字典{}
b = {}
b1 = dict()
print(b,b1,type(b),type(b1))

# 空元组()
c = ()
c1 = tuple()
print(c,c1,type(c),type(c1))

# 空集合set()
d = set()
print(d,type(d))