# -*- codeing = utf-8 -*-
# @Time : 2022/1/14 13:39
# @Author : 邢继森
# @File : 猜数字游戏.py
# Software : PyCharm
import random
if __name__ == "__main__":
    print("----------猜数游戏----------")
    rand = random.randint(1,100)
    cont = 0
    while cont <= 10:
        x = eval(input("在我心中有个数，1-100之间，请你猜一猜:"))
        cont += 1
        if cont == 10:
            print("你已经猜了%d次了,不能猜了" % cont)
            break
        if x > rand:
            print("大了")
        elif x < rand:
            print("小了")
        else:
            if cont <= 3:
                print("你真聪明,猜了%d次了" % cont)
                break
            elif cont <= 6:
                print("还可以,猜了%d次了" % cont)
                break
            elif cont <= 10:
                print("猜的次数有点多啊，长点心吧，猜了%d次了" % cont)
            break
