a = {10,20,30,40}
b = {20,30,40,50,60}

# 交集
print(a.intersection(b))
print(a & b)
print('*'*50)

# 并集
print(a.union(b))
print(a | b)
print('*'*50)

# 差集  所有属于A且不属于B的元素的集合被称为A与B的差集
print(a.difference(b))
print(a - b)
print('*'*50)

# 对称差集
print(a.symmetric_difference(b))