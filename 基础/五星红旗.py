# -*- codeing = utf-8 -*-
# @Time : 2022/1/17 17:10
# @Author : 邢继森
# @File : 五星红旗.py
# Software : PyCharm
import turtle as t
if __name__ == "__main__":
    t.setup(760,800)# 初始化窗口
    # 五星红旗画布
    t.penup()  # 抬笔
    t.penup()  # 抬笔
    t.goto(-350,300)  # 箭头前往绝对角度
    t.right(90)  # 右转90度
    t.pendown()   # 落笔
    t.color('red', 'red')  # 第一个参数画笔颜色，第二个参数填充颜色
    t.begin_fill()  # 开始填充
    t.fd(300)   #向前进300像素
    t.left(90)  # 左转90度
    t.fd(600)   #向前进600像素
    t.left(90)  # 左转90度
    t.fd(300)   #向前进300像素
    t.left(90)  # 左转90度
    t.fd(600)   #向前进600像素
    t.end_fill()  # 结束填充
    t.penup()   # 抬笔
    t.right(36) # 往右40度

    t.color('yellow', 'yellow')  # 第一个参数画笔颜色，第二个参数填充颜色
    # 五星红旗大星星
    t.goto(-250, 180)  # 箭头前往绝对角度
    t.pendown()  # 落笔
    t.begin_fill()  # 开始填充
    for i in range(5):
        t.forward(80)  # 长度80像素
        t.right(144)  # 转角144度
    t.end_fill()  # 结束填充
    t.penup()  # 抬笔

    # 五星红旗第一颗星星
    t.goto(-180,250)  # 箭头前往绝对角度
    t.left(45) # 左转45度
    t.pendown()   # 落笔
    t.begin_fill()  # 开始填充
    for i in range(5):
        t.forward(20)  # 长度300像素
        t.right(144)  # 转角144度
    t.end_fill()  # 结束填充
    t.penup()   # 抬笔
    # 五星红旗第二颗星星
    t.goto(-160,220)  # 箭头前往绝对角度
    t.right(20) # 右转20度
    t.pendown()  # 落笔
    t.begin_fill()  # 开始填充
    for i in range(5):
        t.forward(20)  # 长度20像素
        t.right(144)  # 转角144度
    t.end_fill()  # 结束填充
    t.penup()  # 抬笔
    # 五星红旗第三颗星星
    t.goto(-160, 190)  # 箭头前往绝对角度
    t.right(25) # 右转25度
    t.pendown()  # 落笔
    t.begin_fill()  # 开始填充
    for i in range(5):
        t.forward(20)  # 长度20像素
        t.right(144)  # 转角144度
    t.end_fill()  # 结束填充
    t.penup()  # 抬笔
    # 五星红旗第四颗星星
    t.goto(-180, 160)  # 箭头前往绝对角度
    t.right(25) # 右转25度
    t.pendown()  # 落笔
    t.begin_fill()  # 开始填充
    for i in range(5):
        t.forward(20)  # 长度20像素
        t.right(144)  # 转角144度
    t.end_fill()  # 结束填充
    t.penup()  # 抬笔
    t.goto(-350,298)  # 箭头前往绝对角度
    t.pendown()  # 落笔
    t.width(5)  # 设置线的粗细为5像素
    t.seth(-90) # 绝对角度转90度
    t.color('gray')  # 旗杆颜色
    t.fd(650)   #向前进650像素
    t.penup()  # 抬笔
    t.ht() # 隐藏箭头
    t.done() # 结束不退出