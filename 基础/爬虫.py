import telnetlib
import requests
from bs4 import BeautifulSoup

url = 'https://www.xbiquge.la/24/24770/'

# user-agent伪装，目的是想让服务器认为我们是通过浏览器访问的
headers = {
    'User-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
}

# 获得一个网页上面的全部文本数据
text1 = requests.get(url=url,headers=headers).text

# 数据解析
jiexi = BeautifulSoup(text1,'lxml')  # 将互联网上请求的页面源码数据加载到 jiexi 对象中

list = jiexi.select('.box_con > # list > dl > dd')  # 所有数据的列表

xjs = open('./sanguo.txt','w',encoding='utf-8')
for i in list:
    title = i.a.string
    url1 = 'https://www.xbiquge.la' + i.a['href']

    # 单独对详情页请求获取源码数据
    page_text = requests.get(url = url1,headers=headers).text
    soup = BeautifulSoup(page_text,'html.parser')
    content = soup.find('div',id = 'content').text

    xjs.write(title+'\n'+content+'\n')
    print(title,':爬取成功!')
xjs.close()