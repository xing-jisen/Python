a = [1,5,4,5,7,78,74,45,5,7,8,4,5798]
b = [12,4,84,4,45,45,45,15,1,12,12,12,12]
print(a+b)

print('序列的长度和大小值总和  *'*10)
print('长度:',len(b))
print('最小值:',min(b))
print('最大值:',max(b))
print('总和:',sum(b))

print('遍历列表  *'*10)
for i in b:
    print('0-0  ',i)

x = b.count(45)
print(x)




print('二维列表')

ax = [['千','山','鸟','飞','绝'],
    ['万','径','人','踪','灭'],
    ['孤','舟','蓑','笠','翁'],
    ['独','钓','寒','江','雪']]
print(ax)

print('for循环创建二维列表')
arr = []
for i in range(4):
    arr.append([])
    for j in range(5):
        arr[i].append(j)

print('列表推导式')
axx =  [[j for j in range(5)] for i in range(4)]
print(axx)
print('二维列表根据下表引用，axx[1][3]为:',axx[1][3])