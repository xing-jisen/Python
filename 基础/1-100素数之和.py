# -*- codeing = utf-8 -*-
# @Time : 2022/1/12 16:20
# @Author : 邢继森
# @File : 1-100素数之和.py
# Software : PyCharm
if __name__ == "__main__":
    primesum = 0
    for i in range(2, 100):
        x = 1
        for n in range(2, i - 1):
            if i % n == 0:
                x = 0
                break
        if x == 1:
            primesum += i
    print(primesum)
