# -*- codeing = utf-8 -*-
# @Time : 2022/1/15 22:15
# @Author : 邢继森
# @File : 模拟12306火车票订票流程.py
# Software : PyCharm
if __name__ == "__main__":
    # 创建字典，用于存储车票信息，使用车次作为key，使用其他信息作为calue
    dict_ticket={
        'G1596': ['北京南-天津南', '18:06', '18:39', '00:33'],
        'G1567': ['北京南-天津南', '18:15', '18:49', '00:34'],
        'G8917': ['北京南-天津西', '18:20', '19:19', '00:59'],
        'G203': ['北京南-天津南', '18:35', '19:09', '00:34']
    }
    print('车次\t\t出发站-到达站\t\t出发时间\t\t到达时间隔\t历时时长')
    # 遍历元素
    # 遍历键
    for i in dict_ticket:
        print(i,end="\t")
        # 根据键遍历值
        for j in dict_ticket.get(i):
            print(j,end="\t\t")
        print()
    checi = input('请输入要购买的车次:')
    # 根据键获取值
    info = dict_ticket.get(checi,'车次不存在')   #info表示的车次信息
    # 判断车次是否存在
    if info != '车次不存在':
        chengcheren = input('请输入乘车人，多个乘车人用逗号分割:')
        # 获取车次的出发站-到达站、出发时间
        s = info[0] + '\t' + info[2] + '开'
        print('您已购买了' + checi + '\t' + s + '请' + chengcheren + '尽快换取纸质车票。【铁路客服】')
    else:
        print(info)