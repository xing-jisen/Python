# -*- codeing = utf-8 -*-
# @Time : 2022/1/15 20:48
# @Author : 邢继森
# @File : ’千年虫‘是什么虫.py
# Software : PyCharm
if __name__ == "__main__":
    lst = [88,89,90,98,0,99]
    print('原列表:',lst)
    for index in range(len(lst)):
        if lst[index] != 0:
            lst[index] = '19'+str(lst[index])
        else:
            lst[index] = '200'+str(lst[index])
    print('修改后:',lst)


    # 第二种方法
    lst = [88, 89, 90, 98, 0, 99]
    print('原列表:', lst)
    for i,j in enumerate(lst):
        if lst[i] != 0:
            lst[i] = '19' + str(lst[i])
        else:
            lst[i] = '200' + str(lst[i])
    print('修改后:', lst)