# -*- codeing = utf-8 -*-
# @Time : 2022/1/11 13:36
# @Author : 邢继森
# @File : 练习2.py
# Software : PyCharm
if __name__ == "__main__":
    x = float(input("请输入父亲的身高:"))
    y = float(input("请输入母亲的身高:"))
    print("预测儿子的身高为:" , round(((x + y) * 0.54),2))
    # round  取两位小数