# -*- codeing = utf-8 -*-
# @Time : 2022/1/13 10:31
# @Author : 邢继森
# @File : 温度转换FC.py
# Software : PyCharm
if __name__ == "__main__":
    TempSte = input("请输入带有符号的温度值:")
    if TempSte[-1] in ['F','f']:
        C = (eval(TempSte[0:-1]) - 32) / 1.8
        print("转换后的温度是{:.2f} C " .format(C))
    elif TempSte[0] in ['F','f']:
        C = (eval(TempSte[1:]) - 32) / 1.8
        print("转换后的温度是{:.2f} C ".format(C))
    elif TempSte[-1] in ['C', 'c']:
            F = 1.8 * eval(TempSte[0:-1]) + 32
            print("转换后的温度是{:.2f} C ".format(F))
    elif TempSte[0] in ['C', 'c']:
        F = 1.8 * eval(TempSte[1:]) + 32
        print("转换后的温度是{:.2f} C ".format(F))
    else:
        print("输入格式错误")

eval('print("Hello")')
