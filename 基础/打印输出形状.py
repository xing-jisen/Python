# -*- codeing = utf-8 -*-
# @Time : 2022/1/13 16:03
# @Author : 邢继森
# @File : 打印输出形状.py
# Software : PyCharm
if __name__ == "__main__":
    print('长方形','-' * 100)
    # 长方形
    for i in range(3):
        for j in range(4):
            print('*',end="")
        print()

print('正直角三角形','-'*100)
# 正直角三角形
for i in range(6):
    for j in range(i):
        print('*',end="")
    print()

print('反直角三角形','-'*100)
# 反直角三角形

for i in range(5):
    for j in range(5-i):
        print('*',end="")
    print()


print('等腰直角三角形','-'*100)
# 等腰三角形
for i in range(1,6):
    for j in range(1,6-i):
        print(' ',end="")
    for z in range(1,i*2):
        print('*',end="")
    print()

print('菱形','-'*100)
# 菱形
s = eval(input('请输入菱形的行数(奇数):'))
while s % 2 == 0:
    s = eval(input('都给你说了偶数不行偶数不行，你还输偶数，重新输:'))
t_s = (s + 1) // 2
# 上半部分
for i in range(1,t_s +1):
    for j in range(1,t_s + 1 - i):
        print(' ',end="")
    for z in range(1,i * 2):
        print('*',end="")
    print()

# 下半部分
b_s = s // 2
for i in range(1,b_s + 1):
    for j in range(1 , i + 1):
        print(' ',end="")
# 倒三角形
    for k in range(1 , s - i * 2 + 1):
        print('*',end="")
    print()




print('空心菱形','-'*100)
# 空心菱形
s = eval(input('请输入空心菱形的行数(奇数):'))
while s % 2 == 0:
    s = eval(input('都给你说了偶数不行偶数不行，你还输偶数，重新输:'))
t_s = (s + 1) // 2
# 上半部分
for i in range(1,t_s +1):
    for j in range(1,t_s + 1 - i):
        print(' ',end="")
    for z in range(1,i * 2):
        if z == 1 or z == i * 2 - 1:
            print('*',end="")
        else:
            print(' ',end="")
    print()

# 下半部分
b_s = s // 2
for i in range(1,b_s + 1):
    for j in range(1 , i + 1):
        print(' ',end="")
# 倒三角形
    for k in range(1 , s - i * 2 + 1):
        if k == 1 or k == s - i * 2 + 1 - 1:
            print('*', end="")
        else:
            print(' ', end="")
    print()