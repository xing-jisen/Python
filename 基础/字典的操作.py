x = {'张三':333,'李四':666,'王五':999}
# print(x['邢继森'])   # 第一种
print(x.get('邢继森')) # 第二种
print(x.get('邢继森',123456))



# 删除元素
del x['张三']
print(x)

# # 清空元素
# x.clear()
# print(x)


# 新增元素
x['邢继森'] = 123456789
print(x)


# 修改元素
x['邢继森'] = 123321
print(x)

# 获取所有的键（key）
keys = x.keys()
print(keys)
print(type(keys))
print(list(keys))     # 键（key）组成的视图转换成列列表

# 获取所有值（value）
value = x.values()
print(value)
print(type(value))
print(list(value))   # 值（value）组成的视图转换成列列表

# 获取所有的键值对
items = x.items()
print(items)
print(type(items))
print(list(items))   # 转换之后得列表是由元组组成的


print('*'*99)
x = {'张三':333,'李四':666,'王五':999}
# 字典元素的遍历
for i in x:
    print(i)

for i in x:
    print(i,x[i])

print('*'*10)
for i in x:
    print(i,x.get(i))


print('*'*99)
# 字典的特点
# 字典里面都是一个键值对  值value可以重复，键key不允许重复
x = {'张三':333,'张三':999}
print(x)

x = {'张三':333,'李四':333}
print(x)

# 字典中的元素是无序的
# 字典中的键key必须是不可变对象，可变对象不允许
