# -*- codeing = utf-8 -*-
# @Time : 2022/1/15 21:09
# @Author : 邢继森
# @File : 模拟京东购物流程.py
# Software : PyCharm
if __name__ == "__main__":
    lst = []
    for i in range(5):
        lst1 = input('请输入商品编号和商品名称进行商品入库,每次只能输入一件商品')
        lst.append(lst1)

    print("商品如下")
    for i in lst:
        print(i)

    # 用于存储购物车的商品
    cart = list()
    while True:
        flag = False    # 代表没有商品的情况
        num = input("请输入要购买的商品编号输入q退出")
        for i in lst:
            if num == i[:4]:
                flag = True     # 代表商品已找到
                cart.append(i)
                print("商品已成功添加到购物车")
                break
        if flag == False and num != 'q':
            print("商品不存在")
        if num == 'q':
            break       # 推出while循环

    print("您购物车里已选择的商品为:")
    # 反向
    cart.reverse()
    for i in cart:
        print(i)