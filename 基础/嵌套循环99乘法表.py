# -*- codeing = utf-8 -*-
# @Time : 2022/1/14 13:31
# @Author : 邢继森
# @File : 嵌套循环99乘法表.py
# Software : PyCharm
if __name__ == "__main__":
    for i in range(1,10):
        for j in range(1,i+1):
            print("%d*%d=%d"%(j,i,i*j),"\t",end="")
        print()