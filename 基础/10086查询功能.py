# -*- codeing = utf-8 -*-
# @Time : 2022/1/14 13:14
# @Author : 邢继森
# @File : 10086查询功能.py
# Software : PyCharm
if __name__ == "__main__":
    a = 'y'
    while a == 'y':
        print('''
            ----------欢迎使用10086查询功能----------
            1.查询当前余额
            2.查询当前剩余流量
            3.退出系统
            ''')
        s = eval(input("请输入您要执行的操作:"))
        if s == 1:
            print("当前余额为234.6元")
        elif s == 2:
            print("当前流量为46G")
        elif s == 3:
            print("当前通话时长为3000分钟")
        elif s == 0:
            print("程序退出，感谢您的使用")
            break
        else:
            print("对不起，您输入有误，请重新输入")
        a = input("还要继续操作吗？y/n")
else:
    print("程序退出，感谢您的使用")
