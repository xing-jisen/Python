# -*- codeing = utf-8 -*-
# @Time : 2022/3/24 20:34
# @Author : 邢继森
# @File : 模块及调用.py
# Software : PyCharm

# 导入整个模块的内容
import random
# 生成0-100之间的整数
print(random.randint(0,100))
print(random.random())

# 导入模块某一内容 逗号可以导入多个
from random import randint,random
print(randint(0, 100))

# 数学模块
import math
# 开平方
print(math.sqrt(16))
print(math.sqrt(9))

#
import time
# 返回一个时间戳:计算某一年到现在的毫秒数
print(time.time())
time.sleep(2) # 程序休眠两秒
print("Hello")


# 简化模块名
from time import sleep as s
s(5) # 休眠五秒
print(12345)

