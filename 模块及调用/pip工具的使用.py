# -*- codeing = utf-8 -*-
# @Time : 2022/3/24 20:56
# @Author : 邢继森
# @File : pip工具的使用.py
# Software : PyCharm

# 第三方模块
# pip可以对包进行:安装，卸载，查看的功能

# pip install module_name     安装模块
# pip uninstall module_name   卸载模块
# pip list                    查看安装的模块
# 下载在默认是国外网站，可以换一个数据源

# python绘图工具
from matplotlib import pyplot as plt
plt.plot([1,2,6,9,5,3])
plt.show()