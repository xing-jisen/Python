# -*- codeing = utf-8 -*-
# @Time : 2022/3/9 8:50
# @Author : 邢继森
# @File : python创建数据表.py
# Software : PyCharm
import pymysql

host = '127.0.0.1'  # 主机名
user = 'root'  # 数据库用户名
password = '123456'  # 数据库密码
database = 'notebook'  # 数据库名称
db = pymysql.connect(host=host, user=user, password=password, db=database)  # 建立连接
# 打开数据库连接

# 这4个参数依次是：主机名，用户名，密码和数据库名
# 使用 cursor() 方法创建一个游标对象 cursor
cursor = db.cursor()

# 使用 execute() 方法执行 SQL，如果表存在则删除
cursor.execute("DROP TABLE IF EXISTS users")

# 使用预处理语句创建表
sql = """CREATE TABLE `users` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"""

cursor.execute(sql)

# 关闭数据库连接
db.close()
