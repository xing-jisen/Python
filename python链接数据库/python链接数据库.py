# -*- codeing = utf-8 -*-
# @Time : 2022/3/9 8:44
# @Author : 邢继森
# @File : python链接数据库.py
# Software : PyCharm
import pymysql

host = '127.0.0.1'  # 主机名
user = 'root'  # 数据库用户名
password = '123456'  # 数据库密码
database = 'notebook'  # 数据库名称
db = pymysql.connect(host=host, user=user, password=password, db=database)  # 建立连接
# 打开数据库连接

# 这4个参数依次是：主机名，用户名，密码和数据库名
# 使用 cursor() 方法创建一个游标对象 cursor
cursor = db.cursor()

# 使用 execute()  方法执行 SQL 查询
cursor.execute("SELECT VERSION()")

# 使用 fetchone() 方法获取单条数据.
data = cursor.fetchone()

print("Database version : %s " % data)

# 关闭数据库连接
db.close()