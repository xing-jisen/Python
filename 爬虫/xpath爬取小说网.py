# -*- codeing = utf-8 -*-
# @Time : 2022/1/19 15:57
# @Author : 邢继森
# @File : xpath爬取当当网.py
# Software : PyCharm
import requests
import parsel
if __name__ == "__main__":
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    # 爬取页面地址
    url = 'https://www.qb5.tw/book_36060/'
    page_text = requests.get(url=url,headers=headers)
    select = parsel.Selector(page_text.text)
    # 进行数据解析
    # 标题名
    tle = select.xpath('//div[@id="info"]/h1/text()').get()
    print(tle)
    tle1 = select.xpath('//div[@id="info"]/h1/small/a/text()').get()
    tle0 = tle + tle1 + '.txt'
    fp = open(tle0,'w',encoding='utf-8')
    li_div = select.xpath('//dl[@class="zjlist"]/dd')
    for div in li_div:
        # 获取标题
        title = div.xpath('./a/text()').get()
        # print(title)
        # 获取文章的href链接地址
        wenzhang = div.xpath('./a/@href').get()
        body_url = url + wenzhang
        # print(body_url)

        fp.write(title + body_url + '\n')
    print("完成!!!")