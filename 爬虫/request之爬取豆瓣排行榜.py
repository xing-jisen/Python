# -*- codeing = utf-8 -*-
# @Time : 2022/1/17 10:46
# @Author : 邢继森
# @File : request之爬取豆瓣排行榜.py
# Software : PyCharm
import requests
import json
if __name__ == "__main__":
    url = 'https://movie.douban.com/j/chart/top_list?'
    param = {
        'type': '24',
        'interval_id': '100:90',
        'action':'',
        'start': '1',  # 从豆瓣库中的第几部电影中取
        'limit': '20'   # 一次取出的个数
    }
    # 2.UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    # 3.获取相应对象
    response = requests.get(url=url,params=param,headers=headers)
    # 4.获取相应数据
    list_data = response.json()
    # 5.持久化存储
    fp = open('./douban.json','w',encoding='utf-8')
    json.dump(list_data,fp=fp,ensure_ascii=False)
    print('Over')