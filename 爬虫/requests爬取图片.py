# -*- codeing = utf-8 -*-
# @Time : 2022/1/18 10:50
# @Author : 邢继森
# @File : requests爬取图片.py
# Software : PyCharm
import requests
import re
if __name__ == "__main__":
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    # 如何爬取图片数据
    url = 'https://www.ivsky.com/bizhi/green_snake_v70220/'
    # .content返回的是二进制形式的图片数据
    # 返回的数据类型:  text(字符串) content(二进制)   json(对象)

    # 使用通用爬虫对一整张网页发起请求
    page_text = requests.get(url=url,headers=headers).text
    # 使用聚焦爬虫对url对应的一整张页面进行解析/提取
    ex = '<li>.*?<img src="(.*?) alt.*?</li>'
    img_src_list = re.findall(ex,page_text,re.S)
    print(img_src_list)
