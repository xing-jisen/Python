# -*- codeing = utf-8 -*-
# @Time : 2021/11/26 9:46
# @Author : 邢继森
# @File : 十二周团课2.py
# Software : PyCharm
if __name__ == "__main__":
    import requests    # 发送请求
    import parsel      # 解析模块，提取数据
    count = 1
    for i in range(1,20):
        url_img = f"https://www.ivsky.com/tupian/chengshilvyou/index_{i}.html"
        response = requests.get(url=url_img)   # 返回对象
        response.encoding = "utf-8"            # 编码问题
        sel = parsel.Selector(response.text)   # 实体化selector xpath
        lis = sel.xpath("//ul[@class='ali']/li")    # 通过标签节点来提取数据的一个工具
        path = "C:\\Users\\邢继森\\Desktop\\img\\{}.jpg"
        for li in lis:
            img_href = li.xpath("./rdiv/a/img/@sc").get()   # ./代表当前目录
            img_url = 'https:' + img_href
            img_response = requests.get(img_url)   # 请求的图片返回来的数据
            file = open(file=path.format(count),mode="wb") # wb 固定的二进制写入模式
            file.write(img_response.content)   # mp3  mp4  img  jpg  jpeg
            print('OK!')
            file.close()
            count = count + 1