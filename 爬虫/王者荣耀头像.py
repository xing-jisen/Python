# -*- codeing = utf-8 -*-
# @Time : 2021/12/2 20:43
# @Author : 邢继森
# @File : 王者荣耀头像.py
# Software : PyCharm
import requests
import parsel
import json
import os
import time
if __name__ == "__main__":
    path = "C:\\Users\\邢继森\\Desktop\\爬取王者头像\\{}.jpg"
    url_img = f"http://pvp.qq.com/web201605/herolist.shtml"
    response = requests.get(url=url_img)
    response.encoding = "gbk"
    sel = parsel.Selector(response.text)
    lis = sel.xpath("//ul[@class='herolist clearfix']/li")
    count = 1
    for li in lis:
        href_img = li.xpath("./a/img/@src").get()
        real = "http:" + href_img
        imgfile = requests.get(url=real)
        print(imgfile.status_code)
        file = open(file = path.format(count),mode = "wb")
        file.write(imgfile.content)
        print("ok")
        file.close()
        count  += 1