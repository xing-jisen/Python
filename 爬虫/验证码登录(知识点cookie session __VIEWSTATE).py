# -*- codeing = utf-8 -*-
# @Time : 2022/1/21 14:05
# @Author : 邢继森
# @File : 验证码登录古诗文网(__VIEWSTATE未解决).py
# Software : PyCharm
# 1.验证码识别，获取验证码数据
# 2.对post请求进行发送，（处理请求参数）
# 3.对响应数据进行持久化存储
import parsel
import requests
import re
if __name__ == "__main__":
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    url = 'https://so.gushiwen.cn/user/login.aspx?from=http://so.gushiwen.cn/user/collect.aspx'
    page_text = requests.get(url=url,headers=headers)
    tree = select = parsel.Selector(page_text.text)
    code_img_src = 'https://so.gushiwen.cn' + tree.xpath('//*[@id="imgCode"]/@src').get()
    # print(code_img_src)
    code_img_data = requests.get(url=code_img_src,headers=headers).content
    with open('./code.jpg','wb') as fp:
        fp.write(code_img_data)
    # print(code_img_data)
    # 创建一个session对象
    session = requests.Session()

    # 使用打码工具进行识别（人眼识别吧）
    yanzhengma = input('验证码:')
    # post进行模拟登陆
    login_url = 'https://so.gushiwen.cn/user/login.aspx?from=http%3a%2f%2fso.gushiwen.cn%2fuser%2fcollect.aspx'

    # 获取表单VIEWSTATE的值
    __VIEWSTATE = tree.xpath('//*[@id="__VIEWSTATE"]/@value').get()
    print(__VIEWSTATE)
    data = {
        '__VIEWSTATE':__VIEWSTATE,
        '__VIEWSTATEGENERATOR': 'C93BE1AE',
        'from': 'http://so.gushiwen.cn/user/collect.aspx',
        'email': '15269095903',
        'pwd': 'Xjs0912+.',
        'code': yanzhengma,
        'denglu': '登录'
    }
    # 使用session进行post请求的发送
    response = session.post(url = login_url,headers=headers,data=data)
    login_page_text = response.text
    print(response.status_code)
    with open('./renren.html','w',encoding='utf-8') as fp:
        fp.write(login_page_text)


    # 爬取当前页面的数据对应个人主页的页面数据
    detail_url = 'https://www.gushiwen.cn/'
    # 手动cookie处理(不建议使用，通用性不强)
    '''
    headers = {
        'cookie':'login=flase; ASP.NET_SessionId=bj4aeq5wnuyerhkky1dvkdr2; codeyzgswso=426a3f81567554e6; gsw2017user=2419317%7c5BD217892E7610CF610F878852EB80C8; login=flase; wxopenid=defoaltid; gswZhanghao=15269095903; gswPhone=15269095903'
    }'''
    # 使用携带了cookie的session进行get()请求
    detail_data_test = session.get(url=detail_url,headers=headers).text
    with open('bobo.html','w',encoding='utf-8') as fp:
        fp.write(detail_data_test)