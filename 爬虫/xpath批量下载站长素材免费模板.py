# -*- codeing = utf-8 -*-
# @Time : 2022/1/20 16:02
# @Author : 邢继森
# @File : xpath批量下载站长素材免费模板.py
# Software : PyCharm
import requests
import parsel
import os
if __name__ == "__main__":
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    # 创建文件夹
    # 如果文件夹不存在
    if not os.path.exists('C:/Users/邢继森/Desktop/站长素材PPT'):
        # 则创建一个文件夹
        os.mkdir('C:/Users/邢继森/Desktop/站长素材PPT')

    # 循环页码
    pagination = 100
    for i in range(1,pagination):
        i = '_' + str(i)
        print('正在爬取第' + i[-1] + '页的PPT')
        if i == '_1':
            i = i[-1]
        # 爬取页面地址
        url = 'https://sc.chinaz.com/jianli/free' + i + '.html'
        page_text = requests.get(url=url,headers=headers).text
        select = parsel.Selector(page_text)
        # 数据解析获得模板的页面链接
        a_url = select.xpath('//div[@id="container"]/div/a')
        for a in a_url:
            href_url = 'https:' + a.xpath('./@href').get()
            # print(href_url)
            # 获取子页面PPT模板地址
            href_text = requests.get(url=href_url,headers=headers)
            select_url = parsel.Selector(href_text.text)
            # 获取子页面PPT模板的下载地址
            a_xiazai = select_url.xpath('//*[@id="down"]/div[2]/ul/li[1]/a/@href').get()
            print(a_xiazai)

            # 下载
            href_xiazai = requests.get(url=a_xiazai, headers=headers).content
            ppt = 'C:/Users/邢继森/Desktop/站长素材PPT/' + a_xiazai
            # 'C:/Users/邢继森 /Desktop/站长素材PPT/https://downsc.chinaz.net/Files/DownLoad/jianli/202201/    jianli16815.rar'
            fileNameList = ppt.split("/")   # 字符串分割
            fileName = fileNameList[-1]     # 取最后一个为名字
            fileName_merge = 'C:/Users/邢继森/Desktop/站长素材PPT/' + fileName # 字符串合并
            with open(fileName_merge, 'wb') as fp:
                fp.write(href_xiazai)
                print(fileName + '下载成功!!!')
