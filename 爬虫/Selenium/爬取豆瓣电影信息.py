# -*- codeing = utf-8 -*-
# @Time : 2022/4/2 23:50
# @Author : 邢继森
# @File : 爬取豆瓣电影信息.py
# Software : PyCharm

from selenium import webdriver
import time
driver = webdriver.Chrome("C:\Program Files\Google\Chrome\Application\chromedriver.exe")

for i in range(11):
    x = i * 25
    url = f"https://movie.douban.com/top250?start={x}&filter="
    print(url)

    driver.get(url) # 打开页面
    time.sleep(2)

    for j in range(1,26):
        try:
            # xpath 获取页面电影标题元素
            search1 = driver.find_element_by_xpath(
                f"//*[@id='content']/div/div[1]/ol/li[{j}]/div/div[2]/div[1]/a/span[1]")
            search2 = driver.find_element_by_xpath(
                f"//*[@id='content']/div/div[1]/ol/li[{j}]/div/div[2]/div[1]/a/span[2]")
            search3 = driver.find_element_by_xpath(f"//*[@id='content']/div/div[1]/ol/li[{j}]/div/div[2]/div[1]/a/span[3]")
            print("电影名:   " + search1.text, end=" ")
            print(search2.text, end=" ")
            print(search3.text)
        except:
            # xpath 获取页面电影标题元素
            search1 = driver.find_element_by_xpath(
                f"//*[@id='content']/div/div[1]/ol/li[{j}]/div/div[2]/div[1]/a/span[1]")
            search2 = driver.find_element_by_xpath(
                f"//*[@id='content']/div/div[1]/ol/li[{j}]/div/div[2]/div[1]/a/span[2]")
            # search3 = driver.find_element_by_xpath(f"//*[@id='content']/div/div[1]/ol/li[{j}]/div/div[2]/div[1]/a/span[3]")
            print("电影名:   " + search1.text, end=" ")
            print(search2.text)
            # print(search3.text)


        # xpath获取页面评分元素
        score = driver.find_element_by_xpath(f"//*[@id='content']/div/div[1]/ol/li[{j}]/div/div[2]/div[2]/div/span[2]")
        print("评分:  " + score.text)


        try:
            # xpath获取页面一句话元素
            motto = driver.find_element_by_xpath(
                f"//*[@id='content']/div/div[1]/ol/li[{j}]/div/div[2]/div[2]/p[2]/span")
            print("一句话元素:  " + motto.text)
        except:
            print("没有一句话元素")

        # xpath获取页面地址元素
        site = driver.find_element_by_xpath(f"//*[@id='content']/div/div[1]/ol/li[{j}]/div/div[2]/div[1]/a").get_attribute('href')
        print("地址:  " + site)

        # xpath获取页面评价人数元素
        number  = driver.find_element_by_xpath(f"//*[@id='content']/div/div[1]/ol/li[{j}]/div/div[2]/div[2]/div/span[4]")
        print("评价人数:  " + number.text)

        # xpath获取页面介绍元素
        introduce  = driver.find_element_by_xpath(f"//*[@id='content']/div/div[1]/ol/li[{j}]/div/div[2]/div[2]/p[1]")
        print("介绍:  " + introduce.text)

        i  = driver.find_element_by_xpath(f"//*[@id='content']/div/div[1]/ol/li[{j}]/div/div[1]/em")
        print("排名:  " + i.text)

        print('*' * 20)

        print('\n')

        time.sleep(2)
