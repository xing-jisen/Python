# -*- codeing = utf-8 -*-
# @Time : 2022/4/2 23:01
# @Author : 邢继森
# @File : 爬取logo图片.py
# Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

browser = webdriver.Chrome("C:\Program Files\Google\Chrome\Application\chromedriver.exe")

browser.get(r'https://www.baidu.com')
time.sleep(2)

logo = browser.find_element_by_class_name('index-logo-src')
print(logo)
print(logo.get_attribute('src'))

time.sleep(2)

# 关闭浏览器
#browser.close()