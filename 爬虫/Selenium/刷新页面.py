# -*- codeing = utf-8 -*-
# @Time : 2022/3/26 18:11
# @Author : 邢继森
# @File : 刷新页面.py
# Software : PyCharm
from selenium import webdriver
import time

# 初始化浏览器为chrome浏览器
browser = webdriver.Chrome("C:\Program Files\Google\Chrome\Application\chromedriver.exe")

# 设置浏览器全屏
browser.maximize_window()
browser.get(r'https://www.baidu.com')
time.sleep(2)

for i in range(0, 5):
    try:
        # 刷新页面
        browser.refresh()
        print('刷新页面')
        time.sleep(2)
    except Exception as e:
        print('刷新失败')
        time.sleep(2)

# 关闭浏览器
browser.close()
