# -*- codeing = utf-8 -*-
# @Time : 2022/3/26 17:16
# @Author : 邢继森
# @File : 初始化浏览器对象.py
# Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

browser = webdriver.Chrome("C:\Program Files\Google\Chrome\Application\chromedriver.exe")  #打开Chrome浏览器
browser.get(r'https://www.baidu.com')
search = browser.find_element_by_id('kw')   # 找到百度的搜索框
search.send_keys('python')                  # 搜索框输入内容
search.send_keys(Keys.ENTER)                # 相当于回车键

# 关闭浏览器
# browser.close()