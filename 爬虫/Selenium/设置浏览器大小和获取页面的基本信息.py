# -*- codeing = utf-8 -*-
# @Time : 2022/3/29 19:37
# @Author : 邢继森
# @File : 设置浏览器大小和获取页面的基本信息.py
# Software : PyCharm
from selenium import webdriver
import time

# 初始化浏览器
browser = webdriver.Chrome('C:\Program Files\Google\Chrome\Application\chromedriver.exe')

# 设置大小:全屏
browser.maximize_window()
browser.get('https://www.sogou.com')
time.sleep(5)


# 设置大小:800 * 800
browser.set_window_size(800,800)
time.sleep(3)


# 设置全屏
browser.maximize_window()
time.sleep(3)


# 网页的标题
print('网页的标题:' + browser.title)
# 当前的网址
print('当前的网址:' + browser.current_url)
# 浏览器名称
print('浏览器名称:' + browser.name)
# 网页源码
print('网页源码:' + browser.page_source)


# 关闭浏览器
browser.close()