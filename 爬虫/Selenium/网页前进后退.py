# -*- codeing = utf-8 -*-
# @Time : 2022/3/26 18:13
# @Author : 邢继森
# @File : 网页前进后退.py
# Software : PyCharm
from selenium import webdriver
import time

# 初始化浏览器为chrome浏览器
browser = webdriver.Chrome("C:\Program Files\Google\Chrome\Application\chromedriver.exe")

# 设置浏览器全屏
browser.maximize_window()
browser.get(r'https://www.baidu.com')
time.sleep(2)

# 打开淘宝页面
browser.get(r'https://www.taobao.com')
time.sleep(2)

# 后退到百度页面
browser.back()
time.sleep(2)

# 前进的淘宝页面
browser.forward()
time.sleep(2)

# 关闭浏览器
browser.close()