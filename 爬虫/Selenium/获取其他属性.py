# -*- codeing = utf-8 -*-
# @Time : 2022/4/2 23:47
# @Author : 邢继森
# @File : 获取其他属性.py
# Software : PyCharm
# 除了属性和文本值外，还有id、位置、标签名和大小等属性。

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

browser = webdriver.Chrome("C:\Program Files\Google\Chrome\Application\chromedriver.exe")


browser.get(r'https://www.baidu.com')
time.sleep(2)

logo = browser.find_element_by_class_name('index-logo-src')
print(logo.id)
print(logo.location)
print(logo.tag_name)
print(logo.size)

# 关闭浏览器
#browser.close()