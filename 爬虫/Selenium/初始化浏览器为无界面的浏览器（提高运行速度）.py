# -*- codeing = utf-8 -*-
# @Time : 2022/3/26 18:00
# @Author : 邢继森
# @File : 初始化浏览器为无界面的浏览器（提高运行速度）.py
# Software : PyCharm
from selenium import webdriver

browser = webdriver.Chrome("C:\Program Files\Google\Chrome\Application\chromedriver.exe")

# 无界面的浏览器
option = webdriver.ChromeOptions()
path = option.add_argument("headless")
browser = webdriver.Chrome(options=option)

# 访问百度首页
browser.get(r'https://www.baidu.com/')
# 截图预览
browser.get_screenshot_as_file('截图.png')

# 关闭浏览器
browser.close()