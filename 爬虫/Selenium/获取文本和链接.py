# -*- codeing = utf-8 -*-
# @Time : 2022/4/2 23:04
# @Author : 邢继森
# @File : 获取文本和链接.py
# Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

browser = webdriver.Chrome("C:\Program Files\Google\Chrome\Application\chromedriver.exe")


browser.get(r'https://www.baidu.com')
time.sleep(2)

logo = browser.find_element_by_xpath('//*[@id="hotsearch-content-wrapper"]/li[1]/a')
#logo = browser.find_element_by_css_selector('#hotsearch-content-wrapper > li:nth-child(1) > a')

print(logo.text)
print(logo.get_attribute('href'))       # 获取href属性

time.sleep(2)

# 关闭浏览器
browser.close()