# -*- codeing = utf-8 -*-
# @Time : 2022/3/26 17:10
# @Author : 邢继森
# @File : 自动安装驱动.py
# Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager

browser = webdriver.Chrome(ChromeDriverManager().install())

browser.get('http://www.baidu.com')
search = browser.find_element_by_id('kw')
search.send_keys('python')
search.send_keys(Keys.ENTER)

# 关闭浏览器
# browser.close()