# -*- codeing = utf-8 -*-
# @Time : 2022/4/2 22:53
# @Author : 邢继森
# @File : css定位.py
# Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

path = r'C:\Program Files\Google\Chrome\Application\chromedriver.exe'
browser = webdriver.Chrome(path)

browser.get(r'https://www.baidu.com')
time.sleep(2)

# 在搜索框输入 python
search = browser.find_element_by_css_selector('#kw')
search.send_keys('python')
search.send_keys(Keys.ENTER)

time.sleep(2)

# 关闭浏览器
#browser.close()
