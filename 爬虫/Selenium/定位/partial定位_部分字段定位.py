# -*- codeing = utf-8 -*-
# @Time : 2022/4/2 22:51
# @Author : 邢继森
# @File : partial定位_部分字段定位.py
# Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

path = r'C:\Program Files\Google\Chrome\Application\chromedriver.exe'
browser = webdriver.Chrome(path)

browser.get(r'https://www.baidu.com')
time.sleep(2)

# 点击新闻 链接
browser.find_element_by_partial_link_text('闻').click()
time.sleep(2)

# 关闭浏览器全部页面
browser.quit()
# 关闭浏览器
#browser.close()