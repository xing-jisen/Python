# -*- codeing = utf-8 -*-
# @Time : 2022/4/2 22:56
# @Author : 邢继森
# @File : 通用的方法find_element().py
# Software : PyCharm
# 使用前先导入By类
from selenium.webdriver.common.by import By
# 这个方法有两个参数：定位方式和定位值。
'''
    以上的操作可以等同于以下：
    browser.find_element(By.ID,'kw')
    browser.find_element(By.NAME,'wd')
    browser.find_element(By.CLASS_NAME,'s_ipt')
    browser.find_element(By.TAG_NAME,'input')
    browser.find_element(By.LINK_TEXT,'新闻')
    browser.find_element(By.PARTIAL_LINK_TEXT,'闻')
    browser.find_element(By.XPATH,'//*[@id="kw"]')
    browser.find_element(By.CSS_SELECTOR,'#kw')
'''