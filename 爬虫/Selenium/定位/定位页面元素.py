# -*- codeing = utf-8 -*-
# @Time : 2022/4/2 21:57
# @Author : 邢继森
# @File : 定位页面元素.py
# Software : PyCharm
from selenium import webdriver
browser = webdriver.Chrome("C:\Program Files\Google\Chrome\Application\chromedriver.exe")
browser.get(r'https://www.baidu.com')

# 获取基础属性
# 获取网页标题
print(browser.title)
# 获取当前网址
print(browser.current_url)
# 获取浏览器名称
print(browser.name)
# 获取网站源码
print(browser.page_source)

# 定位页面元素
from selenium.webdriver.common.keys import Keys     # 搜索模块
import time
time.sleep(2)

# 在搜索框输入 python   # id定位
search = browser.find_element_by_id('kw')   # 搜索方式
search.send_keys('python')      # 搜索内容
time.sleep(1)
search.send_keys(Keys.ENTER)    # 回车键
time.sleep(2)

# 关闭浏览器
browser.close()

