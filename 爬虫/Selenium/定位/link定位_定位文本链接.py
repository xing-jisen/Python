# -*- codeing = utf-8 -*-
# @Time : 2022/4/2 22:49
# @Author : 邢继森
# @File : link定位_定位文本链接.py
# Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

browser = webdriver.Chrome("C:\Program Files\Google\Chrome\Application\chromedriver.exe")


browser.get(r'https://www.baidu.com')
time.sleep(2)

s = browser.find_element_by_link_text('网盘')# 找到
s.click()  # 自动点击
time.sleep(2)

# 关闭浏览器
#browser.close()
# 关闭浏览器全部页面
browser.quit()