# -*- codeing = utf-8 -*-
# @Time : 2022/1/18 13:21
# @Author : 邢继森
# @File : bs4将本地html网页加载出来.py
# Software : PyCharm
from bs4 import BeautifulSoup
if __name__ == "__main__":
    # 想要将本地的html文档中的数据加载到该对象中
    fp = open('F:\HTML5+CSS3\邢继森\超链接与伪类\锚点.html','r',encoding='utf-8')
    soup = BeautifulSoup(fp,'lxml')
    print(soup)