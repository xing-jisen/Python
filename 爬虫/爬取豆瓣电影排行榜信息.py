import requests
import json

if __name__ == "__main__":
    Request_URL = 'https://movie.douban.com/j/chart/top_list'
    # 把网址后面的信息封装到列表里面
    param = {
        'type':'24',
        'interval_id':'100:90',
        'action':'',
        'start':'0',    # 从豆瓣第几部电影开始取开始位置
        'limit':'20',   # 一次请求的个数
    }
    # UA 伪装
    headers = {
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }

    '''
    url=Request_URL爬取的网址;
    params=param 某个请求携带的参数，如get请求;
    headers=headers UA伪装;
    '''

    # 获取响应对象
    response = requests.get(url=Request_URL,params=param,headers=headers)
    # 获取相应数据
    list_data = response.json()    # .json数据的格式
    # 持久化存储
    fp = open('./douban.json','a+',encoding='utf-8')
    json.dump(list_data,fp=fp,ensure_ascii=False)  # dump() 写入模式
    print("OVER!!!")
