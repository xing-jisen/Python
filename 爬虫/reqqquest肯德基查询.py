# -*- codeing = utf-8 -*-
# @Time : 2022/1/17 11:20
# @Author : 邢继森
# @File : reqqquest肯德基查询.py
# Software : PyCharm
import requests

if __name__ == "__main__":
    url = 'http://www.kfc.com.cn/kfccda/ashx/GetStoreList.ashx?'
    site = input('肯德基地点查询:')
    param = {
        'cname':'',
        'pid':'',
        'op' : 'keyword',
        'keyword': site,
        'pageIndex': '1',
        'pageSize': '10'
    }
    # 2.UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    # 3.获取相应对象
    reaponse = requests.get(url=url,params=param,headers=headers)
    # 4.获取相应数据
    data_text = reaponse.text
    # 5.打印输出
    site_word = site + ':肯德基地点' + '.json'
    with open(site_word,'w',encoding='utf-8') as fp:
        fp.write(data_text)
    print(site_word + '打印完成')