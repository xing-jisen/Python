import requests
import json

if __name__ == "__main__":
    # 1.指定url
    post_url = 'https://fanyi.baidu.com/sug'
    # 2.UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    # 3.post请求参数处理 （同get请求一致）
    word = input('输入需要翻译的词语:')
    data = {
        'kw':word
    }
    # 4.请求发送
    response = requests.post(url = post_url,data = data,headers=headers)
    # 5.获取相应数据:json()返回的是obj（对象）如果确认相应数据是json类型的才可以使用json（）
    # 在抓包头响应信息查看是否为json类型
    dic_obj = response.json()

    print(dic_obj)
    # 6.持久化存储
    if_Name = word+'.json'
    fp = open(if_Name,'w',encoding='utf-8')
    json.dump(dic_obj,fp=fp,ensure_ascii=False)
    print('over!!!')