# -*- codeing = utf-8 -*-
# @Time : 2022/1/17 16:45
# @Author : 邢继森
# @File : request爬取菜鸟教程搜索结果.py
# Software : PyCharm
import requests
if __name__ == "__main__":
    url = 'https://www.runoob.com/'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    name = input()
    params = {
        's': name
    }
    data_text = requests.get(url=url,headers=headers,params=params).text
    data_one = name + '.html'
    print(data_one)
    with open(data_one,'w',encoding='utf-8') as fp:
        fp.write(data_text)