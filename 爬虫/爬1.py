# -*- codeing = utf-8 -*-
# @Time : 2021/11/30 17:11
# @Author : 邢继森
# @File : 爬1.py
# Software : PyCharm
import requests
if __name__ == "__main__":
    # 1.指定URL
    url = 'https://www.sogou.com/'
    # 2.发起请求
    # get方法会返回一个响应对象
    response = requests.get(url=url) # 发起url发起get请求
    # 3.获取相应数据
    get_text = response.text
    print(get_text)
    # 4.持久化存储
    with open('./搜狗首页.html','w',encoding='utf-8',) as fp:
        fp.write(get_text)
    print('结束！！！')