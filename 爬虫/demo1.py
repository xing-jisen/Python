#-*- coding:utf-8 -*-
import requests
import parsel
import csv
# url = 'https://www.qb5.tw/fenlei/1_1/'
# response = requests.get(url=url)
# response.encoding = 'gbk'
# # print(response.text)
# sel = parsel.Selector(response.text)
# lis = sel.xpath('//ul[@class="titlelist"]/li')
# for i in lis:
#     name = i.xpath('./div[@class="zp"]/a/text()').get()
#     zj = i.xpath('./div[@class="zz"]/a/text()').get()
#     author = i.xpath('./div[@class="author"]/text()').get()
#     update_time = i.xpath('./div[@class="sj"]/text()').get()
#     info = f'{name} | 最新章节-{zj} | 作者是:{author} | 更新时间:{update_time}'
#     print(info)
class Spider_text(object):
    def __init__(self):
        self.write_in_file = open('xiaoshuo.csv', mode='a+', encoding='gbk', newline='')
        self.csvwrite = csv.writer(self.write_in_file)
        self.csvwrite.writerow(['书名', '更新章节', '作者', '更新时间'])
        self.index = 1

    def get_data(self):
        for i in range(1, 377):
            url = f'https://www.qb5.tw/fenlei/1_{i}/'
            response = requests.get(url=url)
            response.encoding = 'gbk'
            # print(response.text)
            sel = parsel.Selector(response.text)
            lis = sel.xpath('//ul[@class="titlelist"]/li')
            for i in lis:
                name = i.xpath('./div[@class="zp"]/a/text()').get()
                zj = i.xpath('./div[@class="zz"]/a/text()').get()
                author = i.xpath('./div[@class="author"]/text()').get()
                update_time = i.xpath('./div[@class="sj"]/text()').get()
                list_data = [name, zj, author, update_time]
                self.csvwrite.writerow(list_data)
                print(f'第{self.index}条写入完毕')
                self.index += 1

    def __del__(self):
        print('构析方法执行，文件关闭')
        self.write_in_file.close()

if __name__ == '__main__':
    test = Spider_text()
    test.get_data()












