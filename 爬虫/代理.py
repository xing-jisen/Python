# -*- codeing = utf-8 -*-
# @Time : 2022/1/22 10:12
# @Author : 邢继森
# @File : 代理.py
# Software : PyCharm
import requests
if __name__ == "__main__":
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    url = 'https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&tn=88093251_74_hao_pg&wd=ip'
    page_text = requests.get(url=url,headers=headers,proxies={"http":'47.117.112.103'}).text
    with open('ipdaili.html','w',encoding='utf-') as fp:
        fp.write(page_text)