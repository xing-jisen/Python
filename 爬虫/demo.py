#-*- coding:utf-8 -*-
import requests
import csv
import parsel
class Spider_text(object):
    #准备工作
    def __init__(self):
        self.base_url = 'https://www.qb5.tw/fenlei/1_{}/'
        #打开一个csv文件
        self.write_in_file = open(file='xiaoshuo.csv', mode='a+', encoding='gbk', newline='')#避免空行
        #csv写入哪个文件
        self.csvwirte = csv.writer(self.write_in_file)
        self.csvwirte.writerow(['书名', '更新章节', '作者', '更新时间', '链接'])
        self.count = 1 #计数器，实时反馈我们已经爬取了多少条了

    #获取页面信息并且解析页面，提取内容
    def get_data(self, page):  #获取单个页面信息,page参数for循环的时候动态加入
        try:
            url = self.base_url.format(page) #拼接字符串
            response = requests.get(url=url) #核心代码，发送请求
            response.encoding = 'gbk'  #具体分析
            sel = parsel.Selector(response.text) #parsel.Selector是集成xpath解析器的包，现在实例化,init参数就是我们的响应信息text格式
            #lis就是所有的li标签
            lis = sel.xpath('//ul[@class="titlelist"]/li') #此时lis 是一个Selector类的对象
            for li in lis:  #遍历所有的li标签，逐个处理
                name = li.xpath('./div[@class="zp"]/a/text()').get()   #li仍然是一个Selector对象
                zj = li.xpath('./div[@class="zz"]/a/text()').get()
                author = li.xpath('./div[@class="author"]/text()').get()
                update_time = li.xpath('./div[@class="sj"]/text()').get()
                href = li.xpath('./div[@class="zp"]/a/@href').get()
                list_data = [name, zj, author, update_time, href] #csv写入的时候，csv接受的是序列内容
                self.csvwirte.writerow(list_data)
                print(f"正在写入第{self.count}条数据: 书名->{name} 章节->{zj} 作者->{author} 更新时间->{update_time} 链接->{href}")
                self.count += 1
        except Exception as ve:
            print(ve)
            pass

    def __del__(self):
        print('__del__文件关闭')
        self.write_in_file.close()


if __name__ == '__main__':
    test = Spider_text()
    for page in range(1, 51):#翻页功能
        test.get_data(page)