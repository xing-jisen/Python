# -*- codeing = utf-8 -*-
# @Time : 2022/1/20 15:26
# @Author : 邢继森
# @File : xpath全国城市名称爬取(知识点：两个标签一起解析).py
# Software : PyCharm
import requests
import parsel
if __name__ == "__main__":
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    # 爬取页面地址
    url = 'http://www.aqistudy.cn/historydata/'
    page_text = requests.get(url=url,headers=headers).text
    select = parsel.Selector(page_text)
    # 数据解析
    '''host_li_list = select.xpath('//div[@class="bottom"]/ul/li')
    all_city_names = []
    for li in host_li_list:
        # 解析到了热门城市的城市名称
        host_city_name = li.xpath('./a/text()').get()
        # print(host_city_name)
        all_city_names.append(host_city_name)
    #     解析的是全国城市的名称
    quanguochengshi = select.xpath('//div[@class="bottom"]/ul/div[2]/li')
    for li in quanguochengshi:
        quanguo_name = li.xpath('./a/text()').get()
        # print(quanguo_name)
        all_city_names.append(quanguo_name)

    # print(all_city_names,len(all_city_names))'''
    # 将解析到热门城市和全国城市所有a标签
    # 热门城市a标签://div[@class="bottom"]//ul/li/a
    # 全国城市a标签://div[@class="bottom"]//ul/div[2]/li/a
    a_list = select.xpath('//div[@class="bottom"]/ul/li/a | //div[@class="bottom"]//ul/div[2]/li/a')
    all_names = []
    for a in a_list:
        jieguo = a.xpath('./text()').get()
        # print(jieguo)
        all_names.append(jieguo)
    print(all_names,len(all_names))