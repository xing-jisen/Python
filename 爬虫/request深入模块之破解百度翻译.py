# -*- codeing = utf-8 -*-
# @Time : 2022/1/17 9:52
# @Author : 邢继森
# @File : request深入模块之破解百度翻译.py
# Software : PyCharm
import requests
import json
if __name__ == "__main__":
    # 1.指定URL
    post_url = 'https://fanyi.baidu.com/sug'
    # 2.请求之前进行UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    # 3.post请求参数处理，同get请求一致
    word = input()
    data = {
        'kw':word
    }
    #4.请求发送
    response = requests.post(url=post_url,data=data,headers=headers)

    # 5.获取相应数据:json方法返回的是obj：对象(如果确认相应对象数据是json类型的，才可以使用json())
    dic_obj = response.json()
    print(dic_obj)

    # 6.进行持久化存储
    fileName = word + '.json'
    fp = open(fileName,'w',encoding='utf-8')
    json.dump(dic_obj,fp=fp,ensure_ascii=False)
    print('Over!')