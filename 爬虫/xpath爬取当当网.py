# -*- codeing = utf-8 -*-
# @Time : 2022/1/19 15:57
# @Author : 邢继森
# @File : xpath爬取当当网.py
# Software : PyCharm
import requests
import parsel
if __name__ == "__main__":
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    # 爬取页面地址
    url = 'http://e.dangdang.com/morelist_page.html?columnType=all_pcfreebook&title=%E5%85%8D%E8%B4%B9%E4%B8%93%E5%8C%BA'
    page_text = requests.get(url=url,headers=headers)
    select = parsel.Selector(page_text.text)
    # 进行数据解析
    li_div = select.xpath('//div[@class="inner"]/div')
    print(li_div)
    for div in li_div:
        title = div.xpath('./div/a/div[2]/div[1]/text()').get()
        print(title)