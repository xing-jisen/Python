# -*- codeing = utf-8 -*-
# @Time : 2022/1/17 13:08
# @Author : 邢继森
# @File : request化妆品药监总局.py
# Software : PyCharm
import requests
import json
if __name__ == "__main__":
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    id_list = []  # 存储企业的id
    all_data_list = []  # 存储所有的企业详情数据
    # 批量获取不同企业的id值
    url = 'http://scxk.nmpa.gov.cn:81/xk/itownet/portalAction.do?method=getXkzsList'
    # 参数封装
    for page in range(1,50):        # 打印多少页
        page = str(page)
        data = {
            'on': 'true',
            'page': page,        # 第几页
            'pageSize': '15',   # 一页多少条
            'productName':'',
            'conditionType': '1',
            'applyname':''
        }
        # 获取相应对象
        json_ids = requests.post(url=url,headers=headers,data=data).json()
        for i in json_ids['list']:
            id_list.append(i['ID'])
        print('企业ID第' + page + '页:打印完成!')

    # 获取企业详情数据
    post_url = 'http://scxk.nmpa.gov.cn:81/xk/itownet/portalAction.do?method=getXkzsById'
    for id in id_list:
        data = {
            'id': id
        }
        detail_json = requests.post(url=post_url,headers=headers,data=data).json()
        print('打印完成,ID:' + id)
        all_data_list.append(detail_json)
        # 持久化存储
        fp = open('./allData.json','w',encoding='utf-8')
        json.dump(all_data_list,fp=fp,ensure_ascii=False)