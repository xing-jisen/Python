# -*- codeing = utf-8 -*-
# @Time : 2022/1/19 21:03
# @Author : 邢继森
# @File : xpath 解析下载图片(解决文字乱码问题).py
# Software : PyCharm
import requests
import parsel
import os
if __name__ == "__main__":
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    # 爬取页面地址
    url = 'https://pic.netbian.com/4kmeinv/'# 更改此链接就可实现下载其它页面图片
    page_text = requests.get(url=url, headers=headers)
    select = parsel.Selector(page_text.text)
    # 可以手动设定相应数据的编码格式
    # page_text.encoding = 'utf-8' # 编码改成utf-8
    # page_text.encoding = 'utf-8'
    # page_text.encoding = 'gbk'

    # 数据解析：src和alt的地址
    li_list = select.xpath('//div[@class="slist"]/ul/li')


    # 创建一个文件夹
    # 如果文件夹不存在
    if not os.path.exists('C:/Users/邢继森/Desktop/爬取图片'):
        # 则创建一个文件夹
        os.mkdir('C:/Users/邢继森/Desktop/爬取图片')


    for li in li_list:
        # 拿到的是img的src
        img_src = 'https://pic.netbian.com' + li.xpath('./a/img/@src').get()
        img_name = li.xpath('./a/img/@alt').get() + '.jpg'
        print(img_src)
        # 解决乱码问题
        img_name = img_name.encode('iso-8859-1').decode('gbk')
        # print(img_name,img_src

        # 请求图片，进行持久化存储
        img_data = requests.get(url=img_src,headers=headers).content
        img_path = 'C:/Users/邢继森/Desktop/爬取图片/' + img_name
        with open(img_path,'wb') as fp:
            fp.write(img_data)
            print(img_name,'下载成功!!!')