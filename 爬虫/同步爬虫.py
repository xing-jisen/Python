# -*- codeing = utf-8 -*-
# @Time : 2022/1/22 10:57
# @Author : 邢继森
# @File : 同步爬虫.py
# Software : PyCharm
import requests
if __name__ == "__main__":
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    urls = [
        'https://downsc.chinaz.net/Files/DownLoad/jianli/202101/jianli14452.rar',
        'https://downsc.chinaz.net/Files/DownLoad/jianli/202101/jianli14453.rar',
        'https://downsc.chinaz.net/Files/DownLoad/jianli/202101/jianli14434.rar',
        'https://downsc.chinaz.net/Files/DownLoad/jianli/202101/jianli14435.rar'
    ]
    def get_content(url):
        print('正在爬取:',url)
        # get方法是一个阻塞的方法
        response = requests.get(url=url,headers=headers)
        if response.status_code == 200:    # status_code查看响应状态码
            return response.content    # 返回响应数据
    def parse_content(content):
        print('相应数据的长度为:',len(content))

    for url in urls:
        content = get_content(url)
        parse_content(content)