# -*- codeing = utf-8 -*-
# @Time : 2022/1/18 18:59
# @Author : 邢继森
# @File : xpath.py
# Software : PyCharm
import requests
import parsel
if __name__ == "__main__":
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    # 爬取到页面源码数据
    url = 'https://juancheng.58.com/ershoufang/?PGTID=0d100000-0160-30e8-80e6-dbae4db70d36&ClickID=3'
    page_text = requests.get(url=url,headers=headers)
    select = parsel.Selector(page_text.text)
    # 进行数据解析
    # 列表当中存储的就是div对象
    li_list = select.xpath('//section[@class="list"]/div')
    # 解析标题
    fp = open('./爬取房屋.txt', 'w', encoding='utf-8')
    for div in li_list:
        # 标题
        title = div.xpath('./a/div[2]/div/div/h3/text()').get()
        # print(title)
        # 金额
        p1_span1 = div.xpath('./a/div[2]/div[2]/p[1]/span[1]/text()').get()
        # print(p1_span1)
        # 元
        p2_span2 = div.xpath('./a/div[2]/div[2]/p[1]/span[2]/text()').get()
        # print(p2_span2)
        # 平方
        p2 = div.xpath('./a/div[2]/div[2]/p[2]/text()').get()
        # print(p2)
        # 姓名和评分房产
        s1 = div.xpath('./a/div[2]/div/div/div/span[1]/text()').get()
        # print(s1,end=" ")
        s2 = div.xpath('./a/div[2]/div/div/div/span[2]/text()').get()
        # print(s2,end=" ")
        s3 = div.xpath('./a/div[2]/div/div/div/span[3]/text()').get()
        # print(s3,end=" ")

        x = title + '\t' + p1_span1 + p2_span2 + '\t' + p2 + '\t' + s1 + '\t' + s2 + '\t' + s3
        fp.write(x + '\n\n')
        print('爬取完毕!!!')