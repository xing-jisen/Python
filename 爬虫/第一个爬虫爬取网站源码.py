#-*-codeing = utf-8 -*-
import requests         #导入模块
if __name__ == "__main__":
    # 指定url
    url = 'https://www.baidu.com/'
    # 发起请求
    # get请求返回一个对象
    response = requests.get(url=url)
    # 获取相应数据.text返回的是字符串形式的相应形式
    page_text = response.text
    print(page_text)
    # 持久化存储
    with open('./baidu.html','w',encoding='utf-8') as fp:
        fp.write(page_text)
    print('爬取数据结束！！！')