# -*- codeing = utf-8 -*-
# @Time : 2022/1/26 15:44
# @Author : 邢继森
# @File : 爬取58同城二手房信息（知识点：删除字符串前后空格）.py
# Software : PyCharm
import requests
import parsel
if __name__ == "__main__":
    url = 'https://juancheng.58.com/ershoufang/?PGTID=0d100000-0160-349b-208a-98eeb72913c3&ClickID=2'
    # UA伪装
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    page_text = requests.get(url=url,headers=headers)
    select = parsel.Selector(page_text.text)

    section_div = select.xpath('//section[@class="list"]/div')
    for div in section_div:
        title = div.xpath('./a/div[2]/div[1]/div[1]/h3/text()').get()
        qian = div.xpath('./a/div[2]/div[2]/p[1]/span[1]/text()').get()
        pingfang = div.xpath('./a/div[2]/div[2]/p[2]/text()').get()
        daxiao = div.xpath('./a/div[2]/div[1]/section/div[1]/p[2]/text()').get()
        daxiao = daxiao.lstrip()   # .l/strip()表示删除文字左侧空格    .rstrip()表示删除字符串末尾的空格
        guishu = div.xpath('./a/div[2]/div[1]/div[2]/div/span[3]/text()').get()

        zong = title + '\t' + qian + '万\t' +  pingfang + '\t' +  guishu + daxiao + '\n'
        # print(zong,end='')
        with open('./58二手房.txt','a',encoding='utf-8') as fp:
            fp.write(zong)
            print('成功')