#!/usr/bin/python
# -*- coding: utf-8 -*-
# 版权所有 : 
# 系统名称 : 
# 所属子系统 : 
# @Time    : 2022/01/01 00:00
# @Author  : 刘光旭
# @Email   : 2569844872@qq.com
# @File    : root.py
# @Software: PyCharm

# 爬虫别想得那么玄学
# 其实就是把人能看到的，而且有目标网址的页面数据内容快速的保存下来进行分析
# MP4
# MP3
# jpg 图片
# 百度本质上就是一个大型的爬虫引擎


# 读懂源码基本功就是类，面向对象基础
# 只要学会读源码， 以后不管遇到什么没用过的模块，只要点进去看一下它的内部方法构造，然后会用了

# JAVA MySql
import requests
import time
# 视频里边讲的是 tree， 但是现在淘汰了，先都用parsel
import parsel  # 内置了xpath解析器的一个模块
# csv其实就是Excel， 一样但不完全一样, 快捷， 常用
# 千万级别的数据， 用MySql数据库
# csv模块来操作Excel表格， csv包括任何模块本质上都是一个class类，或者说是一个文件夹，文件夹下面有很多py文件
# py文件里面有很多clss
# clss 有很多def function函数
import sys
import csv

# xpath re正则表达式  bs4  pyquery
# 分析翻页规则
# https://category.dangdang.com/pg1-cp01.05.16.00.00.00.html
# https://category.dangdang.com/pg2-cp01.05.16.00.00.00.html
# base 母版
base = 'https://category.dangdang.com/pg{}-cp01.05.16.00.00.00.html'
# 打开一个文件
# newline=''    避免空行
# 一般在csv写入都用utf-8
# 文件打开和response转码的时候都用utf-8
# 在Pycharm内部使用gbk，Pycharm比较智能
#做数据分析的时候用语言读取，写入 utf-8， 虽然Excel乱码，但是你再用py读取指定utf-8


f = open(file='当当网图书.csv', mode='a+', newline='', encoding='utf-8')
# 构造一个csv的实例化对象
csvwrite = csv.writer(f)
# 提醒一下自己，当前第几页
COUNT = 1
for page in range(1, 60):
    # try
    # except
    # 只执行try:后面缩进的代码
    # 如果报错，就会执行except Exception:后面缩进的代码，捕获异常
    try:
        # 构造新的url链接
        new_url = base.format(page)
        # 目前我们首先训练xpath静态页面的提取，先不管反爬的手段
        # 发送get请求, 拿到返回值数据
        response = requests.get(url=new_url)
        # 总结:如果utf-8不行, 那就gbk,记住一般就是这两个
        response.encoding = 'utf-8'
        # 数据提取
        # 返回值数据丢进来,构造一个实例化对象
        # 解析页面, 分析页面
        sel = parsel.Selector(response.text)
        # print(type(sel))
        # // 就是在任意位置开始查找
        # lis是一个列表
        # print(sel)
        lis = sel.xpath('//*[@id="component_59"]/li')
        for li in lis:
            # li到底是个什么?
            # <class 'parsel.selector.Selector'>
            # 只要是Selector对象就可以继续使用 xpath方法提取
            # ./当前目录/位置, ../上一级位置
            # 如果想拿到标签体中的属性内容，用@符
            # 如果想拿到被标签包裹起来的数据，用text()
            # 调用get方法，拿到纯数据，我们不要把那些乱七八糟的东西

            title = li.xpath('./a/@title').get()
            price = li.xpath('./p/span[@class="search_now_price"]/text()').get()
            author = li.xpath('./p[@class="search_book_author"]/span[1]/a/text()').get()
            count_comments = li.xpath('./p[@class="search_star_line"]/a/text()').get()
            publish = li.xpath('./p[@class="search_book_author"]/span[3]/a/text()').get()
            detail = li.xpath('./p[@class="detail"]/text()').get()
            # csv接收的是一个序列化的内容
            # 构造列表
            data_list = [title, price, author, count_comments, publish, detail]
            csvwrite.writerow(data_list)

    except Exception as e:
        print(e)
        pass  # 放行，回滚, rollback
    # 程序暂停3秒，单位/秒
    print(f'第{COUNT}页')
    COUNT += 1  # 计数器
