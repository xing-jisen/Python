# -*- codeing = utf-8 -*-
# @Time : 2022/1/11 10:10
# @Author : 邢继森
# @File : requests第一血.py
# Software : PyCharm
# requests模块是python中一款基于网络请求的模块，功能强大，简单便捷效率极高
# 作用:模拟浏览器发请求
# 步骤:
#  - 指定URL
#  - 发起请求
#  - 获取相应数据
#  - 持久化存储
#  环境安装: pip install requests

# 导包
import requests
if __name__=="__main__":
    # 1.指定URL
    url = 'https://www.sogou.com/'
    # 2.发起get请求
    # get方法返回的响应听对象赋值给response
    response = requests.get(url=url)
    # 3.获取相应数据
    # 返回的字符串数据赋值给page_text
    page_text = response.text
    print(page_text)
    # 4.持久化存储
    with open('./搜狗首页.html','w',encoding='utf-8') as fp:
        fp.write(page_text)
    print("爬取数据结束")
