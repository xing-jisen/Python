# -*- codeing = utf-8 -*-
# @Time : 2022/1/11 10:32
# @Author : 邢继森
# @File : requests实战之网页采集器.py
# Software : PyCharm
# UA伪装:
import requests
if __name__ == "__main__":
    # UA伪装
    headers = {
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    }
    # 1.指定URL
    url = 'https://www.baidu.com/s?'
    # 对URL携带的参数处理:对参数封装到字典中
    kw = input('输入需要搜索的数据')
    param = {
        'wd':kw
    }
    # 2.发起请求
    # params:参数 对指定的URL发起的请求是携带参数的，并且已经处理，参数拼接到URL后面
    response = requests.get(url=url,params=param,headers=headers)
    # 把字符编码类型改成utf-8
    response.encoding = 'utf-8'
    # 3.获取相应数据
    page_text = response.text
    # 4.持久化存储
    fileName = kw + '.html'
    with open(fileName,'w',encoding='utf-8') as fp:
        fp.write(page_text)
    print(fileName + "保存成功")
    print(response.apparent_encoding)# 查看网页返回的字符集类型
    print(response.encoding) # 自动判断的字符集类型
