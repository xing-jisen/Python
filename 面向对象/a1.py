# -*- codeing = utf-8 -*-
# @Time : 2022/3/10 19:16
# @Author : 邢继森
# @File : a1.py
# Software : PyCharm
class Persen:
    pass

    info = "这是一个" # 通过类拿到一个属性
    name = "人"
    # python类

    def __init__(self,name,height):
        # 初始化一个将第一个参数，创建一个内存空间
        self.name = name
        self.height = height

    def __trunc__(self):
        print(f"{self.name}会走路")
        xjs = Persen("邢继森",100)
        print(xjs.name)
    @classmethod
    def test(cls):
        print(f'这是一个类方法,由{cls}组成')


zs = Persen('邢继森',180)
print(zs.name)
print(zs.height)

print(Persen.info,Persen.name)


xiaoming = Persen
print(xiaoming.__class__)


def main():
    pass