# -*- codeing = utf-8 -*-
# @Time : 2022/3/3 19:06
# @Author : 邢继森
# @File : 打印九九乘法表.py
# Software : PyCharm
def cfb():
    for i in range(1, 10):
        for j in range(1, i + 1):
            print('{}x{}={}\t'.format(j, i, i * j), end='')
        print()
for i in range(0,10):
    print(i)
    cfb()