# -*- codeing = utf-8 -*-
# @Time : 2022/3/3 19:32
# @Author : 邢继森
# @File : persion.py
# Software : PyCharm
if __name__ == "__main__":
    # 类分为属性和方法
    class persion:
        name = "邢继森"
        age = 10
        score = 100
        def eat(self):
            print(fr'{self.name}会吃顿饭')
        def info(self):
            print(f'{self.name}今年{self.age}岁,高考数学{self.score}分')
    # 实例化
    zhangsan = persion()
    # 通过实例对象调用eat方法啊
    zhangsan.eat()
    zhangsan.info()

    persion().eat()