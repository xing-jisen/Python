# -*- codeing = utf-8 -*-
# @Time : 2022/3/3 19:32
# @Author : 邢继森
# @File : persion.py
# Software : PyCharm
if __name__ == "__main__":
    # 类分为属性和方法
    # class相当于一个模板，里面包含多个方法，eat和info
    class persion:
        # 实例化对象
        def __init__(self,name,age,score):
            self.name = name
            self.age = age
            self.score = score
        def eat(self):
            print(fr'{self.name}会吃顿饭')
        def info(self):
            print(f'{self.name}今年{self.age}岁,高考数学{self.score}分')

    # 实例化
    xjs = persion('邢继森',19,130)
    xjs.info()
    
    persion.eat(xjs)

    persion('邢继森1',20,130).eat()