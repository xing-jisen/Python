# -*- codeing = utf-8 -*-
# @Time : 2022/3/10 21:07
# @Author : 邢继森
# @File : a2.py
# Software : PyCharm
class person:
    # 方法（模板）
    # 面向对象（先设计模板）
    def __init__(self,name,age,score):#属性（三个：name，age，score）
        '''
        将第一个参数初始化
        参数：self（产生新的内存区域）
        当前没有初始化，只是创建一个内存区域
        '''
        self.name = name
        #前面是内存空间名，后面是变量名（属性）
        self.age = age
        self.score = score

#对象方法
    def eat(self):
        #eat为方法，定义self
        print(fr'{self.name}会干饭')

    def info(self):
        print(fr'{self.name}今年{self.age},身高{self.score}')


    def test(cls):
        #test为类方法
        print('类方法')

#person.test()
'''
li = [2,123,4,3,4,35,2,4,5,56]#相当于类
li.sort()
print(li)
'''
class son (person):
    #继承父级
    #包含父级所有类方法
    pass



# 实例 --新的内存区域
#
aa = person('Nagi',30,180)
# 初始化模板
aa.info()
#实例化对象

aa.name = 'jj'#变量内容可更改
print(aa.name)
