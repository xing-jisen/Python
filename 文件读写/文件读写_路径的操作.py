# -*- codeing = utf-8 -*-
# @Time : 2022/3/24 21:17
# @Author : 邢继森
# @File : 文件读写_路径的操作.py
# Software : PyCharm

# 绝对路径
# 相对路径

# os 可以获取路径的内容
import os

print(os.getcwd()) # 获取当前的工作目录
print(os.path.abspath('文件读写_路径的操作.py')) # 返回当前文件的绝对路径
print(os.path.dirname('D:\PyCharm\pycharm one\文件读写\文件读写_路径的操作.py')) # 获取上一层文件夹的位置

print(os.path.exists(r'D:\PyCharm\pycharm one\文件读写\文件读写_路径的操作.py'))# a判断这个文件夹存不存在
print(os.path.exists('面向对象'))# 判断这个文件夹存不存在

print(os.path.split(r'D:\PyCharm\pycharm one\文件读写\文件读写_路径的操作.py')) # 最后一个文件夹抽离开来

print(os.path.splitext(r'D:\PyCharm\pycharm one\文件读写\文件读写_路径的操作.py')) # 快速获取文件的后缀名

# 判断是不是一个文件夹或文件
print(os.path.isfile('文件读写_路径的操作.py')) # 判断是不是文件
print(os.path.isfile(r'D:\PyCharm\pycharm one\文件读写\文件读写_路径的操作.py')) # 判断是不是文件

# 展示出当前目录里都有什么目录
print(os.listdir('.')) # 获取当前目录的所有文件或文件夹
print(os.listdir('..')) # 获取上一级目录的所有文件或文件夹
