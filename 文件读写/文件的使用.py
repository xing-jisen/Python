# -*- codeing = utf-8 -*-
# @Time : 2022/3/24 21:45
# @Author : 邢继森
# @File : 文件的使用.py
# Software : PyCharm

import os
# 创建文件夹
os.makedirs("tmp")
# 删除文件夹
os.rmdir('tmp')


# 打开  w 写入
f = open('aaa.py','w')
# 写入
f.write('HelloPython')
# 关闭
f.close()

# 打开  a 追加
f = open('bbb.py','a')
# 写入
f.write('HelloPython\n')
# 关闭
f.close()

# 简化
# 打开  a 追加
with open('bbb.py','a') as f:
    # 写入
    f.write('HelloPython\n')

# 打开  r读取
with open('bbb.py','r') as f:  # read
    print(f.read())        # 读取指定字节的内容
    print(f.readline())    # 读取一行内容
    print(f.readlines())   # 读取所有行内容

# 读取照片用    rb
# 保存图片用    wb