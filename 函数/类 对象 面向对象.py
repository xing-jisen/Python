# -*- codeing = utf-8 -*-
# @Time : 2022/3/22 19:12
# @Author : 邢继森
# @File : 类 对象 面向对象.py
# Software : PyCharm
class Person:
    def __init__(self , name:str , sex:str = '男' , hobby:str = '抽烟，喝酒，烫头') -> None:     # ->  代表有没有返回值
        # 初始化的参数
        self.name = name    # self表示当前类的对象
        self.sex = sex
        self.hobby = hobby


    def eat(self):
        print("干烦了")

    def study(self , info:str):
       print(f"努力学习:{info}")

    def show_me(self) -> str:
        return f'我叫：{self.name}，我的爱好是：{self.hobby}'

p1 = Person(name = '邢继森')
print(p1.hobby)
p1.eat()
p1.study('python')
print(p1.show_me())
