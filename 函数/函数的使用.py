# -*- codeing = utf-8 -*-
# @Time : 2022/3/20 13:18
# @Author : 邢继森
# @File : 函数的使用.py
# Software : PyCharm
# 函数
def function_name():
    print('大家好,请记得给我点赞关注')
function_name()


# 传递参数
def function_name1(a):
    print(f'大家好,我叫{a}')
function_name1('邢继森')


# 传递多个参数
def function_name2(a,b):
    print(f'大家好,请记得给我{a}{b}')
function_name2('点赞','关注')


# 传递可变参数
def function_name2(*args):
    print(f'大家好,请记得给我{args}')
function_name2('点赞','关注')

def a1(*a):
    for i in a:
        print(i)
    print(a)
a1(1,2,3,4,5,6,7)


# 相当于存入的参数是字典
def a2(**k):   # name = allen age = 18
    print(type(k))
    print(k.get('a'))
a2(a = 1,b = 2)



# 参数约束
def fun(a:int,b:int):       # b:int传入的参数必须是int类型
    print(a+b)
fun(1,2)

# 函数里面赋值
def fun2(a:str,b = '投币'):
    print(f'大家好,请记得给我{a}{b}')
fun2('点赞')

def fun3(a:str,b:str = '投币'):
    print(f'大家好,请记得给我{b}{a}')
fun2(a = '投币',b = '点赞')


# 返回值
def fun(a,b):       # b:int传入的参数必须是int类型
    return a + b
x = fun(1,2)
print(x)
